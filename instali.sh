#!/usr/bin/env bash

#instali grav
git clone -b master https://github.com/getgrav/grav.git gej
cd gej
bin/grav install
bin/gpm install admin
bin/gpm install quark-open-publishing
bin/gpm install social-media-links
cd ..

#aldonas nian agorgojn, enhavojn
bash update.sh
