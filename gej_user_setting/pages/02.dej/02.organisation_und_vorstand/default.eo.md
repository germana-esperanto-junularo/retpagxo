---
title: 'Organizo kaj Estraro'
hide_git_sync_repo_link: false
menu: 'Organizo kaj Estraro'
---

## Organizo de GEJ

La laboro de GEJ estas kontribuita de homoj en pluraj laborgrupoj, komisioj kaj projektoj.
Por rapida klarigado jen ĉi-tiu bildo.

<div class="columns">
    <div id="item" class="column col-8 col-mx-auto col-md-12 extra-spacing">
      <figure class="figure">
        <img class="img-responsive" src="/retideoj/gej/user/pages/images/GEJ_organiza_skemo.png">
        <figcaption class="figure-caption text-center">Organiza skemo de GEJ</figcaption>
      </figure>
    </div>
</div>

#  Estraro

Jen la plej gravaj adresoj:

<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <h2>Prezidantino</h2>
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="/retideoj/gej/user/pages/images/large_Michaela.png">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <b>Michaela Stegmaier</b> <br> <br>
           Geysostraße 15 <br>
           38106 Braunschweig <br> <br>
           <p>Retpoŝto:
              <a href="mailto:gej.prezidanto _at_ esperanto _punkt_ de">gej.prezidanto<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <h2>Kasisto</h2>
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="/retideoj/gej/user/pages/images/large_Lars.png">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <b>Lars Hansen</b> <br> <br>
           Stettiner Str. 22    <br>
           71032 Böblingen <br> <br>
           <p>Retpoŝto:
              <a href="mailto:gej.kasko _at_ esperanto _punkt_ de">gej.kasko<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>
</div>

##  Kromaj Estraranoj


<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="/retideoj/gej/user/pages/images/large_Konstanze.png">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <p>Retpoŝto:
              <a href="mailto:konstanze.schoenfeld _at_ esperanto _punkt_ de">konstanze.schoenfeld<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="/retideoj/gej/user/pages/images/large_Devid.png">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <p>Retpoŝto:
              <a href="mailto:david.mamsch _at_ esperanto _punkt_ de">david.mamsch<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>
</div>
