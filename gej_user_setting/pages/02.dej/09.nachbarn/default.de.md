
---
title: Unsere Nachbarn
menu:  Unsere Nachbarn
taxonomy:
    tag:
    category:
---

<link href="nachbarn/jqvmap.min.css" media="screen" rel="stylesheet" type="text/css">
<script type="text/javascript" src="nachbarn/jquery.vmap.min.js"></script>
<script type="text/javascript" src="nachbarn/orgas.js"></script>
<script src="nachbarn/jquery.vmap.europe.js"></script>

### Unsere Nachbarn

Esperanto ist eine internationale Sprache, und die DEJ ist in stehts in reger Zusammenarbeit mit andern Esperanto Jugenden und Organisationen auf der ganzen Welt. Hier ein kleiner Überlick und Verweise auf Esperantojugenden in Europa und der Welt, hier auch als [Liste](https://www.tejo.org/pri-ni/strukturo/landaj-sekcioj/) auf der Website von TEJO.

<div>
<div class="columns">
    <div class="column col-2 col-md-hide"></div>
    <div id="vmap" class="column col-8 col-md-12" style="height: 400px;"></div>
    <div class="column col-2 col-md-hide"></div>
</div>
<div id="ligiloj" style="min-height:100px"></div>
</div>

### Freiwilligendienst mit Esperanto

Mit einer internationalen Sprache verkehrts sich gut im internationalen Umfeld! Bei diesen Organisationen wird dir davon viel geboten. Wenn du ein Jahr Zeit hast, und es mit vielen interessanten Erfahrungen füllen möchtest, denk auf jeden Fall über diese Möglichkeiten nach:


#### Freiwilligendienst bei E@I in der Slowakei

[LINK ZU E@I FREIWILLIGENDIENST](https://ikso.net/evs/)

#### Freiwilligendienst bei Frankreich 

Gibs da noch was? Bzw regelmäßig?

#### Freiwilligendienst bei TEJO in den Niederlanden 

[LINK ZU E@I FREIWILLIGENDIENST](https://www.tejo.org/agadoj/talento/volontulado/)


<script>
//color: '#31a354',
//#238b45
//hoverColor: '#a1d99b',

var hover = function(event, code, region){
    //alert("over "+code+" "+code_2_eo[code])
    var kodo_havebla = code_2_eo[code];
    if(kodo_havebla){
        var ligiloj = landaj_organizoj[code_2_eo[code]]
        document.getElementById('ligiloj').innerHTML = ligiloj
    }
}

jQuery('#vmap').vectorMap({
    map: 'europe_en',
    backgroundColor: null,
    color: '#efefef',
    hoverColor: '#238b45',
    selectedColor:'#006d2c',
    showTooltip: true,
    enableZoom: false,
    showTooltip: true,
    selectedRegions: ['de'],
    onRegionTipShow: function(e, el, code){
        el.html(el.html()+' (GDP - '+code+')');
    },
    onRegionOver: hover,
    onRegionClick: hover,
    onLoad: function(e, code, r){
        var m={};
        for(let code_label of landoj_uea){
            //alert(code_label)
            if(code_2_eo[code_label])
                m[code_label] = '#31a354';
            else
                m[code_label] = '#a1d99b';
        }    
        jQuery('#vmap').vectorMap('set', 'colors', m);
        jQuery('#vmap').vectorMap('set', 'selected', {de: '#006d2c'});
    }
});
</script>
    