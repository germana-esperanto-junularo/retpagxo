
---
title: DDR
order:
    by: date
    dir: asc
date: '18-07-1965 12:15'
taxonomy:
    tag:
    category:
---

Esperanto-Gruppen, die nach dem Ende des Zweiten Weltkrieges in der
Sowjetischen Besatzungszone wiedererstanden waren, wurden 1949 auf Grund eines
Dekretes der "Deutschen Verwaltung des Inneren in der SBZ" verboten. Die
Bemühungen ostdeutscher Esperantisten um eine Wiederzulassung organisierter
Tätigkeit für und mit Esperanto führten erst am 31. März 1965 zur Gründung des
"Zentralen Arbeitskreises der Esperanto-Freunde im Deutschen Kulturbund"
(ZAKE), der 1981 in den "Esperanto-Verband im Kulturbund der DDR" umgewandelt
wurde (gebräuchliche Esperanto-Abkürzung: GDREA).

Seit 1968 koordinierte eine Jugendkommission Aktivitäten junger Esperantisten.
Sie unterhielt Kontakte zur [FDJ](http://de.wikipedia.org/wiki/FDJ) und wirkte
seit 1971 im Esperanto-Weltjugendverband [TEJO](http://www.tejo.org/) mit. Aus
ihr entwickelte sich 1990 die selbständige "Esperanto-Jugend (DDR)", die sich
im Dezember gleichen Jahres mit der Deutschen Esperanto-Jugend vereinigte.

Die Arbeit mit Kindern wurde durch eine Kommission koordiniert, die u.a. von
1981-1989 zehn Kindertreffen organisierte.

Beschränkungen staatlicherseits (Reisebeschränkungen, Valutamangel,
Einfuhrbeschränkungen, wenig "Westbeziehungen") und eine (zwar abnehmende,
doch regional immer wieder auftauchende) unterschiedlich ausgeprägte
Reserviertheit staatlicher Stellen dem Esperanto gegenüber engten die
Betätigungsmöglichkeiten des Verbandes ein. Dennoch erschlossen ZAKE und GDREA
vielen Interessenten wertvolle internationale Beziehungen außerhalb
offizieller Kanäle und konnten erreichen, daß die internationale Sprache in
der DDR ein relativ hohes gesellschaftliches Ansehen genoss.

### Aktive

Vorsitzende des ZAKE bzw. von GDREA waren Rudi Graetz (1965-77), Rudolf
Hahlbohm (1977-81), Hans Heinel (1981-89) und Dr. Ronald Lötzsch (1989-zur
Vereinigung mit dem DEB im Mai 1991).

Sekretäre von ZAKE/GDREA waren Eugen Menger (1965-67), Raimund Knapp
(1967-68), Detlev Blanke (1968-90) und Ulrich Becker (1990-91). GDREA verfügte
über eigene Büroräume, ein Budget sowie hauptamtliche Mitarbeiter.
