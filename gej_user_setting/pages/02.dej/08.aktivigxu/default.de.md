---
title: 'Aktiv werden'
media_order: 'Michaela.jpg,Lars.jpg,Konstanze.jpg,Paul.jpg,David1.jpg,Jan.jpeg,Annika.jpg,Cornelia.jpg,Jani.jpg,Jonny1.jpg'
hide_git_sync_repo_link: false
menu: 'Aktiv werden'
---

### Engagier dich für Esperanto

Im Verein engagieren sich viele Menschen auf unterschiedliche Weise, um Anderen Esperanto nahe zu bringen. Wenn du auch motiviert bist dich für die Verwendung und Verbreitung von Esperanto einzusetzen [melde](../../kontakt) dich gerne bei uns mit deiner Idee!

### Engagierte Mitglieder

<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Michaela</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Michaela.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
              <li>Soziale Medien</li>
              <li>Redakteurin 'kune'</li>
            <li>Jugendvertreterin im DEB</li>
            <li>Ortsgruppe Herzberg'</li>
            <li>Newsletter</li>  
      </div>
    </div>
  </div>
    
 <div class="column col-6 col-md-12 extra-spacing">
    <h4>Lars</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Lars.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
              <li>Finanzen</li>
              <li>Subventionen</li>
            <li>JES Budget</li>
            <li>Mitgliederverwaltung</li>  
        </div>
      </div>
  </div>
</div>


<div class="columns">
    <div class="column col-6 col-md-12 extra-spacing">
      <h4>Konstanze</h4>
      <div class="columns">
          <div class="column col-6 col-md-12 extra-spacing">
            <figure class="figure">
              <img class="img-responsive" src="aktivigxu/Konstanze.jpg">
            </figure>
          </div>
          <div class="column col-6 col-md-12 extra-spacing">
                <li>Zusammenarbeit mit TEJO</li>
              <li>Erasmus+ Projekte</li>
          </div>
        </div>
    </div>
    <div class="column col-6 col-md-12 extra-spacing">
        <h4>David</h4>
        <div class="columns">
            <div class="column col-6 col-md-12 extra-spacing">
              <figure class="figure">
                <img class="img-responsive" src="aktivigxu/David1.jpg">
              </figure>
            </div>
            <div class="column col-6 col-md-12 extra-spacing">
                <li>JES Organisation</li> 
                <li>Internationale Zusammenarbeit</li>
            </div>
          </div>
      </div>
  </div>
    

<div class="columns">
 <div class="column col-6 col-md-12 extra-spacing">
    <h4>Annika</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Annika.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
           <li>JES Organisation</li> 
            <li>Unterricht</li>
            <li>Ortsgruppe Würzburg</li>
            <li>Spieletreffen</li>
        </div>
      </div>
  </div>

  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Paul</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Paul.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>Website Administrator</li>
            <li>Öffentlichkeitsarbeit</li>
        </div>
      </div>
  </div>
</div>

<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Michael</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/michael.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>JES Helfer</li> 
            <li>Social Media</li>
            <li>TEJO-Komitatano</li>
            <li>Ortsgruppe Saarbrücken</li>
      </div>
    </div>
  </div>

  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Jani</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Jani.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
              <li>KEKSO Organisation</li> 
      </div>
    </div>
  </div>
</div>

<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Cornelia</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Cornelia.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
              <li>Mitarbeiterin kune</li> 
            <li>Unterricht</li> 
      </div>
    </div>
  </div>

  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Jonny</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Jonny1.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
              <li>Kunst und Kultur</li> 
            <li>Social Media</li> 
            <li>Newsletter</li>
            <li>Mitgliederverwaltung</li>            
        </div>
    </div>
  </div>
</div>

###Kommissionen

- Finanzen, den Kassenwart beim Geschäftsbetrieb oder bei Budgets für Treffen oder Projekte unterstützen (KasKo): Lars, Christine, Rolf (gej.kasko@esperanto.de)
- Unterpunkt Subventionen mit Andreas Diesel (SuPo): Lars

- Internationales z.B. Beziehungen zu TEJO und PEJ (KIR): Vorstand (Michaela, Lars, David, Tuŝka) und komitatanoj (Michael und Klara)
- Unterpunkt Außenbeziehungen zu anderen deutschen Jugendverbänden (KonKERo): vakant

- Internet, Website (IReK): Paul, Jonny Michael, Michaela (gej.retejo@esperanto.de)
- Öffentlichkeitsarbeit in Sozialen Medien, Zeitungen usw. (KAPRi): Jonny, Michaela, Michael und Tim

- Mitgliederverwaltung (MA): Lars, Jonny

- Regionales, Ortsgruppen (KoLA): Michaela Herzberg, Annika Würzburg, Michael Saarbrücken

- Treffen, JES und KEKSO (KKRen): David und Lars (JES), Jani, Paul, Michaela (KEKSO)

- Mitgliederzeitschrift, Artikel zu interessanten Themen veröffentlichen, Mitglieder vorstellen, Interviews führen uvm. (Kune): Michaela Cornelia (kune@esperanto.de)

- Unterricht und Neulingsbetreuung, z.B. unser Kostenloser Esperanto Kurs, alternative Kurse (KINo): Paul, Annika, Cornelia
- KEK-Mentoren zuweisen: Lars, Michaela (kek@esperanto.de)

- Geschäftsstelle (BerO): Sibylle Bauer (bero@esperanto.de)

- Zusammenarbeit mit DEB (Jugendvertreter): Michaela (michaela.stegmaier@esperanto.de)