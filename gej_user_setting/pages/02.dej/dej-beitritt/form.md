---
title: 'Anmeldung Deutsche Esperanto Jugend'
form:
    name: join-form
    fields:
        -
            name: name
            label: Vorname/antaŭnomo:
            placeholder: 'Dein Name/Via Nomo'
            type: text
            validate:
                required: true
        -
            name: familia_name
            label: Name/nomo:
            placeholder: 'Dein Name/Via Nomo'
            type: text
            validate:
                required: true
        -
            name: email
            label: Email
            placeholder: 'Deine Email-Adresse/Via retpoŝtadreso'
            type: email
            validate:
                required: true
        -
            name: naskigxtago
            type: date
            label: 'Dein Geburtsdatum/Via naskiĝdato'
            default: '2015-01-01'
            validate:
                min: '1985-01-01'
                max: '2015-13-13'
                required: true
        -
            name: adreso
            type: text
            placeholder: 'Deine Adresse/Via adreso'
            label: 'Straße und Hausnummer/strato kaj domnumero:'
            validate:
                required: true
        -
            name: adresaldonajxo
            type: text
            label: 'Adresszusatz/adresaldonaĵo:'
        -
            name: plz_urbo
            type: text
            label: 'PLZ und Stadt/poŝtkodo kaj urbo:'
            validate:
                required: true
        -
            name: lingonivelo
            label: 'Deine Sprachniveau/Via lingvonivelo'
            placeholder: Auswählen/Elektu
            type: select
            options:
                - fließend/flue
                - 'geht so/meze'
                - 'ein bisschen/iomete'
                - Anfänger/komencanto
            validate:
                required: true
        -
            name: laboro
            label: Berufstätig/ĉu vi laboras?:
            type: radio
            options:
                - Ja/Jes
                - Nein/Ne
            validate:
                required: true
        -
            name: datumoj
            type: radio
            label: 'Persönliche Daten/ Personaj datumoj'
            options:
                - Ja/Jes
                - Nein/Ne
            validate:
                required: true
    buttons:
        -
            type: submit
            value: Anmelden
    process:
        -
            email:
                from: '{{ config.plugins.email.from }}'
                to:
                    - '{{ config.plugins.email.to }}'
                    - '{{ form.value.email }}'
                subject: '[Mitgliedsanmeldung] {{ form.value.name|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
            message: Danke für deine Anmeldung in der Deutschen Esperantojugend! Wir melden uns bald bei dir!
        -
---

## Beitrittsformular zur Deutschen Esperantojugend

Hiermit beantrage ich die Aufnahme in die Deutsche Esperanto Jugend.
