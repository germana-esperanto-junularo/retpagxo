---
title: 'Germana Esperanto Junularo'
hide_git_sync_repo_link: false
menu: 'Germana Esperanto Junularo'
---

### Germana Esperanto Junularo

Germana Esperanto Junularo

La Germana Esperanto Junularo (GEJ) estas la branĉo de la GEA por junuloj. Fondita en 1951 nuntempe GEJ havas ĉirkaŭ 100 membrojn.

La GEJ celas la interkomunikadon de la popoloj kaj speciale fokusas tion per internacia junularkunlaborado. Esperanto helpas nin fari tiun, dum la laboro kun aliaj landoj. Samtempe ni ankaŭ zorgas pri la agado en niaj federacŝtataj organizoj.
<!-- es gibt zwar nur noch bavelido als Landesverband, das nicht aufgelöst werden kann, weil keiner mehr da ist, der das machen kann, aber egal -->
Niaj taskoj principe konsistas el publikinformado, organizi esperantoinstruadon, renkontiĝoj, klerigado kaj internacia kunlaboro.  

Anecoj: La GEJ estas sekcio de TEJO kaj fonditmembro de la "Arbeitsgemeinschaft Neue Demokratische Jugendverbände".
<!-- hier auch ein Link zu Tejo in der Eo-Version? -->
### Publikinformado

Multe da homoj en germanio ne scias kio estas Esperanto kaj ke ekzistas la GEJ. Fojfoje la ideoj rilata al Esperanto estas miskomprenataj. Aliaj kredas, ke Esperanto estas hispana dialekto kaj neniam aŭdis de [konstruitaj lingvoj](https://eo.wikipedia.org/wiki/Planlingvo). Ĉar ne sufiĉe da homojn scias pri la multaj esperantorenkontiĝoj kaj la riĉa kulturo oni ankoraŭ ofte povas aŭdi: "Esperanto, ĉu iu parolas tion?"

La GEJ volas informi tiujn, kiuj ne scias pri Esperanto per prezentado de informigbudoj ĉe grandaj aranĝoj, kiel ekzemple la ekleziaj tagoj aŭ eldonado de broŝuroj. Antaŭ la eklerno de Esperanto ja oni devas scii, kio estas Esperanto kaj kion oni povas fari per ĝi.

### Esperantokursoj

Ekzistas multe da esperantokursoj kaj lernlibroj por memlerni, sed pli agrable kaj ĝoje oni lernas kun aliaj. Pro tio la GEJ organizas kursojn dum la tuta jaro. Plejfoje estas semajnfinkursoj en Germanio, sed ankaŭ eblas por vi lerni Esperanton dum grandaj internaciaj renkontiĝoj, kiuj daŭras pli longe. Ambaŭkaze vi povas lerni kaj praktiki kune kaj ekkoni Esperantujon.

### Erasmus+ projektoj

Krom la esperantoaktivaĵoj ni ankaŭ ebligas por niaj membroj partopreni en projektoj, kiuj ne ĉiam rekte rilatas al Esperanto, sed kiuj ege utilas por gajni gravan scipovon, kiun vi povas uzi en asocioj aŭ alimaniere. Dank al la subvencioj de la Eŭropa Unio ni povas viziti  internaciajn seminariojn kadre de la projektoj, kiujn gvidas spertaj kaj kapablaj trejnistoj.

### Organizi renkontiĝojn

Esperanto vivas per siaj parolantoj. Malgraŭe Esperanto utilas multmaniere, plejparte de la homoj estas plejatraktata de la [internaciaj renkontiĝoj](https://eventaservo.org). Tie eblas speciale por junuloj kontakigi homojn de la tuta mondo, diskuti, festi, aŭ havi klerinterŝanĝon. Dum esperantorenkontiĝoj ekestas jarlongaj emikecoj inter homoj de diversaj nacioj. 

Ĉiujare ĉirkaŭ novjaro la "Junulara Esperanto Semajno" (JES), kiu okazas normale alterne en Germanio kaj Pollando, estas organizata de la GEJ kaj la Pola Esperanto Junularo (PEJ). Kun ĉirkaŭ 250 partoprenantoj el 30 landoj ĝi estas la dua plej granda renkontiĝo por junaj Esperantistoj en la mondo. Nur la "Internacia Junulara Kongreso" ([IJK](https://www.tejo.org/cause/internacia-junulara-kongreso/)) estas pli granda.

### Labori kune kun aliaj landoj

La internacia kunlaboro estas unu de la ĉefaferoj la GEJ zorgas pri.

Per subvencipetoj ni ebligas ankaŭ studentojn kaj lernejanojn partopreni en diversaj renkontiĝoj, ĉar tiuj tiel povas ricevi rabatojn.

Certe ni ja ankaŭ organizas renkontiĝojn kun alilandaj esperantojunularoj, kiel la JES kun la PEJ.

Finfine la GEJ ankoraŭ havas du reprezentantojn en la komitato de la "Tutmonda Esperantista Junulara Organizo" (TEJO).
