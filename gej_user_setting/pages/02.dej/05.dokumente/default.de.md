
---
title: Dokumente
menu:  Dokumente
taxonomy:
    tag:
    category:
---

## Vereinsdokumente

Hier einige wichtige Dokumente der GEJ:

Auf Deutsch:


  1. [Statut / Satzung](satzung/default.de.md) oder als [PDF](./2019_01_02_dejsatzung.pdf)
  2. [Geschäftsordnung der Hauptversammlung](go_vorstand/default.de.md)
  3. [Geschäftsordnung des Bundesvorstands](go_jhv/default.de.md)
  4. [Beitragsordnung]()
  5. [Formular zur Fahrtkostenbeantragung](./Fahrtkostenformular.pdf)
  6. [Regelung über die Teilnahme an Veranstaltungen]()
  7. [Regelungen über Fahrtkostenzuschüsse]()
  8. [Regelung für Veranstaltungen]() durch Unterorganisationen
  9. [Freistellungsbescheid](./Freistellungsbescheid_2014-16.pdf) (2014-2016)

Auf Esperanto:

  1. [Regularo pri repagoj, rabatoj kaj honorarioj]()
  2. [Regularo pri monsubteno al la suborganizoj]()
  3. [Specimena tagordo de la jarĉefkunveno]()
  4. [Kontrakto pri unua Junulara E-semajno]() inter Germana kaj Pola Esperanto-Junularoj
  5. [Kontrakto pri interŝanĝo JES-IFJ](./Kontrakto_IEJ-GEJ-PEJ.pdf) inter Germana, Itala kaj Pola Esperanto-Junularoj
  
