---
title: 'Deutsche Esperanto-Jugend'
hide_git_sync_repo_link: false
menu: 'Deutsche Esperanto-Jugend'
---

Die Deutsche Esperanto-Jugend (DEJ) ist der Jugendverband des [Deutschen
Esperanto-Bundes](http://esperanto.de/deb/) (DEB). Sie wurde 1951 gegründet
und hat heute ca. 100 Mitglieder bis 27 Jahre.

Ziel der Deutschen Esperanto-Jugend ist die Völkerverständigung, insbesondere
auf der Ebene der internationalen Jugendarbeit. Zur Erreichung dieses Ziels
wird dabei die internationale Sprache Esperanto verwendet.

Die Umsetzung erfolgt hauptsächlich durch Zusammenarbeit mit Jugendorganisationen in anderen Ländern.
Gleichzeitig vernetzen wir die Aktivitäten unserer Ortsgruppen und
Landesverbände.
<!-- es gibt zwar nur noch bavelido als Landesverband, das nicht aufgelöst werden kann, weil keiner mehr da ist, der das machen kann, aber egal -->
Unsere Aufgaben lassen sich grob in Öffentlichkeitsarbeit, Esperanto-
Unterricht, Treffenorganisation, Seminare und internationale Zusammenarbeit
untergliedern.

Mitgliedschaften: Die DEJ ist Sektion der [Weltesperantojugend
TEJO](http://www.tejo.org/) und Unterverband des Deutschen Esperanto-Bundes.
Außerdem sind wir Gründungsmitglied der Arbeitsgemeinschaft Neue Demokratische
Jugendverbände.

###  Öffentlichkeitsarbeit

Ein Großteil der Menschen in Deutschland weiß nicht, was Esperanto ist und
dass es die Deutsche Esperanto-Jugend gibt. Manchmal werden sogar falsche
Ideen mit Esperanto verbunden, beispielsweise wird der Euro oft als
"Esperanto-Währung" bezeichnet (was er nicht ist, da Esperanto andere Sprachen
gerade erhalten und nicht ersetzen möchte). Andere meinen, Esperanto sei ein
spanischer Dialekt und wissen nicht, was eine
[Plansprache](http://de.wikipedia.org/wiki/Plansprache) ist. Da die
zahlreichen Esperanto-Treffen und die Esperanto-Kultur in der Öffentlichkeit
kaum wahrgenommen werden, hört man auch immer wieder "Esperanto? Das spricht
doch keiner!".

Dieser Nichtinformiertheit versucht die Deutsche Esperanto-Jugend
entgegenzuwirken, indem sie verschiedene Informationsbroschüren
<!-- Soll hier noch ein Link zu den Broschüren hin oder warum ist ein neuer Absatz hier? -->
herausgibt und u.a. auf Messen, Kirchentagen, usw. präsent ist. Bevor jemand
Esperanto lernt, muss er/sie erst wissen, was Esperanto überhaupt ist und was
man damit alles machen kann.

###  Esperantokurse anbieten

Es gibt zwar eine ganze Reihe von Büchern und Internetkursen, um sich
Esperanto im Selbststudium alleine beizubringen. Schöner und mit mehr Spaß
verbunden ist es aber sicher, mit anderen Leuten zusammen Esperanto zu lernen!
Deswegen organisiert die Deutsche Esperanto-Jugend über das ganze Jahr
verteilt Esperantokurse. In Deutschland handelt es sich dabei meistens um
Wochenendkurse. Auf den großen internationalen Treffen wie der Internationalen
Woche gibt es außerdem Intensivkurse, die eine ganze Woche dauern. Dabei kann
man Esperanto lernen und sofort anwenden.

###  Erasmus+ Projekte

Neben den Esperanto-Aktivitäten nehmen wir auch an interantionalen Projekten teil, in deren Rahmen Seminare stattfinden, die nicht unbedingt direkt mit Esperanto in Verbindung stehen, dafür aber unseren Mitgliedern
wichtige Kenntnisse und Fähigkeiten vermitteln, die im Vereinsleben oder
anderweitig nützlich werden können. Dank der Erasmus+ Projekte der Europäischen Union können diese Seminare auf internationalen Treffen mit gut geschulten Referenten organisiert und durchgeführt werden.

###  Treffen organisieren

Esperanto lebt von seiner Sprechergemeinschaft. Auch wenn man eine Menge
verschiedener Dinge mit Esperanto machen kann, sind für viele die
internationalen [Treffen](http://eventoj.hu/) am reizvollsten. Hier besteht
die gerade für Jugendliche sonst eher seltene Gelegenheit, sich mit Leuten aus
aller Welt auszutauschen, zu diskutieren, zu feiern und miteinander in Kontakt
zu kommen. Auf Esperanto­treffen entstehen jahrelange Freundschaften zwischen
Menschen verschiedenster Nationalität. Die Deutsche Esperanto-Jugend
organisiert jedes Jahr zu Silvester gemeinsam mit der Polnischen Esperanto-
Jugend die Jugend-Esperantowoche (JES - Junulara E-Semajno) abwechselnd meist in Deutschland und Polen. Sie ist
mit rund 250 Teilnehmern aus 30 Ländern nach dem
[Esperantojugendweltkongress](http://ijk-69.mesha.org/) das zweitgrößte
internationale Esperanto-Jugendtreffen der Welt.

###  Zusammenarbeit mit anderen Ländern

Die Zusammenarbeit mit Esperanto-Organisationen aus der ganzen Welt ist eine
zentrale Aufgabe der Deutschen Esperanto-Jugend, die ganz unterschiedliche
Bereiche berührt.

So werden mit anderen Esperanto-Jugendverbänden gemeinsam Subventionsanträge
für Treffen und Seminare gestellt, um den Teilnehmern einen Teil der
Reisekosten zurückzuerstatten und die Treffen damit auch für Schülerinnen und
Schüler sowie Studierende bezahlbar zu machen.

Auch die Treffen selbst werden manchmal von mehreren Esperanto-Jugenden aus
verschiedenen Ländern organisiert, z.B. das gastronomische Wochenende von der
Französischen und der Deutschen Esperanto-Jugend.

Ferner vermitteln wir auch Brieffreundschaften zu anderen Esperantosprechenden
in der ganzen Welt.

Schließlich ist die DEJ aktiv in der Weltesperantojugend TEJO mit zwei
Delegierten vertreten.

