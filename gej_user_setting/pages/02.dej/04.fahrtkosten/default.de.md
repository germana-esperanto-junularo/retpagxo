
---
title: Fahrtkostenzuschüsse
menu:  Fahrtkostenzuschüsse
taxonomy:
    tag:
    category:
---

Wenn du in Esperanto-Sachen unterwegs bist, kannst du unter Umständen einen
Teil der Fahrtkosten von der DEJ zurückbekommen. Durch Aktivität für den
Verein sollen dir möglichst keine gesonderten Kosten entstehen. Vor allem dank
der Unterstützung des Bundesministeriums für Familie, Senioren, Frauen und
Jugend können Mitglieder für einige Treffen Fahrtkostenunterstützung erhalten,
die in der Regel den Jahresbeitrag deutlich übersteigt..


## Internationalen Treffen

Subventionen ermöglichen unserem Verein, die Fahrtkosten seiner Mitglieder zu
[internationalen Treffen](https://www.eventaservo.org) in gewissen
Grenzen zu übernehmen. Dafür solltest du dich während des Treffens vor Ort in
die Subventionsliste, das sogenannte "Formblatt L", eintragen, das eine*r der
deutschen Teilnehmer*innen für die DEJ mitbringt. Dieses wird dann an unseren
Subventionsbeauftragten weitergegeben.

Nach dem Treffen sind innerhalb von 4 Wochen die Fahrtkosten, etwa durch das
Einsenden der Fahrkarten, nachzuweisen. Vewende bitte das dafür [vorgesehene
Formular](../dokumente/Fahrtkostenformular.pdf)
und schicke es inklusive der zugehörigen Fahrkarten oder Belege per Post an
unseren [Schatzmeister](../organisation_und_vorstand). Alternativ kannst
du das ausgefüllte Formular (inkl. Scan oder Foto der Fahrkarten/Belege) auch
per E-Mail an <a href="mailto:gej.kasko _at_ esperanto _punkt_ de">gej.kasko<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
schicken.

Bei weiteren Fragen wende dich bitte an unseren Schatzmeister unter
<a href="mailto:gej.kasko _at_ esperanto _punkt_ de">gej.kasko<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>

<h3 id="internationale">Geförderte Treffen und maximale Zuschüsse 2017</h3>

<!--
This is a comment
| Treffen |  Ort/Datum |  Maximaler Zuschuss |
| ---|---|---|
| [IJF](http://iej.esperanto.it/ijf/?lang=eo) | 12.04.-18.04. Castione della Presolana, Italio |  80 €                                              |  
| [IJK](http://ijk2017.tejo.org/)             | 05.08.-12.08. Aneho, Togolando                 |  *)auf vorherige Anfrage (doch es lohnt sich ;) )  |
| [UK](http://www.2017uk.net/uk2017/)         | 22.07.-29.07. Seolo, Koreo                     |  *)auf vorherige Anfrage (doch es lohnt sich ;) )  |
| [SES](http://ses.ikso.net/)                 | 15.07.-23.07. Banská Štiavnica, Slovakio       | 80€                                                |
| [JES](http://jes.pej.pl)                    | 26.12. 2016 - 02.01. 2018 Stettin, Pollando    | 50 €                                               |
-->
<table class="table table-striped table-hover">
<thead>
<tr>
<th>Treffen</th>
<th>Ort/Datum</th>
<th>Maximaler Zuschuss</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="http://iej.esperanto.it/ijf/?lang=eo" target="_blank" rel="nofollow noopener noreferrer" class="external-link no-image">IJF</a></td>
<td>12.04.-18.04. Castione della Presolana, Italio</td>
<td>80 €</td>
</tr>
<tr>
<td><a href="http://ijk2017.tejo.org/" target="_blank" rel="nofollow noopener noreferrer" class="external-link no-image">IJK</a></td>
<td>05.08.-12.08. Aneho, Togolando</td>
<td>*)auf vorherige Anfrage (doch es lohnt sich ;) )</td>
</tr>
<tr>
<td><a href="http://www.2017uk.net/uk2017/" target="_blank" rel="nofollow noopener noreferrer" class="external-link no-image">UK</a></td>
<td>22.07.-29.07. Seolo, Koreo</td>
<td>*)auf vorherige Anfrage (doch es lohnt sich ;) )</td>
</tr>
<tr>
<td><a href="http://ses.ikso.net/" target="_blank" rel="nofollow noopener noreferrer" class="external-link no-image">SES</a></td>
<td>15.07.-23.07. Banská Štiavnica, Slovakio</td>
<td>80€</td>
</tr>
<tr>
<td><a href="http://jes.pej.pl" target="_blank" rel="nofollow noopener noreferrer" class="external-link no-image">JES</a></td>
<td>26.12. 2016 - 02.01. 2018 Stettin, Pollando</td>
<td>50 €</td>
</tr>
</tbody>
</table>

<br> <br>

Fehlende Angaben werden bei Bekanntgabe ergänzt.

[[Die Regelung im Wortlaut.]()]

## Vereinsarbeit

Wer im Auftrag der DEJ unterwegs ist, kann ebenfalls Fahrtkostenzuschüsse
erhalten. Dafür gibt es die Regelung zu Fahrtkostenzuschüssen für
Dienstreisen.

Was genau eine _Dienstreise_ im Sinne dieser Regelung ist, legt der Vorstand
individuell fest - im Zweifelsfalle kannst du nachfragen.

Einige häufige Fälle wurden bereits festgelegt:

1. Reisen zu Vorstandssitzungen sind für Vorstandsmitglieder und für vom Vorstand dazu eingeladene Personen Dienstreisen.
2. Reisen zur Vorbereitung eines JES sind für die dazu beauftragten Teilnehmer*innen Dienstreisen.
3. Der Vorstand kann festlegen, dass eine Anreise eines Lehrenden zu einem durch die Esperantojugend vermittelten Esperanto-Kurs eine Dienstreise ist.
