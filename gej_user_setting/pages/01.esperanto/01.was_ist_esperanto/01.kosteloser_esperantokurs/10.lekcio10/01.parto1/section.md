---
title: Teksto - Sed en Esperanto!
---

Klara alvokas Peter per sia poŝtelefono.* Saluton Peter, kion vi faros hodiaŭ?

* Mi prepariĝas al lingva ekzameno pri Esperanto. Baldaŭ mi povos sen grandaj problemoj legi Esperanton, sed mi ŝatus ankaŭ flue paroli ĝin. En la ekzameno, ĉio estas skribe, sed laŭ mi ambaŭ, kaj skriba kaj buŝa Esperanto, gravas. Kaj la buŝan mi ankoraŭ devas ekzerci.
* Kiom da tempo vi fakte bezonis por fari la tutan kurson?
* Po ĉirkaŭ kvar horojn por unu leciono, do entute eble kvardek. Kompare al ajna alia lingvoj tio estas kvazaŭ nenio, kaj mi sentas min jam sufiĉe progresinta, sed nur lege kaj ne buŝe. Ĉu morgaŭ ni povas renkontiĝi kaj ekzerci iom? Mi ankoraŭ havas kelkajn demandojn.
* Ankaŭ hodiaŭ ni povas ekzerci, se vi volas. Mi ne kontraŭus.
* Nu, jam hieraŭ ni renkontiĝis.
* Peter, tio ne estas ĝentila. Ĉu vi preferas renkonti alian knabinon anstataŭ mi?
* Tute ne! Simple, mi apenaŭ havas tempon por aliaj aferoj, se mi nur renkontiĝas kun vi. Antaŭ nia konatiĝo estis pli da tempo por ĉio, sed nun, kiam ni renkontiĝas tiom ofte, mi preskaŭ streĉiĝas iomete, ĉar mi ne certas, ke mi povas plenumi ĉiujn taskojn, devigajn kaj libervolajn. Sed eble ni povas paroli pri tio iam vid-al-vide. Almenaŭ mi preferus tion.
* Bone, Peter. Ni priparolos tion morgaŭ, sed en Esperanto!
* En ordo, adiaŭ!

### NEUE WÖRTER

* ajna - beliebiger
* anstataŭ - statt
* apenaŭ - fast nicht
* devigi - zwingen
* kontraŭ - gegen
* libervola - freiwillig
* po - je
* preskaŭ - fast
* streĉo - Stress
