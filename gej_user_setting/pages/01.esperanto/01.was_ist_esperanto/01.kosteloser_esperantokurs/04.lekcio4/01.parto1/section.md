---
title: "Teksto - La naskiĝtago"
---

En kafejo Klara kaj Peter parolas:

* Peter, en la sekva semajno mi veturos al internacia aranĝo por Esperanto-lernantoj en Slovakujo.
* Ĉu vi forveturos? Kaj mi? Kial vi ne restas ĉe mi?
* Ĉar mi legis en la interreto, ke oni aranĝos tie kurson de vendredo, la dek dua de majo, ĝis sabato, la dudeka de majo. Lerni eksterlande (ekster-land-e) estas pli interese ol resti hejme. Plej bone estus, se vi veturus kun mi! Mi plenigis jam la aliĝilon en la retpaĝo, kaj hieraŭ mi ricevis tiun ĉi retpoŝton. Tiel simpla ĉio estis. Jen, rigardu! Mi havas la retpoŝton en mia poŝtelefono.

> Peter prenas la poŝtelefonon el la manoj de Klara kaj eklegas (ek-legas) la retpoŝton:

> Kara Klara!
Ni ricevis vian aliĝilon kaj informas vin, ke vi povos partopreni en nia renkontiĝo. La
aranĝo komenciĝos la 12-an de majo kaj finiĝos la 20-an de majo. Do, la kurso daŭros
unu semajnon. Bonvolu alveni la unuan tagon ĝis la dudek unua horo. Vi devas kunporti
ĉiujn necesajn personajn aferojn, skribilojn kaj paperon.
Vi deziris loĝi en kvarlita (kvar-lit-a) ĉambro. Bedaŭrinde ne plu estas liberaj ĉambroj. Vi
povas elekti loĝi aŭ en amasloĝejo aŭ en tendo. Por ambaŭ vi devas kunporti dormsakon.
La lokon de nia kurso vi facile trovos, ĉar rekte apud ĝi haltas la aŭtobuso numero du ĉe la
haltejo „urba naĝejo“ en Poprad.

> Amike
Piotr

P.S: Mi legis en la datumbazo, ke vi naskiĝis la dekkvaran de majo. Tio estas la tago de
nia bicikla ekskurso. Se vi konsentas, ni aranĝos gajan naskiĝtagan feston dum la
ekskurso. Ĝis baldaŭ!

Peter redonas (re-don-as) la poŝtelefonon al Klara. Klara diras:* Estos ankaŭ kurso por komencantoj. Vendrede mi ekveturos. Ĉu vi volas veturi kun
mi?
* Ne, veturu, mi restos ĉi tie. Unue mi volas lerni pli per koresponda kurso. Mi
atendos vin.


### NEUE WÖRTER

Es wird Dir sicher immer leichter fallen, mehr und mehr Wörter aus Deiner Muttersprache abzuleiten:
internacia (inter-naci-a), Slovakujo, majo, lando, informo, sendi, numero, ekskurso, festo.

* ambaŭ - beide
* ami - lieben
* angulo - Ecke
* apud - neben
* arbo - Baum
* cent - hundert
* dekstre - rechts
* dekstren - nach rechts
* denove - erneut, von neuem, wieder
* direkto - Richtung
* frazo - Satz
* haltejo - Haltestelle
* infano - Kind
* kafejo - Café, Kaffeehaus
* konduti - sich benehmen
* konsisti el - bestehen aus
* konstruaĵo - Bauwerk, Gebäude
* konstrui - bauen
* kredi - glauben
* kvazaŭ - wie; so wie, gleichsam
* lakto - Milch
* mendi - bestellen
* muzeo - Museum
* ponto - Brücke
* poŝtejo - Post (Gebäude)
* proksima - nah
* proponi - vorschlagen
* prunte - leihweise
* serĉi - suchen
* simpla - einfach
* stacio - Station
* sukero - Zucker
* taso - Tasse
* trafiko - Verkehr
* tramo - Straßenbahn
* trans - darüber hinweg; auf der anderen Seite
* trovi - finden
* unue - erstens; zuerst
* veturi - fahren
* vojo - Weg
* volonte - gern

Das Esperanto-Wort ja heißt so viel wie „doch, ja“, aber nicht als Bejahung, sondern als
leichte Verstärkung der Bedeutung des nachfolgenden, selten des voranstehenden Worts
oder Wortgruppe:

mi estas ja nur ... - ich bin ja nur ..., ich bin doch nur ...
