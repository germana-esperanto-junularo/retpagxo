---
title: "Grammatik"
---

### unu/alia

* unu kun la alia - miteinander
* unu la alian - einander, einer den anderen
* unu al la alia - einander, einer dem anderen
* unu de la alia - voneinander
* unu por la alia - füreinander
* unu post la alia - nacheinander
* unu sub la alia - untereinander

usw.

### Die bezüglichen Fürwörter (Relativpronomen)

Die bezüglichen Fürwörter (Relativpronomen) können wie im Deutschen mit Präpositionen
zusammengesetzt werden, und bilden so eine Umstandsbestimmung im Teilsatz:

* kiu - der, welcher
* kun kiu - mit dem (gemeinsam), mit der
* kun kiuj - mit denen (gemeinsam)
* per kiu - mit dessen/deren Hilfe, mit dem/der
* pri kiu - über den/die/das
* el kiu - aus dem/der
* en kiu - in dem/der

usw.

* kio - was
* pri kio - worüber
* de kio - wovon

usw.

### Reihenfolge der Satzglieder

In der 1. Lektion haben wir dir die gebräuchlichste Reihenfolge der Satzglieder erläutert.
Dennoch kann diese Reihenfolge geändert werden, z.B. aus stilistischen Gründen:



| La nomon de la franca knabino     | oni     | elparolas     | Orelij     |
| :-------------                    | :------------- | :------------- | :------------- |
| direktes Objekt                          | Subjekt       | Prädikat       | Ergänzung       |

Hier ist das direkte Objekt an den Satzanfang gestellt worden. Das ist leicht an der
Endung -n zu erkennen.

Ähnlich verhält es sich mit den Relativpronomen. Wenn sie das direkte Objekt des zweiten
(abhängigen) Teilsatzes darstellen, erhalten auch sie die Akkusativendung:

* Kie estas la libro, kiun mi vidis hieraŭ? (Wo ist das Buch, das ich gestern gesehen habe?)
* Im Plural: Kie estas la libroj, kiujn mi vidis hieraŭ?

Da das Verb „vidi“ wie auch das deutsche „sehen“ ein Akkusativobjekt verlangt, spielt das
Relativpronomen „kiu“ diese Rolle, da es für das Wort „libro“ aus dem ersten Teilsatz steht.

Ein anderes Beispiel:

Pardonu al mi tion, kion mi faris.

Oder kürzer: Pardonu al mi, kion mi faris. - Verzeih mir (das), was ich getan habe.

### Ländernamen im Esperanto

Ländernamen im Esperanto werden nach zwei Regeln gebildet.

Ableitung des Ländernamens vom Bewohner mit der Nachsilbe -uj

Germano loĝas en Germanujo,
Franco loĝas en Francujo,
Slovako loĝas en Slovakujo;
usw. für Svedo, Norvego, Dano, Anglo, Skoto, Sviso, Hispano, Italo, Greko, Polo, Ĉeĥo, Hungaro, Ruso, ktp.

Ableitung der Bewohnerbezeichnung aus dem Ländernamen mit der Nachsilbe -an

En Nederlando loĝas la Nederlandanoj.
En Irlando loĝas la Irlandanoj.
En Kanado loĝas la Kanadanoj,
en Usono la Usonanoj,
en Brazilo la Brazilanoj,
en Indonezio la Indonezianoj,
en Luksemburgio la Luksemburgianoj. (La anoj de la urbo Luksemburgo estas la
Luksemburganoj)

Leider musst du lernen, welche Regel zu welchem Land/Bewohner gehört.
Neben diesen beiden Grundformen haben sich im Laufe der Zeit weitere Bildungsformen
etabliert – Esperantosprechern ist ihre Sprache manchmal irgendwie zu einfach. Zu diesen
Bildungsformen gehören -i- und -land- statt -uj-.
Danujo / Danlando / Danio

Wir empfehlen dir – in Übereinstimmung mit der Esperanto-Sprachnormungsinstanz
„Akademio de Esperanto“ – die Wortbildung mit -uj- nach dem klassischen Prinzip.

Eine lehrreiche Vorlesung auf Esperanto von Akademiemitglied Anna Löwenstein gibt es
auf Esperanto im Netz – wenn du weißt, was hier steht, reicht es aber auch.

Über die historische Entstehung der Ländernamen auf Esperanto und alles drum herum
weiß auch die Esperantosprachige Wikipedia viel zu berichten.


Zur Wortbildung

Präfix:


* ge-: fasst Vertreter beiderlei Geschlechts zusammen
  * patro (Vater) – gepatroj (Eltern)
  * frato (Bruder) - gefratoj (Geschwister)
  * sinjoro (Herr) - gesinjoroj! (Meine) Damen und Herren!

Suffixe:

* -uj-: zur Bildung vieler Ländernamen, außerdem auch für Gefäße
  * franco (Franzose) - Francujo (Frankreich)
  * germano (Deutscher) - Germanujo (Deutschland).
* -uj-: hat auch eine zweite Bedeutung: Gefäß, Behältnis
  * pano (Brot) - panujo (Brotbüchse)
  * floro (Blume) - florujo (Blumentopf)
  * mono (Geld) - monujo (Geldbeutel, Portemonnaie)
* -uj- hat auch eine selten gebrauchte dritte Bedeutung: Gewächs
  * pomo (Apfel) - pomujo (Apfelbaum)
  * rozo (Rose) - rozujo (Rosenstock)
  * Anstelle der Nachsilbe -uj- kann man für mehr Klarheit hier auch andere beschreibende Wörter zusammengesetzt benutzen: pomarbo und rozplanto
* -an- (Teilnehmer, Angehöriger):
  * Usono (USA) - Usonano (Einwohner der USA)
  * la sama lando (das gleiche Land) - samlandano (Landsmann)
  * urbo (Stadt) - urbano (Städter, Stadtmensch)
* -ad- (unterstreicht die Dauer einer Tätigkeit):
  * atendadi - lange warten
  * kuri (laufen) - la kurado (das Laufen, der Lauf, das Hin- und Herlaufen)
* -ar- (Ansammlung gleicher Dinge, Sammelbegriff):
  * tendo (Zelt) - tendaro (Zeltlager)
  * infano (Kind) - infanaro (Kinderschar)
  * homo (Mensch) - homaro (Menschheit)
