---
title: "Übungen - Ĉu kun fremda knabo al kinejo?"
---

Übungen, die unter dem Punkt 4 stehen, sind für deine eigene Kontrolle gedacht. Die Lösungen
zu den Übungen stehen vor den Kontrollübungen, also zwischen 6. und 7, du brauchst sie also
nicht von deinem Mentoren korrigieren lassen. Falls du unsicher bist, kannst du deinem Mentor
natürlich Fragen dazu stellen. Hier die Übungen:


### Lies die folgenden Sätze laut und übersetze!

1. Mi havas belan amikon.
2. Ŝi petas la knabon.
3. Li atendas la fratinon.
4. Ni atendas kun nia amikino.
5. Li iras en la parko.
6. Li iras en la parkon.

Finde jetzt noch alle Akkusativendungen und begründe für Dich selber, warum in einigen Sätzen
an die Ergänzung eine Akkusativendung angehängt wird und in anderen nicht.

### Finde eine deutsche Entsprechung!

Versuche für die folgenden Wörter eine deutsche Entsprechung zu finden. Sie muss nicht
unbedingt wörtlich sein, eine sinngemäße Übertragung reicht aus. Analysiere das Wort vorher
(Wortstamm? Nachsilbe? Endung?):

1. peto
2. amika
3. beli
4. ripeto
5. nei
6. nuno
7. nuna
8. petego
9. benkego
10. ina
11. knabo
12. iro

Jetzt wollen wir uns aber endlich wieder Klara und Peter zuwenden. Wenn du Dir die
vorangegangenen Erläuterungen eingeprägt hast, wird es Dir nicht schwerfallen, Klaras Antwort
auf Peters Einladung ins Kino zu verstehen:

### Ĉu kun fremda knabo al kinejo?

![Ĉu kun fremda knabo al kinejo?](kek-3-2.mp3)

* Mi bedaŭras, Peter. Mi ne volas. Mi tute ne konas vin! Mi ne iras kun iu fremda knabo en
kinejon. Ĉu vi ne povas iri kun alia persono?
* Sed ...
* Mi ripetas: Mi atendas mian amikinon Petra. Ni iras al kurso de Esperanto.
* Al kurso de kio?
* De Esperanto. Tio estas lingvo, facila lingvo. Ni estas grupo, kiu lernas Esperanton. Ha,
tie estas mia amikino. Adiaŭ, Peter.
* Klara, kaj ĉu morgaŭ? Bonvolu iri kun mi en la kinejon morgaŭ!
* Ankaŭ morgaŭ mi ne povas ...
* Sed vi devas! Klara, mi petas vin. Morgaŭ, je la sama horo, en la parko!
* Nu, eble. En ordo.
* Ho, Klara, dankegon! Do, ĝis morgaŭ!
* Ĝis!

### NEUE WÖRTER

Folgende Wörter kannst du - unter Beachtung der Wortart (Endungen!) - leicht vom Deutschen
ableiten: bedaŭri, fremda, kurso, grupo, lerni, persono.

* adiaŭ
* al
* alia
* de
* devi
* do
* eble
* el
* facila
* ĝis
* Ha, Ho
* hodiaŭ
* horo
* iu
* je
* tschüss, adieu
* zu, nach
* anderer
* von
* müssen
* also
* vielleicht, möglicherweise
* aus (mi venas el la kinejo)
* leicht, einfach
* bis, bis zu
* Oh (Interjektion, allg. Ausruf)
* heute
* Stunde
* irgendein
* hier: zu, um (zeitl.)
* jes
* kiu
* koni
* lingvo
* morgaŭ
* nu
* ordo
* povi
* sama
* tie
* tio
* tute
* tute ne
* voli
* ja
* (hier:) die, welche
* kennen
* Sprache
* morgen
* na, nun, tja (Interjektion, allg. Ausruf)
* Ordnung
* können
* gleich
* dort
* das
* ganz
* gar nicht, überhaupt nicht
* wollen

Redewendungen:
En ordo. - In Ordnung.
Ĝis! - Tschüss! Mach's gut! Bis dann!
Dankon! - Danke.

## KONTROLLÜBUNGEN FÜR DEINEN MENTOR

Schicke die Lösungen der folgenden Übungen bitte an deinen Mentor.

### Übersetze die folgenden neuen Begriffe auf Esperanto!

Übersetze die folgenden neuen Begriffe auf Esperanto unter Verwendung der neu gelernten
Nachsilben oder durch Veränderung oder Anfügen der Wortartenendungen:
1. Schule (Ort des Lernens)
2. kinderleicht
3. das Bedauern
4. sprachlich (Adjektiv)
5. morgige
6. Pflicht (von „müssen“)
7. der Abschied

### Übersetze die folgenden Sätze ins Deutsche!

1. Ni adiaŭas nian amikinon.
2. La hodiaŭa tago estas belega.
3. Ŝi dankas la amikinon.

### Beginne ein Gespräch!

1. Begrüße jemanden!
2. Sage, wie du heißt!
3. Sage, woher du kommst (aus welcher Stadt)!
4. Frage jemanden, wie es ihm geht!
5. Frage jemanden, ob er mit Dir ins Kino geht!
6. Sage, dass du jetzt in einem Kurs Esperanto lernst!

Damit hast du das Ende von Lektion 1 erreicht. Falls du Fragen zur Lektion hast, schreibe sie
einfach deinem Mentor zusammen mit der Lösung.


Lösung zu 4.

4.1

1. Ich habe einen schönen Freund.
2. Sie bittet den Jungen.
3. Er wartet auf die Schwester.
4. Wir warten mit unserer Freundin.
5. Er geht im Park (umher).
6. Er geht in den Park (hinein).

4.2

1. Bitte
2. freundlich
3. schön sein
4. Wiederholung
5. verneinen
6. Gegenwart
7. jetzig
8. Flehen
9. große Bank
10. weiblich
11. Junge
12. Gang
