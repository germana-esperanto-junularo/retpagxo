---
title: Übungen - Mi celas flue paroli esperanton!
---

#### Lies dir die „Zusammenfassung der Grammatik. Teil I“ gründlich durch!

#### Bilde mit jedem der Fragewörter aus der Tabelle (1. Spalte) einen Fragesatz!

#### Formuliere zu jeder Variante des Satzaufbaus (3.5) zwei Beispielsätze!

#### Übersetze die Beispielsätze aus 3.4-4 und 3.4-5!

### „Mi celas flue paroli esperanton!“

La sekvan matenon Klara leviĝas post kaj mallonga kaj gaja nokto. Ŝi ankoraŭ lacas, sed
ŝi ne volas maltrafi la kurson. Do ŝi iras unue al necesejo. Proksime troviĝas ankaŭ la
duŝoj, kie Klara volas rapide duŝi. Sed kiam ŝi vidas la longegan vicon, ŝi decidas nur
laviĝi kaj brosi dentojn. La duŝado devas atendi, Klara ne havas multe da tempo. Ŝi
ankoraŭ ne faris la hejmtaskojn por la hodiaŭa kurso! Post la lavado ŝi reiras al la ĉambro
por vestiĝi kaj kuras al la matenmanĝo. Ju pli rapide ŝi finmanĝos des pli da tempo restas
por la hejmtaskoj, kiujn ŝi bedaŭrinde ne sukcesis fini hieraŭ pro la amuza
vesperprogramo.
La instruisto bonvenigas la lernantojn kaj deziras agrablan lernadon. Kune la grupo
korektas la hejmtaskajn ekzercojn. Bedaŭrinde Klara ne ĉiam trovis la ĝustan solvon, sed
faris kelkajn stultajn erarojn.* Por la fina ekzameno mi devas pli bone prepariĝi, pensas Klara. Mi ja celas flue
paroli Esperanton!

Feliĉe, ŝi lernis aliajn gravajn vortojn dum la libertempo, kiam ŝi interparolis kun multaj
homoj el diversaj landoj. La tuta aranĝo estas kaj lerniga kaj amuza afero.

### NEUE WÖRTER

* leider - bedaŭrinde
* Dusche - duŝo
* glücklicherweise - feliĉe
* wichtig - grava
* Hausaufgabe - hejmtasko
* Je ... desto - Ju ... des ...
* müde - laca
* etw. waschen - lavi
* verpassen - maltrafi
* Toilette, Klo (wörtlich: nötiger Ort) - necesejo
* Nacht - nokto
* sich anziehen - vestiĝi

Lösungen zu 4.

Für 4.1 und 4.2 sind viele Lösungen möglich. Hier ein Beispiel:

4.1 Kiu estas la knabino en la parko?
4.2 Ni lernas Esperanton. (für Variante 3)

* 4-4 Hier ist der Junge, mit dem deine Tochter getanzt hat. Weißt du nicht, auf was er das Gedicht
geschrieben hat? Auf den Tisch in der Schule!
* 4-5 Hat sie geschrieben, wie es ihr geht? Hast du gehört, wie viele Kinder sie haben?

### KONTROLLÜBUNGEN FÜR DEINEN MENTOR

#### Übersetze den Text der beiden Comics!

![comic](comic.png)

#### Beschreibe auf Esperanto, was du auf dem rechten Bild siehst!

Dabei kannst du folgende Wörter benutzen:
viro, teni, maŝino, mano, diri, kristnasko, komerco
Wenn du andere Wörter verwenden willst, hilft dir vielleicht das Reta Vortaro weiter [reta-vortaro.de](http://www.reta-vortaro.de/)

Leider sind die Esperantosprachigen Asterixbände derzeit vergriffen. Aber keine Sorge, es
gibt ausreichend andere Comics, schau doch einfach mal beim Projekt RoMEo. Du kannst
auch direkt einen Comic als PDF herunterladen: Lasta Lekcio en Gotingeno Die Comics
von nichtlustig.de und andere findest du unter [komiksoj.wordpress.com](http://komiksoj.wordpress.com/)
