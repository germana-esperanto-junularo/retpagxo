---
title: Kostenloser Esperanto-Kurs
content:
    items: '@self.children'
    order:
        by: folder
        dir: asc
hide_next_prev_page_buttons: false
---

Der KEK ist der "Kostenlose Esperanto-Kurs", ein E-Mail-Korrespondenzkurs. Der Kurs besteht insgesamt aus zehn Lektionen, die hier online machen könnt, oder als PDF heruntergeladen werden können. Je nach Neigung kann man die Lektionen ausdrucken oder am Bildschirm lernen. Für die ersten Lektionen stehen die Lektionstexte auch als mp3 bereit, um die Aussprache zu veranschaulichen.

### Ablauf

Jede Lektion enthält einen Text zum Einstieg. Im Anschluss werden die neuen Wörter übersetzt und die neue Grammatik erklärt. Nach einem zweiten Text kommen Kontrollübungen, die du an einen Mentor schicken kannst. Nach den zehn Lektionen brauchst du bloß noch etwas Sprachpraxis und schon kannst du Esperanto. Für Schüler und Studenten ist es allgemein ein realistisches Ziel den Kurs in 15 Wochen zu beenden. Nach Abschluss des Kurses kannst du ein Zertifikat erhalten.
Anmeldung

Wenn du einen Mentor für den Kurs haben willst, melde dich bitte über das Anmeldeformular an. Neben einigen persönlichen Daten kannst du dort auch bereits die Lösungen der ersten Lektion angeben. Es ist daher am besten, wenn du die erste Lektion schon vor der Anmeldiung durchgearbeitet hast. Du findest sie hier zum Herunterladen - wie alle anderen Lektionen auch. Für die Eingabe der Lösungen, lohnt es sich, unsere Hinweise zur Eingabe oder Ersetzung der Sonderzeichen im Esperanto (ĉ, ĝ, ŭ usw.) zumindest zu überfliegen.
Lektionen zum Herunterladen

Bei den ersten drei Lektionen sind zur Verdeutlichung der Aussprache im Esperanto die Lektionstexte auch als mp3 zum Anhören herunterladbar.

* Lektion 1  [PDF](pdf/KEK_lec_01.pdf), Text 1 [mp3](mp3/kek-1-1.mp3), Text 2 [mp3](mp3/kek-1-2.mp3)
* Lektion 2  [PDF](pdf/KEK_lec_02.pdf), Text 1 [mp3](mp3/kek-2-1.mp3), Text 2 [mp3](mp3/kek-2-2.mp3)
* Lektion 3  [PDF](pdf/KEK_lec_03.pdf), Text 1 [mp3](mp3/kek-3-1.mp3), Text 2 [mp3](mp3/kek-3-2.mp3)
* Lektion 4  [PDF](pdf/KEK_lec_04.pdf)
* Lektion 5  [PDF](pdf/KEK_lec_05.pdf)
* Lektion 6  [PDF](pdf/KEK_lec_06.pdf)
* Lektion 7  [PDF](pdf/KEK_lec_07.pdf)
* Lektion 8  [PDF](pdf/KEK_lec_08.pdf)
* Lektion 9  [PDF](pdf/KEK_lec_09.pdf)
* Lektion 10 [PDF](pdf/KEK_lec_10.pdf)

### Kosten und Lizenz

Die Betreuung im Fernkurs ist kostenlos. Wir würden uns aber freuen, wenn du dem Verein etwas spendest, oder Mitglied wirst, wenn dir Esperanto gefällt.

Alle Materialien des KEK stehen unter der Creative-Commons-Lizenz CC-BY..

### Ideen, wie es besser geht?

Wir haben einige Ideen, was man bei dem Fernkurs noch verbessern könnte. Falls du auch welche hast oder mitarbeiten möchtest, schreib Michaela.
