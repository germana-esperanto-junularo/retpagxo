---
title: 'Was ist Esperanto?'
hide_git_sync_repo_link: false
menu: 'Was ist Esperanto?'
image_align: right
---

<link rel="stylesheet" href="/user/themes/quark/css/font-awesome.min.css"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.1/css/brands.css"/>
<link rel="stylesheet" href="./jquery-jvectormap.css"/>
<link rel="stylesheet" href="./jquery-jvectormap.css"/>
<script src="./jquery-jvectormap.min.js"></script>
<script type="text/javascript" src="./jquery-jvectormap-world-mill.js"></script>

<style>
html, body {
  height: 100%;
  margin: 0;
}
#map {
  height: 300px;
  width: 100%;
}

.info { padding: 6px 8px; font: 14px/16px Arial, Helvetica, sans-serif; background: white; background: rgba(255,255,255,0.8); box-shadow: 0 0 15px rgba(0,0,0,0.2); border-radius: 5px; }
.info h4 { margin: 0 0 5px; color: #777; }
.legend { text-align: left; line-height: 18px; color: #555; }
.legend i { width: 18px; height: 18px; float: left; margin-right: 8px; opacity: 0.7; }
.card { margin-top: 20px !important; }
</style>

<div class="container p-centered">

<div class="column" style="margin-top:50px;">
  <div class="hero hero-sm bg-success">
    <div class="hero-body p-centered text-justify">
      <h2>Esperanto ist eine Sprache</h2>
      <p>Die größte Plansprache der Welt</p>
    </div>
  </div>
</div>


<div class="container text-center">
<h2>Und was soll das sein?</h2>
</div>

<div class="container">
<div class="columns">
  <div class="column col-8 col-sm-12 extra-spacing empty">
    <blockquote>Eine Plansprache ist eine von einem Menschen ausgedachte Sprache. Esperanto wurde von Ludwig Zamenhof vor über 130 Jahren erfunden (dem Typen hier rechts). Irgendwie hat Esperanto sich seitdem in jede Ecke der Welt verbreitet.</blockquote>
  </div>
  <div class="column col-4 col-sm-12 extra-spacing" style="display:flex;">
    <img alt="Junulara Esperanto Semajno 2018" class="float-lef" style="align-self: flex-end;" src="/retideoj/gej/user/pages/images/zamenhof_saluton.png">
  </div>
</div>
</div>

<div class="container text-center">
<h2>Und was macht man damit?</h2>
</div>

<div class="container">
<div class="columns">
  <div class="column col-6 col-sm-12 extra-spacing">
      <div class="columns text-left" style="margin-bottom:20px;">
          <div id="item" class="column col-2">
          <svg></svg>
          </div>
          <div id="item" class="column col-10">
            Mit Esperanto kannst du schnell eine neue Sprache lernen. Mit Esperanto kannst du Kontakt zu einer internationalen Gemeinschaft bekommen. Andere Sprachen und Kulturen kennen zu lernen wird so erleichtert.
          </div>
      </div>
      <div class="columns text-left" style="margin-bottom:20px;">
          <div id="item" class="column col-2">
              <i class="fa fa-3x fa-luggage-cart"></i>
          </div>
          <div id="item" class="column col-10">
            Besuch Leute auf der ganzen Welt! <a href="pasportaservo.org">pasportaservo</a> ist ein großes Netzwerk aus Esperanto sprechern Weltweit, die dir ein Stück Zuhause in deinem Urlaub bieten.
          </div>
      </div>
    </div>
  <div class="column col-6 col-sm-12 extra-spacing">
  <div class="columns text-left" style="margin-bottom:20px;">
      <div id="item" class="column col-2">
          <i class="fa fa-3x fa-glass-cheers"></i>
      </div>
      <div id="item" class="column col-10">
        In jedem Land werden regelmäßig Kongresse organisiert, wo die viele Leute aus allen Kulturkreisen treffen kannst. Dazu gibt es meist Konzerte, ein buntes Programm und ein Tourismusprogramm.
      </div>
      </div>
      <div class="columns text-left" style="margin-bottom:20px;">
          <div id="item" class="column col-2">
              <i class="fa fa-3x fa-hand-holding-heart"></i>
          </div>
          <div id="item" class="column col-10">
            In jedem Land werden regelmäßig Kongresse organisiert, wo die viele Leute aus allen Kulturkreisen treffen kannst. Dazu gibt es meist Konzerte, ein buntes Programm und ein Tourismusprogramm.
          </div>
      </div>
  </div>
</div>
</div>

<!--
<div class="container">
  <p>
  Jedoch ist der Ursprung von Esperanto im Gegensatz zu anderen Sprachen sehr klar. Im Jahre 1887 wurde die Sprache vom polnischen Augenarzt in einem Lehrbuch veröffentlicht. Diese Idee hatte zu der Zeit viel Resonanz in erst Zentraleuropa und bald schon der Welt gefunden. Esperanto damals eine Alternative zum grassierenden Imperialismus und Nationalismus auf der Welt, den Menschen wurde eine neutrale Sprache gegeben um miteinander über jegliche Ländergranzen und Barrieren hinweg miteinander kommunizieren zu können.
  </p>

  <p>
  Über die Jahrzehnte hinweg ist Esperanto ein globales Phänomen geworden. Es ist die Plansprache, die von den meisten Menschen auf der Welt gesprochen wird.
  </p>
</div>
-->

<div class="container text-center">
<h2>Wo spricht man das denn?</h2>
<p>Wirklich überall auf der Welt, hier nur ein paar Beispiele</p>
</div>

<div class="columns">
  <div id="item" class="column col-1"></div>
  <div id="item" class="column col-10">
    <div id='map'></div>
  </div>
  <div id="item" class="column col-1"></div>
</div>

<div class="container text-center">
<h2>Wie soll eine künstliche Sprache bitte sein?</h2>
</div>

<div class="columns">
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card">
    <div class="card-header bg-success text-center">
      <div class="card-title h5">Gleich und Gerecht</div>
    </div>
    <div class="card-body text-justify">
      Esperanto ist eine wirklich internationale Sprache und bevorzugt als Weltsprache keine Nation.
    </div>
  </div>
  </div>
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card">
    <div class="card-header bg-success text-center">
      <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-book" width="64" height="64" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <path d="M3 19a9 9 0 0 1 9 0a9 9 0 0 1 9 0" />
        <path d="M3 6a9 9 0 0 1 9 0a9 9 0 0 1 9 0" />
        <line x1="3" y1="6" x2="3" y2="19" />
        <line x1="12" y1="6" x2="12" y2="19" />
        <line x1="21" y1="6" x2="21" y2="19" />
      </svg>
      <div class="card-title h5">Einfach zu lernen</div>
    </div>
    <div class="card-body text-justify">
      Esperanto lernt sich viel schneller als andere Sprachen, da es komplett regelmäßig und logisch ist.
    </div>
  </div>
  </div>
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card">
    <div class="card-header bg-success text-center">
      <i class="fas fa-3x fa-globe-europe"></i>
      <div class="card-title h5">International</div>
    </div>
    <div class="card-body text-justify">
      Mit Esperanto kannst du auf der ganzen Welt kommunizieren, Sprecher gibt es wirklich überall.
    </div>
  </div>
  </div>
</div>

<div class="container text-center">
<h2>Was sagen die Leute?</h2>
</div>

<div class="columns">
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card" style="height:100%;">
    <div class="card-body text-justify">
      Ich mag Esperanto. Ich mag Esperanto. Ich mag Esperanto.Ich mag Esperanto. Ich mag Esperanto.
        Ich mag Esperanto. Ich mag Esperanto. Ich mag Esperanto.Ich mag Esperanto. Ich mag Esperanto.
    </div>
    <div class="card-footer">
      <i class="fa fa-2x fa-quote-left"></i>
      <i>Paul</i>
      <i class="fa fa-2x fa-quote-right"></i>
      <figure class="avatar avatar-xl">
          <img src="/retideoj/gej/user/pages/images/paul_konterfei.jpg" alt="paul">
      </figure>
    </div>
  </div>
  </div>
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card" style="height:100%;">
    <div class="card-body text-justify">
      Ich mag Esperanto auch. Ich mag Esperanto auch. Ich magEsperanto auch. Ichmag Esperanto auch. Ich mag Esperanto auch.
      Ich mag auchEsperanto. Ich magauch Esperanto.
    </div>
    <div class="card-footer">
      <i class="fa fa-2x fa-quote-left"></i>
      <i>Julia</i>
      <i class="fa fa-2x fa-quote-right"></i>
      <figure class="avatar avatar-xl">
          <img src="/retideoj/gej/user/pages/images/julia_konterfei.jpg" alt="julia">
      </figure>
    </div>
  </div>
  </div>
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card" style="height:100%;">
    <div class="card-body text-justify">
      Esperanto ist wirklich sehr gut. Esperanto ist wirklich sehr gut.Esperanto ist wirklich sehr gut. Esperanto ist wirklich sehr gut.
      Esperanto ist wirklich sehr gut.Esperanto ist wirklich sehrgut. Esperanto ist wirklich sehr gut.
    </div>
    <div class="card-footer">
      <i class="fa fa-2x fa-quote-left"></i>
      <i>Alina</i>
      <i class="fa fa-2x fa-quote-right"></i>
      <figure class="avatar avatar-xl">
          <img src="/retideoj/gej/user/pages/images/alina_konterfei.jpg" alt="alina">
      </figure>
    </div>
  </div>
  </div>
</div>

<div class="container text-center" style="margin-top:60px">
<h2>Triff den Vorstand</h2>
</div>

<div class="columns">
  <div id="item" class="column col-3 col-sm-6 extra-spacing">
  <div class="card" style="max-width:180px;height:100%;">
    <div class="card-image p-centered">
        <img src="/retideoj/gej/user/pages/images/lars.jpg" class="img-responsive">
    </div>
    <div class="card-body">
      <b>Lars Hansen</b><br />
      <small>Vizepräsident, Schatzmeister</small>
    </div>
  </div>
  </div>
  <div id="item" class="column col-3 col-sm-6 extra-spacing">
  <div class="card" style="max-width:180px;height:100%;">
    <div class="card-image p-centered">
        <img src="/retideoj/gej/user/pages/images/devid.png" class="img-responsive">
    </div>
    <div class="card-body">
      <b>Devid Mamsch</b><br />
      <small>Eventorganisation</small>
    </div>
  </div>
  </div>
  <div id="item" class="column col-3 col-sm-6 extra-spacing">
  <div class="card" style="max-width:180px;height:100%;">
    <div class="card-image p-centered">
        <img src="/retideoj/gej/user/pages/images/konstanze.png" class="img-responsive">
    </div>
    <div class="card-body">
      <b>Konstanze Schönfeld</b><br />
      <small>Internationales</small>
    </div>
  </div>
  </div>
  <div id="item" class="column col-3 col-sm-6 extra-spacing">
  <div class="card" style="max-width:180px;height:100%;">
    <div class="card-image p-centered">
        <img src="/retideoj/gej/user/pages/images/michaela.png" class="img-responsive">
    </div>
    <div class="card-body">
      <b>Michaela Stegmaier</b><br />
      <small>Präsidentin</small>
    </div>
  </div>
  </div>
</div>
</div>

<div class="container text-center" style="margin-top:60px">
<h2>Lust bekommen?</h2>
<p>Dann nehm dir doch mal ein Wochenende und fang einen unserer (betreuten) Online-Kurse an.</p>


<div class="columns">
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card" style="min-height:100%;">
    <div class="card-body text-justify">
      <a href="https://learn.esperanto.com/">Esperanto in 12 Tagen</a> ist ein Selbstlernkurs nach der Zagreb-Methode.
      In weniger als zwei Wochen kannst du mit ihm schnell ein gutes Gesprächsniveau erreichen.
      Er ist in über 20 Sprachen verfügbar.
    </div>
  </div>
  </div>
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card" style="min-height:100%;">
    <div class="card-body text-justify">
      Der <a href="/was_ist_esperanto/kosteloser_esperantokurs">Kostenlose Esperantokurs</a> ist unser Angebot für euch, mit einem Privatlehrer zusammen Esperanto zu lernen.
      Die Lektionen sind online und für Fragen und Korrekturen stehen dir unsere Lehrer zu Verfügung
    </div>
  </div>
  </div>
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card" style="min-height:100%;">
    <div class="card-body text-justify">
          Es gibt auch noch <a href="http://www.duolingo.com/courses/eo">Duolingo</a>, den  Kurso de Esperanto (kurz <a href="http://www.kurso.com.br/index.php?de">KDE</a>) und die Internetseite <a href="http://www.lernu.net">lernu.net</a>. Zum weiterlernen und lesen bietet sich die Nachrichtenseite vom Weltesperantobund (Universala Esperanto Asocio, UEA) mit einfachen Texten an <a href="http://www.uea.facila.org">uea.facila.org</a>.
    </div>
  </div>
  </div>
</div>

<div class="text-large" style="margin-top:30px;">
  Oder komm auf eines unserer Treffen vorbei.
</div>

<div class="columns">
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card" style="min-height:100%;">
    <div class="card-header text-center">
      <figure class="figure">
        <img class="img-responsive" src="/retideoj/gej/user/pages/images/JES-emblemo.png" style="margin-top:30px;">
     </figure>
    </div>
    <div class="card-body text-justify">
      Der Höhepunkt des Jahres, zusammen organisiert von der deutschen und polnischen Esperantojugend. Unser Silvestertreffen, das <a href="jes.pej.pl">Junulara E-Semajno</a>, kurz JES.
    </div>
  </div>
  </div>
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card" style="min-height:100%;">
    <div class="card-header text-center">
     <figure class="figure">
       <img class="img-responsive" src="/retideoj/gej/user/pages/images/eventaservo.png">
    </figure>
    </div>
    <div class="card-body text-justify">
      Auf <a href="eventaservo.org">eventaservo.org</a> kannst du dich über viele Esperantoveranstalltungen und Events weltweit informieren.
    </div>
  </div>
  </div>
  <div id="item" class="column col-4 col-sm-12 extra-spacing">
  <div class="card" style="min-height:100%;">
    <div class="card-header text-center">
     <figure class="figure">
       <img class="img-responsive" src="/retideoj/gej/user/pages/images/logo-kekso.png" style="margin-top:30px;">
    </figure>
    </div>
    <div class="card-body text-justify">
      Wenn du noch kein Esperanto spricht, oder einfach ein Wochenende mal was anderes machen willst, ist das <a href="/de/kekso">KEKSO</a> genau das richtige für dich!
    </div>
  </div>
  </div>
</div>

</div>

<script type="text/javascript" src="/user/pages/01.was_ist_esperanto/eorgas.js"></script>
<script type="text/javascript" src="/user/pages/01.was_ist_esperanto/events.js"></script>
<script type="text/javascript" src="/user/pages/01.was_ist_esperanto/pasportaservantoj.js"></script>
<script type="text/javascript">
//$(function(){
  var map,
      markers = [],
      cityAreaData = [],
      type2intClass = {
        "org":1,
        "gastiganto":2,
        "evento":3,
      };
      //gastigantoj={"crs": {"properties": {"type": "proj4", "href": "http://spatialreference.org/ref/epsg/4326/"}, "type": "link"}, "type": "FeatureCollection", "features": [{"id": 832, "properties": {"url": "/ejo/832/", "owner_name": "Rogier", "model": "maps.plottableplace", "city": "Utrecht"}, "type": "Feature", "geometry": {"type": "Point", "coordinates": [5.10, 52.07]}}
      for (m of gastigantoj["features"]) {
        if(m.geometry){
          markers.push({latLng: [m["geometry"]["coordinates"][1], m["geometry"]["coordinates"][0]], status: "Gastgeber", status:"gastiganto", style: {fill: 'orange', r: 2}})
        }
      }
      for (m of eorgas) {
        markers.push({latLng: [m["Koord"][1],m["Koord"][0]], name: m["Nomo"], status:"Verein", style: {fill: 'red', r: 2}})
      }
      for (m of eventoj) {
        //console.log({latLng: [m["latitudo"],m["longitudo"]], name: m["titolo"], status:"evento", style: {fill: 'blue', r: 2}})
        markers.push({latLng: [m["loko"]["latitudo"],m["loko"]["longitudo"]], name: m["titolo"], status:"Event", style: {fill: 'blue', r: 2}})
      }
  map = new jvm.Map({
    container: $('#map'),
    map: 'world_mill',
    regionsSelectable: false,
    markersSelectable: false,
    backgroundColor: "#006f00",
    markers: markers,
    series: {
      markers: [{
            attribute: 'r',
            scale: [0, 3],
            values: cityAreaData
          },
          {
            attribute: 'fill',
            scale: {
              "Verein": "red",
              "Gastgeber": "orange",
              "Event": "blue"
            },
            values: markers.reduce(function(p,c,i) {
                p[i] = c.status;
                return p;
            }, {}),
            values: cityAreaData,
            legend: {
              vertical: true,
            }
      }]
    },
    onRegionSelected: function(){
      if (window.localStorage) {
        window.localStorage.setItem(
          'jvectormap-selected-regions',
          JSON.stringify(map.getSelectedRegions())
        );
      }
    },
    onMarkerSelected: function(){
      if (window.localStorage) {
        window.localStorage.setItem(
          'jvectormap-selected-markers',
          JSON.stringify(map.getSelectedMarkers())
        );
      }
    }
  });
  map.setSelectedRegions( JSON.parse( window.localStorage.getItem('jvectormap-selected-regions') || '[]' ) );
  map.setSelectedMarkers( JSON.parse( window.localStorage.getItem('jvectormap-selected-markers') || '[]' ) );
//});
</script>
