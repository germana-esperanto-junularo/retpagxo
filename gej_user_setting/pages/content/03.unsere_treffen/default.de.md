
---
title: Unsere Treffen
menu:  Unsere Treffen
taxonomy:
    tag:
    category:
---

## KEKSO

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      KEKSO, mallonge por "Kreema Esperanto-KurSO", estas renkontiĝo organizata de Germana Esperanto Junularo. Ĝi okazas dufoje jare, dum printempa kaj aŭtuna semajnfinoj. La loko ŝanĝiĝas ĉiufoje, por ke la renkontiĝoj okazu en la tuta Germanio. Ĝis estis vizititaj: Rostock, Potsdam, Soest, Karlsruhe aŭ Hamburgo. La Esperanto-renkontiĝo havas 15 ĝis 20 partoprenantojn. En 2019 KEKSO festis sian 25-an jubileon.
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <figure class="figure">
      <img class="img-responsive" src="/gej/user/images/KEKSO_Logo.png">
   </figure>
  </div>
</div>

## JES

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <figure class="figure">
  <img class="img-responsive" src="/gej/user/images/jes.png">
  </figure>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      La Junulara E-Semajno (JES) estas novjara junulara esperanto-aranĝo organizata de Pola Esperanto Junularo (PEJ) kaj Germana Esperanto-Junularo (GEJ). Ĝi okazas ĉiujare dum lastaj tagoj de decembro kaj unuaj tagoj de januaro, proksimume dum unu semajno, en iu loko en Meza Eŭropo.
  </div>

</div>
