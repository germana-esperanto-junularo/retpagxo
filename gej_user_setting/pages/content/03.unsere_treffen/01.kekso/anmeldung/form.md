---
title: 'Anmeldung KEKSO'
form:
    name: contact-form
    fields:
        -
            name: name
            label: Name
            placeholder: 'Dein Name/Via Nomo'
            type: text
            validate:
                required: true
        -
            name: email
            label: Email
            placeholder: 'Deine Email-Adresse/Via retpoŝtadreso'
            type: email
            validate:
                required: true
        -
            name: nakigxtago
            type: date
            label: 'Dein Geburtsdatum/Via naskiĝdato'
            default: '2015-01-01'
            validate:
                min: '1985-01-01'
                max: '2015-13-13'
                required: true
        -
            name: keksoj
            type: radio
            label: 'Kekse/ Keksoj'
            options:
                keksoj: 'Ich bringe selbst Obst und Kekse mit/Mi mem kunportos fruktojn kaj keksojn'
                keksotakso: 'Ich zahle 10 Euro mehr und bringe kein Obst und keine Kekse mit/Mi pagos pliajn 10 eŭrojn kaj ne mem kunportos fruktojn kaj keksojn'
            validate:
                required: true
        -
            name: lingonivelo
            label: 'Deine Sprachniveau/Via lingvonivelo'
            placeholder: Auswählen/Elektu
            type: select
            options:
                - fließend/flue
                - 'geht so/meze'
                - 'ein bisschen/iomete'
                - Anfänger/komencanto
            validate:
                required: true
        -
            name: diät
            label: Diät
            placeholder: Auswählen/Elektu
            type: select
            options:
                - 'Auch Fleisch/Ankaŭ vianda'
                - Ovo-Lakto-Vegetarisch/Laktaĵ-vegetarana
                - Laktosefrei/Senlaktoza
                - Vegan/Vegana
                - Diabetisch/Diabeta
                - Koscher/Koŝera
                - 'Anderes (bitte im Kommentar spezifizieren)/Alie (bonvole klarigu en la komentaro)'
            validate:
                required: true
        -
            name: monkontribuo
            label: 'Teilnahmebeitrag/ Partoprenkotizo'
            placeholder: Auswählen/Elektu
            type: select
            options:
                - 'Das ist mein erstes Treffen, muss also  0 Euro zahlen/ Temas pri mia unua renkontiĝo - mi devas pagi 0 eŭrojn'
                - 'Ich komme aus dem Ausland, muss also  0 Euro zahlen/ Mi venas el eksterlando - mi devas pagi 0 eŭrojn'
                - 'Ich bin Mitglied der DEJ, muss also 30 Euro zahlen/ Mi estas membro de GEJ - mi devas pagi 30 eŭrojn'
                - 'Nichts der obigen - ich muss 45 Euro zahlen/ Nenio supre menciita - mi devas pagi 45 eŭrojn'
            validate:
                required: true
        -
            type: radio
            default: null
            label: 'Persönliche Daten/ Personaj datumoj'
            markdown: true
            options:
                - Ja./Jes.
                - Nein./Ne.
        -
            type: display
            size: medium
            label: ""
            markdown: true
            content: 'Ich bin damit einverstanden, dass während der Veranstaltung Fotos von mir gemacht und später gegebenenfalls in der Mitgliederzeitschrift, auf der Website und/oder in sozialen Netzwerken veröffentlicht werden dürfen. --- Mi konsentas ke dum la eventoj mi povos esti fotita kaj ke tiuj fotoj poste povos esti publikitaj en nia magazino, la retpaĝo kaj/aŭ en en sociaj retoj.'
        -
            type: radio
            default: null
            label: Fotos/Fotoj
            markdown: true
            options:
                - Ja./Jes.
                - Nein./Ne.
        -
            type: display
            size: medium
            label: ""
            markdown: true
            content: 'Ich bin damit einverstanden, dass während der Veranstaltung Fotos von mir gemacht und später gegebenenfalls in der Mitgliederzeitschrift, auf der Website und/oder in sozialen Netzwerken veröffentlicht werden dürfen. \n Mi konsentas ke dum la eventoj mi povos esti fotita kaj ke tiuj fotoj poste povos esti publikitaj en nia magazino, la retpaĝo kaj/aŭ en en sociaj retoj.'
        -
            type: textarea
            autofocus: true
            label: 'Bemerkungen (Programmbeiträge, Hinweise, medizinische Besonderheiten)/ Komentoj (programkontribuoj, sciindaĵoj, kuracaj apartaĵoj)'
    buttons:
        -
            type: submit
            value: Anmelden
    process:
        -
            email:
                from: '{{ config.plugins.email.from }}'
                to:
                    - '{{ config.plugins.email.to }}'
                    - '{{ form.value.email }}'
                subject: '[Feedback] {{ form.value.name|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: Danke!
        -
            display: angemeldet
---

## Anmeldung zum KEKSO
