---
title: Übungen - Mi preferas foriri de ĉi tie
---

### „al“ und „de“

Übersetze folgende Wortbildungen mit Hilfe der Präpositionen „al“ oder „de“
Beispiel:
zur Schule gehen - iri al la lernejo
Peters Adresse - la adreso de Peter
1. der Beginn des Tages
2. das Treffen von gestern
3. zum Park kommen
4. die Musik des Films
5. weil es uns gefällt

### Zeitformen der Verben

Übersetze wieder ins Esperanto. Beachte dabei die richtige Zeitform.

1. ich war
2. du kommst
3. er erreichte
4. der Film wird gefallen
5. wir werden hören
6. wir begleiten
7. ihr werdet rennen
8. sie diskutieren
9. sie kennen
10. er ging los

### Mi preferas foriri de ĉi tie

![peter devas atendi](kek-2-2.mp3)

Peter estas ege kontenta. Li nun iras kun bela knabino en kinejon. La filmo estos bona,
ankaŭ la muziko de la filmo. Kaj li povos esti sola kun Klara, sen ŝia amikino Petra. Sed ...
Ili atingas la kinejon.* Klara, ĉu mi ne povas esti sola kun vi?

* Peter, bonvolu saluti mian amikinon. Tiu ĉi estas Petra. Kaj tie staras mia tuta familio!
* Ho, Klara, sed ...
* Tio estas mia patrino kaj mia patro, tie vi vidas mian onklon kun filino kaj filo. La onklo malbone aŭdas. Cetere, la filino de la onklo nomiĝas Eva! Tiu alta virino estas la edzino de la onklo, mia onklino Marta. La alia viro, tiu dika kun la granda nazo, estas mia avo. Mi estas lia nepino. Kaj tie staras mia malgranda frato.

Peter salutas la familion kaj la amikinon de Klara. La onklo demandas:

* Klara, kiu li estas?
* Li nomiĝas Peter. Kaj mi renkontis lin en parko, onklo.

La onklo ne tute bone aŭdis:* Sen pantalono? Ĉu?

* Ne, vi malbone aŭdis; ne sen pantalono, sed en parko mi renkontis lin. En la parko,
onklo!

Peter vidas ankoraŭ alian knabinon, kiu plaĉas al li.* Klara, kiu estas tiu ĉi persono, la knabino kun la bela vizaĝo?

* Kara Peter, ŝi ne estas el nia familio. Ni nun devas iri en la kinejon.

Nun venas la patro al Peter.
Sinjoro Peter! La amiko de nia filino estas ankaŭ nia amiko. Vi estas nia gasto kaj ni
pagos por vi. Cetere, la titolo de la filmo estas: „La gasto de la familio“. Do, ni nun
ekiras ...

Post la filmo Klara vidas, ke Peter jam estas for. Sur lia seĝo estas papereto. Ŝi prenas la
paperon en la manon. Ĝi estas letero de Peter. Li skribis:
„Kara Klara! Pardonu, ke mi foriris. Tio estas tre malĝentila de mi. Via familio estas tre
granda kaj interesa, sed mi preferas esti sola kun vi. Cetere, vi portis belan robon! Kun
amika saluto, ĝis morgaŭ. Via Peter.“

Eine wirklich schwachsinnige Geschichte, nicht wahr? Bevor Du Peter allzu sehrbedauerst, kommen wir lieber zu den neuen...

6. VOKABELN

Die folgenden Wörter kannst Du sicher wieder selbst aus dem Deutschen ableiten:

familio, onklo, dika, persono, gasto. Beachte die Wortart.

* alia - andere
* alta - hoch, groß (Höhe)
* antaŭ - vor
* avo - Großvater
* cetere - übrigens
* Ĉu? - hier: wirklich?
* demandi - fragen
* edzo - Ehemann
* el - aus
* filo - Sohn
* for - weg; weit entfernt
* foriri - weggehen
* frato - Bruder
* granda - groß
* ho - oh (Ausruf)
* jam - schon
* kara - lieber, liebe; teuer
* ke - dass
* letero - Brief
* mano - Hand
* nazo - Nase
* nepo - Enkel
* pagi - (be-)zahlen
* pantalono - Hose
* papero - Papier
* papereto - Zettel, kleines Papier
* patro - Vater
* porti - tragen
* preferi - bevorzugen
* preni - nehmen
* robo - Kleid
* seĝo - Stuhl
* sen - ohne
* sinjoro - Herr (auch als Anrede)
* skribi - schreiben
* stari - stehen (mi staras – ich stehe)
* tie ĉi / ĉi tie - hier
* titolo - Titel
* tiu - jener
* tiu ĉi / ĉi tiu - dieser
* viro - Mann
* vizaĝo - Gesicht


### Lösungen zu 4.

4.1

1. La komenco de la tago
2. La renkonto de hieraŭ
3. Veni al la parko
4. La muziko de la filmo
5. Ĉar ĝi plaĉas al ni

4.2

1. Mi estis
2. Vi venas
3. Li atingis
4. La filmo plaĉos
5. Ni aŭdos
6. Ni akompanas
7. Vi kuros
8. Ili diskutas
9. Ili konas
10. Li ekiris

## KONTROLLÜBUNGEN FÜR DEINEN MENTOR

### Übersetze folgende Sätze!

1. Er ist schon weggegangen.
2. Was hat sie gefragt? Hier steht das Fragewort im Akkusativ
3. Er wartet auf sie. Auf Esperanto: Er erwartet sie.
4. Sie nimmt seine Hand.
5. Was schreibst du? Gefragt wird nach dem Akkusativobjekt!
6. Wen siehst du lieber? wörtlich: Wen bevorzugst du zu sehen?

### Übersetze mit Wortsilben

Versuche, nur mit den dir schon bekannten Wortbildungssilben (-eg, -ul, …) und den
verschiedenen Endungen für Wortarten (-o, -a, -e, -i) folgende Wörter zu übersetzen:
Beispiel: ega - groß, egi - groß sein, ego - das Große
1. Person
2. starten
3. weibliche Person
4. Beginn
5. Gegenteil
6. gegenteilig
7. Ort
8. sehr

### Übersetze ins Deutsche:

1. postmorgaŭ
2. antaŭhieraŭ

### Schreibe einem Bekannten, ...* dass Du einen interessanten Film im Kino gesehen hast,

* dass Du nach dem Film mit Deinem Freund über ihn diskutiert hast,
* dass Dein Freund den Film schon kannte,
* dass Du morgen Gast sein wirst in seiner Familie.
