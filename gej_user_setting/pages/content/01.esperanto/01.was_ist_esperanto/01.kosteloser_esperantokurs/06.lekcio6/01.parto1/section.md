---
title: Teksto -  Varma vetero hodiaŭ, ĉu ne?
---

* „Varma vetero hodiaŭ, ĉu ne?“

Aĥ, pensas Klara, ĉiam konversacioj komenciĝas per tiu ĉi aŭ simila frazo. Klara nun sidas
en ĝardeno, post la matena Esperanto-kurso, kaj ĝuas la belan naturon ĉirkaŭe. De tempo
al tempo aperas birdoj. Apud ŝi sidas ĉirkaŭ dudekjara knabino, kiu deziras konversacii
kun ŝi. La knabino denove ekparolas:

* „De kie vi venas? Ĉu ankaŭ el Polujo?“

Klara rigardas la knabinon, kiu ŝajnas esti afabla kaj bonkora (bon-kor-a). Klara kapneas
(kap-ne-as).

* Ne, mi venas el Germanujo. Mi estas Klara. Kiel vi nomiĝas?
* Agnjeŝka.
* …...? Kiel?
* Rigardu, jen mia nomŝildo.

Klara legas la nomon sur ŝia nomŝildo.* Ho! Mi ja komprenas... Kion vi faris hodiaŭ?

* Mi partoprenis prelegon pri Esperantlingva literaturo, respondas Agnjeŝka. Antaŭ ne longe oni tradukis faman svedan romanon por infanoj: „Pipi Ŝtrumpolonga“. Kaj ĉu vi scias ke eĉ ekzistas komiksoj en Esperanto?
* Ĉu vere? Mi ŝategas komiksojn!
* Ankaŭ mi! Rigardu, tiu ĉi apartenas al mia instruisto, li prunte donis ĝin al mi.

Klara prenas la libron, kies nomo estas „Asteriks ĉe la
Olimpiaj ludoj“. Klara kaj ŝia najbarino samtempe legas la
unuajn paĝojn. Iom poste, Klara rigardas la horloĝon:

* Ho, jam estas la kvina! Mi planis partopreni la koktelistan kurson. Ĉu ankaŭ vi?
* Jes, mi volonte venos. Poste ni povos plulegi.

### NEUE WÖRTER

Zum Selberherausfinden: varma, naturo, literaturo, romano, aŭtoro, originala, diversa, kontroli.

* aparteni - gehören
* aperi - erscheinen
* ĉirkaŭe - in der Umgebung, ringsherum
* fama - berühmt
* horloĝo - Uhr
* kapnei - verneinend den Kopf schütteln
* kapo - Kopf
* kien - wohin
* koktelo - Cocktail, Mischgetränk
* najbaro - Nachbar
* nomŝildo - Namensschild
* pensi - denken
* Pipi Ŝtrumpolonga - Pipi Langstrumpf
* Polujo - Polen
* simila - ähnlich
* sveda - schwedisch
* tempo - Zeit
* traduki - übersetzen
