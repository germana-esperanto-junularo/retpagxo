---
title: "Grammatik"
---

### (Wiederholung:) Die Endung -n als Kennzeichen für eine Richtungsangabe

* Berlinon - nach Berlin
* urben - in die Stadt

Wird das Wort oder die Wortgruppe, die die Richtung anzeigt, durch eine Präposition
eingeleitet, so steht die Richtungsendung -n nur nach solchen Präpositionen, die
zweideutig sind, d.h. sowohl einen Ort als auch eine Richtung bezeichnen können.

Diese zweideutigen Präpositionen sind: antaŭ (malantaŭ), apud, ĉirkaŭ, ekster, en, inter,
post, sub, super, sur, tra, trans.

Immer ohne nachfolgende Richtungsendung -n, da bezüglich der Richtung immer
eindeutig in ihrer Bedeutung sind die Präpositionen: al, ĉe, ĝis, laŭ, preter.

Beispiele:

* zweideutige Präpositionen: Ort (wo?) Richtung (wohin?)
  * esti antaŭ la urbo / veturi antaŭ la urbon
  * stari apud la ŝranko / iri apud la ŝrankon
  * kreski ĉirkaŭ la domo / kuri ĉirkaŭ la domon
  * sidi ekster la ĝardeno / veni ekster la ĝardenon
  * lerni en la lernejo / iri en la lernejon
  * stari inter la aŭtoj / veturi inter la aŭtojn
  * stari post la knabino / kuri post la knabinon
  * serĉi sub la tablo / ĵeti sub la tablon
  * pendi super la ŝranko / pendigi super la ŝrankon
  * kuŝi sur la seĝo / meti sur la seĝon
  * iri tra parko (kreuz und quer) / iri tra parkon (gerade hindurch)
  * stari trans la strato / iri trans la straton
* eindeutige Präpositionen: wo? wohin/woher?
  * stari ĉe la muro / iri al teatro
  * kuri laŭ la strato / kuri ĝis la fino
  * iri preter la domo / veni el la domo

Natürlich können alle diese Präpositionen auch als Vorsilbe für Verben (u.a. Wortarten)
verwendet werden. Wenn nach diesen Verben die Präposition nicht wiederholt wird und
auch keine andere Präposition steht, dann folgt in jedem Fall der Akkusativ:

iri laŭ la konstruaĵo - laŭiri la konstruaĵon - laŭiri laŭ la konstruaĵo (entlang des
Gebäudes gehen)

veni el la ĉambro - elveni la ĉambron - elveni el la ĉambro (aus dem Zimmer kommen)

### Beachte den Akkusativ nach „kiel“

La estro bonvenigis lin kiel amiko. - Der Leiter begrüßte ihn wie ein Freund: Hier bezieht
sich „amiko“ auf „estro“.
La estro bonvenigis lin kiel amikon. - Der Leiter begrüßte ihn wie einen Freund: Hier
bezieht sich „amikon“ auf das Akkusativobjekt „lin“ und erhält daher auch die
Akkusativendung -n.

### Die Präposition „je“

Die Präposition „je“ wird normalerweise immer dann gebraucht, wenn keine andere
Präposition passt. Sie selbst hat keine besondere Bedeutung:
Je kiu horo li alvenis? - Zu welcher Uhrzeit kam er an?
Je via sano! - Auf Dein Wohl!

Wortbildung:

Präfixe:

* pra- (Ur-): praavo, pranepo, prahomo
* dis- (auseinander, zer-): disfali, disvastigi
* bo- (Verwandtschaft durch Heirat): bofrato (Schwager), bopatrino (Schwiegermutter)
* eks- (Ex-, ehemalig): eksedzo, eksĉefo
* fi- (bringt eine moralisch verächtliche Meinung des Sprechers zum Ausdruck): fihomo/fiulo (Lump). fi! (pfui!)

Suffixe:

* -ind- (-wert, -würdig): salutinda (begrüssenswert), mirinda (bewundernswert)
* -estr- (-leiter, -chef): fakestro (Abteilungsleiter), lernejestro (Schulleiter)
* -op- (an Zahlwörter angehängt: zu wievielt): kvarope (zu viert), kvaropo (eine vierköpfige Gruppe), unuopa (einzeln), unuopulo (einzelner)
* -em- (Neigung zu ..., Lust auf ...): laborema (arbeitsam), parolema (gesprächig)
* -ec- (betont die Eigenschaft): bona (gut) - boneco (Güte); beleco (Schönheit)
