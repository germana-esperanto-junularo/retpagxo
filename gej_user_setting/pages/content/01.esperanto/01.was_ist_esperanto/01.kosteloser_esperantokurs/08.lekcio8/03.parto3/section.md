---
title: Übungen - Muziko en esperanto
---

#### Bilde aus zehn esperantosprachigen Beispielen aus 3.1. je einen Satz und übersetze ihn ins Deutsche.

Wenn du den folgenden Text am Bildschirm liest, kannst du die unterstrichenen
Verknüpfungen anklicken und bei geeigneter Internetverbindung die entsprechenden
Lieder gleich auf youtube anhören.

####  Muziko en esperanto

Aŭskultinte kelkajn anekdotojn kaj rakontojn, Peter pluesploras la klubejon.
En angulo li vidas aron de kompaktdiskoj kun Esperanto-muziko.

* Klara, vi ne diris al mi, ke en Esperanto ekzistas ankaŭ muziko!
* Peter, mi ankaŭ ne diris al vi, ke en Esperanto ekzistas poemoj.
* Ah, ĉu ekzistas?
* Certe, muziko ja estas ofte kantata poemo, ĉu ne? Ekzistas eĉ diskeldonejo, kiu eldonas nur esperantlingvan muzikon. Ĝi nomiĝas vinilkosmo. Sed eble ni simple aŭskultu iomete. Jen estas iom trista kanto, kiun mi ŝatas. Mi plibonigis mian Esperanton per ĝi, ĝi estas ne tiom malfacile komprenebla. Aŭskultu!

> Berlino sen vi

> Se vi dirus tion pli frue,
eble mi irus ĝis la stacidom'
repreni mian monon forgesi pri la vojo.
Se vi dirus tion pli klare,
eble mi ne agus kiel metronom'
kaj aŭskultus vian voĉan sonon por la unua fojo

> Berlino sen vi estas urb' sen harmoni'
estas trista trista amasloĝej'.
Berlino sen vi lasas min sen energi'
en la trista trista amasloĝej'.

> Se vi dirus tion pli frue, eble mi irus ĝis la flughaven'
por aĉeti biletojn por ambaŭ ni.
Se vi dirus tion pli klare eble mi vivus sen tiu ĉagren',
nun aŭskultus vian voĉon kun emoci'.

> Ho mia pejzaĝ' de trista amasloĝej'.
Urba vizaĝ' neniam iĝos la hejm'
de mia plej kara dezir'

> Se vi dirus tion pli frue eble mi restus en tiu vilaĝ'
kaj ne indus komenti kial Petro iris for.

Ekzistas multaj aliaj kantoj en Esperanto, kaj modernaj kaj malnovaj. En
youtube vi povas spekti kelkajn kun teksto. Verŝajne kelkajn vortojn vi ne
konas, sed ne gravas, se vi ne ĉion komprenas. Ĉio ĉi simple informu vin pri
la granda diverseco de Esperanto-muziko. Jen eta superrigardo. Se vi
volas, vi povas ankaŭ aŭskulti la kanton per la interreto, alklakante la ligilojn
en la pdf-dosiero.

* La kanto Berlino sen vi estas de inicialoj DC. Li ankaŭ faras pli gajajn kantojn, ekzemple Kurso de Esperanto
* Persone estas unu el la plej famaj Esperanto-rokgrupoj. Unu tre konata kanto de ili estas Liza Pentras Bildojn.
* La Perdita Generacio estas idealistoj el Svedujo, kiuj kritikas troan komercemon kaj propagandas amon. Jen tre kaj eble tro dolĉa amokanto de ili.
* Nova Kanto estas „verda“ hiphopo de Tone el Brazilo, kiu entuziasme prikantas Esperanton. Sed ekzistas ankaŭ la malo, nome „krima“ hiphopo de la franca grupo La Pafklik', kiu primokas iom Esperantistojn.

#### NEUE WÖRTER

* amasloĝejo - Massenunterkunft, Turnhalle
* ambaŭ - beide
* aŭskulti - anhören, zuhören
* bileto - Fahrkarte
* ĉagreno - Ärger
* deziro - Wunsch
* diskeldonejo - Plattenfirma
* dolĉa - süß
* ekrano - Bildschirm
* emocio - Gefühl
* entuziasma - enthusiastisch, begeistert
* esplori - erforschen
* fama - berühmt
* flughaveno - Flughafen
* generacio - Generation
* hejmo - Heim
* kanto - Lied
* klaki - klicken
* komenti - kommentieren
* lasi - etw. oder jmd. Lassen
* ligi - verbinden
* ligilo - Link, Verknüpfung
* pejzaĝo - Landschaft
* pentri - malen
* primoki - sich lustig machen über
* sono - Klang
* stacidomo - Bahnhof
* superrigardo - Überblick
* trista - traurig
* urbo - Stadt
* verda - grün
* vilaĝo - Dorf
* vizaĝo - Gesicht
* voĉo - Stimme

### KONTROLLÜBUNGEN FÜR DEINEN MENTOR

#### Traduko - Übersetze nur unter Verwendung und evtl. Kombination* der Präfixe pra-, dis-, eks-, fi-,

* der Suffixe -ul-, -iĝ-, -estr-, -ec-, -ind-, -em-,
* sowie der entsprechenden Endungen (-a, -e, -o, -i)

folgende Wörter auf Esperanto:

1. Vorfahre
2. auseinander
3. ausscheiden (aus der Firma, dem Verein, usw.)
4. (moralisch) schlecht
5. leiten
6. Eigenschaft
7. würdig sein
8. Lust

#### Ripeto de la akuzativo. Enmetu la finaĵon -n kie necesas

1. La knabino_ ne ŝatas viando_.
2. La biciklo_ apartenas al mi_.
3. La libro_ falis sub la lito_.
4. Li_ elektis ŝi_ kiel mentorino_.
5. La gramatika_ libro_ kostas ok eŭro__
6. La tria_ de majo_ mi_ veturos hejme_.

#### Kelkaj el la sekvaj frazoj, sed ne ĉiuj, estas gramatike malĝustaj. Kiuj? Kial?

Substreku la malĝustan vorton, se estas, por marki la malĝustajn frazojn.

1. La knabino ne havas biciklo.
2. Ĉu vi jam lernis la poemeton?
3. Mankas al mi aeron.
4. Kiu ne prenis bonbono?
5. Nun mi havas tempon por vi.
6. Ili ne volis gustumi la dolĉa sukon.
7. Kie estas la skribilon?
8. Tie okazis grandan akcidenton.
9. La bebo havas nova liton.
10. Ne forgesu la akuzativon!

#### Vojpriskribo

Beschreibe einen Weg, den du häufig gehst, zum Beispiel zur Schule, zu einem Verein,
zur Universität oder zur Arbeit. Versuche, dabei auch Partizipien zu verwenden, und
benutze die Präpositionen

* tra, trans, en, preter, ĉe, apud, super, el
