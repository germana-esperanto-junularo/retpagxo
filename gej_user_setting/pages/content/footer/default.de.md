---
process:
    twig: true
---

<div class="columns">
  <div id="item" class="column col-4 col-md-12 extra-spacing">
      <h4>Kontakt</h4>
      <p style="text-align:left;">
        <span><b>Bundesgeschäftsstelle (Esperanto-Laden)</b></span><br>
        <span>Deutsche Esperanto-Jugend e. V.</span><br>
        <span>Geschäftsstelle</span><br>
        <span>Katzbachstraße 25</span><br>
        <span>10965 Berlin</span><br>
        <span><b>Tel.:</b> +49 30 4285 7899</span>
      </p>
      <h4>Vereinsdaten</h4>
      <p style="text-align:left;">
          <span><b>Registergericht:</b></span><br>
          <span>Amtsgericht Berlin-Charlottenburg</span><br>
          <span><b>Registernummer:</b></span><br>
          <span>VR 13651 B</span><br>
          <span><b>Steuernummer:</b></span><br>
          <span>27/663/59791 Finanzamt für Körperschaften I, Berlin</span>
      </p>
  </div>
  <div id="item" class="column col-4 col-md-12 extra-spacing">
        <h4>Soziales</h4>
        <div class="social-pages">
					<a href="https://t.me/joinchat/DcGzfk89ndwgNPsXvFUSQA" title="telegram" target="_blank" rel="me" class="u-url col-2 external-link">
              <img src="/gej/user/images/telegram.svg">
          </a>
					<a href="https://www.facebook.com/esperantojugend/" title="Facebook" target="_blank" rel="me" class="u-url col-2 external-link">
              <img src="/gej/user/images/facebook.svg">
          </a>
					<a href="https://www.instagram.com/esperantojugend/" title="Instagram" target="_blank" rel="me" class="u-url col-2 external-link">
              <img src="/gej/user/images/instagram.svg">
          </a>
					<a href="https://gitlab.com/germana-esperanto-junularo" title="GitLab" target="_blank" rel="me" class="u-url col-2 external-link">
              <img src="/gej/user/images/gitlab.svg">
          </a>
					<a href="https://www.linkedin.com/company/dej/about/" title="LinkedIn" target="_blank" rel="me" class="u-url col-2 external-link">
              <img src="/gej/user/images/linkedin.svg">
          </a>
  			</div>  
  </div>
  <div id="item" class="column col-4 col-md-12 extra-spacing">
      <h4>Bankverbindungen:</h4>
      <p style="text-align:left;">
        <span style="color:#666;"><b>Spenden- und Beitragskonto</b></span><br>
        <span><b>Bank für Sozialwirtschaft</b></span><br>
        <span><b>Kontoinhaber:</b>  Deutsche Esperanto-Jugend</span><br>
        <span><b>IBAN:</b>  DE37 2512 0510 0008 4249 01</span><br>
        <span><b>BIC:</b>  BFSWDE33HAN</span>
      </p>
      <p style="text-align:left;">
        <span><i><small>(zum Beispiel für Anzahlungen für Treffen)</small></i></span><br>
        <span style="color:#666;"><b>Geschäftskonto</b></span><br>
        <span><b>Bank für Sozialwirtschaft</b></span><br>
        <span><b>Kontoinhaber:</b> Deutsche Esperanto-Jugend</span><br>
        <span><b>IBAN:</b> DE64 2512 0510 0008 4249 00</span><br>
        <span><b>BIC:</b> BFSWDE33HAN</span>
      </p>
      <p style="text-align:left;">
        <span style="color:#666;"><b>UEA-Konto:</b></span><br>
        <span><b>UEA-Konto-Code:</b> GEJU-H</span>
      </p>
</div>
</div>
