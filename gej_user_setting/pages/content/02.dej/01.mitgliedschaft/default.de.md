
---
title: Mitgliedschaft
menu:  Mitgliedschaft
taxonomy:
    tag:
    category:
---

Es lohnt sich, Mitglied bei der Deutschen Esperanto-Jugend zu sein! Wem unsere
Aktivitäten gefallen, der sollte beitreten, denn unseren Mitgliedern bieten
wir eine ganze Menge:

###  Zeitschrift /·kune·/

Unsere bilinguale Mitgliedszeitschrift
[/·kune·/](https://www.esperanto.de/de/enhavo/gej/kune) erscheint als Teil der
"Esperanto Aktuell", der Zeitschrift des DEB. Durch sie erhaltet ihr
regelmäßig aktuelle Infos über Treffen, Kurse und sonstige Esperanto-
Aktivitäten stehen. Die Zeitschrift erscheint alle 2 Monate.

###  Preisgünstigeres Reisen

Die Aktivitäten der Deutschen Esperanto-Jugend werden vom Bundesministerium
für Frauen, Senioren, Familie und Jugend gefördert. Unter anderem gibt es
[Fahrtkostenzuschüsse
](https://www.esperanto.de/de/enhavo/gej/fahrtkostenzusch%C3%BCsse)für Reisen
zu großen internationalen Treffen. Das macht die Teilnahme an internationalen
Treffen um einiges billiger.

###  Rabatt beim Silvestertreffen

Für _ordentliche_ Mitglieder (A, B, F, S) der DEJ gibt es in der Regel
Vergünstigungen beim JES, dem Silvestertreffen der deutschen und polnischen
Esperanto-Jugend.

##  Mitgliedsbeiträge und Kategorien

###  Kategorie A (Jugendliche von 16-26):

Das ist die normale Kategorie für alle über 16 Jahren. Schüler*innen,
Studierende, Azubis, Zivil- oder Wehrdienstleistende, Erwerbssuchende zahlen
einen Mitgliedsbeitrag von 30 Euro, Jugendliche mit eigenem Einkommen zahlen
50 Euro.

###  Kategorie B (Jugendliche unter 16 Jahren):

Jugendliche unter 16 Jahren müssen nur 20 Euro zahlen.

###  Kategorie F (Familienmitgliedschat):

Hat sich deine Familie beim DEB als Familie angemeldet, so sind alle im
Haushalt lebenden Personen unter 27 Jahren automatisch Mitglied bei der DEJ.
Ihr erhaltet dann ein Exemplar der Mitgliedszeitung für die ganze Familie

Der Mindestbeitrag für die Familienmitgliedschaft ist 108 €, der Beitritt ist
nur über den DEB möglich.

###  Kategorie P (Patrone):

Diese Kategorie ist für Personen,die die DEJ besonders fördern wollen. Der
freiwilliger Jahresbeitrag beträgt mindestens 50 EUR.

###  Kategorie S (Schnuppermitgliedschaft):

Die Schnuppermitgliedschaft können alle (unter 27) erhalten, die an einem von
der DEJ anerkannten [Esperanto-Sprachkurs](http://esperanto.de/ereignisse)
oder einem KEKSO teilgenommen haben. Falls Zweifel bestehen, ob die DEJ einen
Kurs anerkennt, schreib uns einfach eine Mail und frag nach. Die Kategorie S
ist von den Rechten und Leistungen her äquivalent zur Kategorie A bzw. B,
allerdings entfällt ein Jahresbeitrag.

Nach Ablauf des ersten Jahres endet die Schnuppermitgliedschaft und man kann
entweder zahlendes Mitglied (A,B,F,P) werden, symbolisches Mitglied (C) werden
oder aus dem Verein austreten.

DEJ-Mitglieder sind automatisch (ohne zusätzlichen Beitrag) auch Mitglieder
des Deutschen Esperanto-Bundes (DEB) . Mitglieder der Kategorien A, B, F, P
und S sind ordentliche Mitglieder der DEJ. und bekommen die Zeitschrift
[/·kune·/](https://www.esperanto.de/de/kune).

**Alle Förder- und Mitgliedsbeiträge sind steuerlich abzugsfähig. Auf Wunsch
wird eine Spendenquittung ausgestellt.**

**Wenn du Mitglied werden möchtest, findest du unten das Beitrittsformular,
das du online ausfüllen kannst. Darunter findest du nocheinmal genauere
Hinweise zu Zahlungsmethoden für den Mitgliedsbeitrag**

  * [Internetbeitrittsformular](/sites/default/files/esperanto.de/dej/Beitrittsformular.pdf)
  * [Zahlung des Mitgliedbeitrags](https://www.esperanto.de/de/enhavo/gej/konten)

