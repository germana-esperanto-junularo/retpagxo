
---
title: Geschäftsordnung Hauptversammlung
taxonomy:
    tag:
    category:
---

# Geschäftsordnung Hauptversammlung der Deutschen Esperantojugend

Zuletzt geändert durch Beschluss der Hauptversammlung vom 31.12.2006.

  1. Die Hauptversammlung besteht aus allen ordentlichen Mitgliedern. Jedes ordentliche Mitglied hat eine Stimme. Nicht anwesende Stimmberechtigte können ihre Stimme schriftlich auf anwesende Stimmberechtigte übertragen. Jeder anwesende Stimmberechtigte darf höchstens eine übertragene Stimme ausüben.
  2. Die Hauptversammlung gilt als den Delegierten bekanntgegeben und damit ordnungsgemäß einberufen, wenn die Einladung termingerecht an die Unterorganisationen versandt wurde, in der Regel in der Mitgliederzeitschrift.
  3. Zu Beginn der Hauptversammlung werden ein Versammlungsleiter, ein Wahlleiter und ein Protokollführer gewählt.
  4. Die Tagungsordnung umfaßt wenigstens die folgenden Punkte:
    * Eröffnung; Wahl des Versammlungsleiters, des Wahlleiters und des Protokollführers; Feststellung der Stimmberechtigung; Ergänzungswahl
    * Abstimmung über die Tagesordnung
    * Diskussion über Vorstands- und Kassenbericht
    * Bericht der Kassenprüfer
    * Entlastung des Bundesvorstandes
    * Wahl des Bundesvorstandes und der Kassenprüfer (falls notwendig)
    * Arbeitsplan und Haushalt für das kommende Jahr
    * Verschiedenes
    * Entscheidung über Ort und Datum der nächsten Hauptversammlung
    * Schließung der Sitzung
