
---
title: Geschäftsordnung Vorstandt
taxonomy:
    tag:
    category:
---

## Geschäftsordnung Vorstandt der Deutschen Esperantojugend

Beschlossen auf der Vorstandssitzung in Naumburg, 23.02.03, zuletzt geändert
durch schriftlichen Beschluss des Vorstandes am 29.2.2004.

  1. Jedes Mitglied des Bundesvorstandes kann schriftliche Abstimmungen außerhalb der Sitzungen verlangen. Der Wunsch nach schriftlicher Abstimmung ist eindeutig als solcher zu kennzeichnen. Es ist ein Stichtag zu nennen, der mindestens fünf Tage Zeit für die Beantwortung lässt.
  2. Bei außerordentlich hoher Dringlichkeit kann der Bundesvorsitzende in Absprache mit einem anderen Vorstandsmitglied, vorzugsweise dem, dessen Aufgabenbereich der Beschluss berührt, einen Beschluss fassen. Dieser ist dem restlichen Vorstand innerhalb von drei Werktagen mitzuteilen.
  3. Grundsätzlich gilt bei schriftlichen Abstimmungen eine nicht abgegebene Stimme als Zustimmung, also das Prinzip "Silento estas konsento". Wenn sich zwischen dem Beginn und dem Ende der Abstimmung ein Einwand einstellt, der als dieser deutlich erkennbar ist und den vorgeschlagenen Konsens in Frage stellt, sollte die Abstimmung so lange aufgeschoben werden, bis dieser geklärt ist. Anschließend ist die Abstimmung neu zu beginnen und ggf. neu zu formulieren.
  4. Jedes Mitglied des Bundesvorstandes kann eine Postvollmacht für die DEJ erhalten.
  5. Vor Eingehen von Verpflichtungen (Verträge, Unterschrift von Resolutionen usw.)gegenüber anderen Verbänden ist grundsätzlich ein Beschluss des Bundesvorstandes erforderlich.
  6. Abweichend von Punkt 5 gilt folgende Regelung in Fällen, in denen aus zeitlichen Gründen die Herbeiführung eines Beschlusses des Bundesvorstandes nicht möglich ist: Falls eine Person als Vertreter/in von DEJ auf einer Veranstaltung ist, hat sie eine weit gefasste Vollmacht mitzudiskutieren. Falls sie eine Entscheidung für wichtig oder folgenschwer hält, muss sie vor einer Stellungnahme namens DEJ zu dieser Entscheidung die Zustimmung folgender Personen einholen:
    * Leiter/in der zuständigen Kommission,
    * Bundesvorsitzende/r bzw. stellv. Bundesvorsitzende/r,
    * Kassierer/in, falls die Entscheidung finanzielle Konsequenzen hat.
    * Wenn ein Vorstandsmitglied bei einer Vorstandssitzung fehlt, kann es seine Stimme einem anderen Vorstandsmitglied übertragen, das dann für ihn an Abstimmungen teilnimmt.
  7. Es sollen wenn möglich immer alle Vorstandsmitglieder an Abstimmungen teilnehmen. Wenn dies unvertretbare zeitliche Verzögerungen zur Folge hätte, kann von diesem Grundsatz abgewichen werden.
