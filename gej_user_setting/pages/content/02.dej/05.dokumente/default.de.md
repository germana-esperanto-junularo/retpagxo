
---
title: Dokumente
menu:  Dokumente
taxonomy:
    tag:
    category:
---

## Vereinsdokumente

Hier einige wichtige Dokumente der GEJ:

Auf Deutsch:


  1. [Statut / Satzung](satzung/default.de.md) oder [PDF](/sites/default/files/esperanto.de/dej/dokumentoj/2019_01_02_dejsatzung.pdf)
  2. [Geschäftsordnung der Hauptversammlung](go_vorstand/default.de.md) oder [PDF](go_hauptversammlung)
  3. [Geschäftsordnung des Bundesvorstands](go_jhv/default.de.md) oder [PDF](go_dej_vorstand)
  4. [Beitragsordnung](http://esperanto.de/dej/asocio/dokumentoj/beitragsordnung.php)
  5. [Formular zur Fahrtkostenbeantragung](/sites/default/files/esperanto.de/dej/Fahrtkostenformular.pdf)
  6. [Regelung über die Teilnahme an Veranstaltungen](http://esperanto.de/dej/asocio/dokumentoj/regelungteilnahme.php)
  7. [Regelungen über Fahrtkostenzuschüsse](http://esperanto.de/dej/asocio/dokumentoj/fahrtkosten.php)
  8. [Regelung für Veranstaltungen](http://esperanto.de/dej/asocio/dokumentoj/veranstaltungen.php) durch Unterorganisationen

Auf Esperanto:

  1. [Regularo pri repagoj, rabatoj kaj honorarioj](http://esperanto.de/dej/asocio/dokumentoj/repagojrabatojhonorarioj.php)
  2. [Regularo pri monsubteno al la suborganizoj](http://esperanto.de/dej/asocio/dokumentoj/monsubtenosuborganizoj.php)
  3. [Specimena tagordo de la jarĉefkunveno](http://esperanto.de/dej/asocio/dokumentoj/specimenatagordo.php)
  4. [Kontrakto pri unua Junulara E-semajno](http://esperanto.de/dej/asocio/dokumentoj/jes-kontrakto-2008-10-26.pdf) inter Germana kaj Pola Esperanto-Junularoj (PDF)
