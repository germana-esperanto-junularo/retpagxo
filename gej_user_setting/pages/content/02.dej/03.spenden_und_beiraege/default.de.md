
---
title: Spenden und Beiträge
menu:  Spenden und Beiträge
taxonomy:
    tag:
    category:
---

## Spenden

Die Deutsche Esperanto-Jugend ist als gemeinnützige Körperschaft anerkannt,
Spenden an die DEJ sind daher steuerlich absetzbar. Eine Spendenquittung
senden wir gerne zu - bitte schick dafür eine E-Mail an
[gej.kasko@esperanto.de](mailto:gej.kasko@esperanto.de).

Unsere Bank ist die _Bank für Sozialwirtschaft_ , ansässig in Hannover.

###  Spenden- und Beitragskonto:

Kontoinhaber: Deutsche Esperanto-Jugend  
IBAN: DE37 2512 0510 0008 4249 01
BIC: BFSWDE33HAN

## Mitgliedsbeiträge

###  Zahlungsweisen

Es bestehen zwei Möglichkeiten, deinen Beitrag zu entrichten. Entweder du
erteilst der DEJ eine Einzugsermächtigung, so dass sie jährlich den
entsprechenden Betrag von deinem Konto automatisch abbuchen kann, oder
überweist deine Beiträge selbst, z. B. per Dauerauftrag

####  Einzugsermächtigung

Die Einzugsermächtigung ist sowohl für uns, als auch für dich der
komfortablere Weg. Leider können wir momentan über unseren Server keine
sichere Verschlüsselung anbieten, weswegen wir kein Onlineformular zur Eingabe
der Kontodaten bereitstellen können. Du kannst uns aber einfach eine Mail an
[gej.ma@esperanto.de](mailto:gej.ma@esperanto.de) senden. Wir stellen auch
eine entsprechende [Vorlage]("mailto:gej.ma_at_esperanto.de?subject=Einzugserm%C3%A4chtigung%20-%20Mitgliedsbeitrag%20DEJ%20-%20%5BDEIN%20NAME%5D&body=Hiermit%20erm%C3%A4chtige%20ich%20die%20Deutsche%20Esperanto%20Jugend%20e.%20V.%20\(DEJ\)%20j%C3%A4hrlich%20den%20satzungsgem%C3%A4%C3%9Fen%20Mitgliedsbeitrag%20f%C3%BCr%20%5BDEIN%20NAME%5D%20in%20H%C3%B6he%20von%20%5BBETRAG%5D%20Euro%20von%20meinem%20Konto%0A%0AKontoinhaber%3A%20%0AIBAN%3A%20%0ABIC%3A%20%0A%0A%0Aeinzuziehen.%20Bei%20%C3%84nderungen%20dieser%20Angaben%2C%20z.%20B.%20aufgrund%20der%20%C3%84nderung%20des%20Erwerbst%C3%A4tigkeitsstatus%2C%20setze%20ich%20die%20DEJ%20zeitnah%20in%20Kenntnis.%0A") bereit.

Wenn dir die herkömmliche Post lieber ist, kannst du [unsere PDF-
Vorlage](/sites/default/files/esperanto.de/dej/Einzugsermaechtigung_DEJ.pdf)
ausdrucken (natürlich ist schwarz-weiß völlig ausreichend), ausfüllen und an
die angegebene Adresse senden.

Bitte gib als Verwendungszweck " **Mitgliedsbeitrag DEJ - [JAHR] - [NAME, VORNAME]** " an.

###  Höhe des zu zahlenden Mitgliedsbeitrages:

* Für Jugendliche unter 16 Jahren gilt der Ermäßigte Satz von 10 Euro pro Jahr
* Für Mitglieder die einer Erwerbstätigkeit nachgehen, und damit ein eigenes Einkommen haben gilt der Satz von 50 Euro pro Jahr
* Alle anderen Mitglieder zahlen 30 Euro pro Jahr.

Daneben besteht noch die Möglichkeit als Patron der DEJ (auch für Menschen,
die aus Altersgründen nicht mehr in der DEJ sein können) Mitglied zu sein und
einen höheren Beitrag zu zahlen. Wenn das für dich zutrifft, nimm bitte
Kontakt mit [gej.ma@esperanto.de](mailto:gej.ma@esperanto.de) auf.
