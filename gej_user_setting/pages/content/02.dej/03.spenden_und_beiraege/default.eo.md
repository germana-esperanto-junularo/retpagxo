
---
title: Donacoj kaj kotizoj
menu:  Donacoj kaj kotizoj
taxonomy:
    tag:
    category:
---

La Germana Esperanto-Junularo estas karitata organizaĵo laŭ Germana leĝo;  
donacoj al ĝi deklareblas por imposta rabato. Ni kvitancas donacojn, se vi  
deziras tion – simple petu tion retpoŝte de
[gej.kasko@esperanto.de](mailto:gej.kasko@esperanto.de).

Nia banko estas Bank für Sozialwirtschaft, sidante en Hanovro.

###  Konto por Donacoj kaj Membrokotizoj:

tenanto: Deutsche Esperanto-Jugend  
IBAN: DE37 2512 0510 0008 4249 01  
BIC: BFSWDE33HAN

###  Konto por Eventoj kaj Komerco:

(ekzemple por antaŭpagoj de renkontiĝoj)  
tenanto: Deutsche Esperanto-Jugend  
IBAN: DE64 2512 0510 0008 4249 00  
BIC: BFSWDE33HAN

###  UEA-Konto

Se vi estas eksterlande kaj ĝiro al Germanio tro kostas,  
eblas sendi monon al la UEA-konto de GEJ.  
Per tio rimedo daŭras pli longe ĝis mono alvenas ĉe ni.

Kodo de UEA-Konto: GEJU-H
