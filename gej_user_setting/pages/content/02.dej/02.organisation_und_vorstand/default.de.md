
---
title: Organisation und Vorstand
menu:  Organisation und Vorstand
taxonomy:
    tag:
    category:
---

## Aufbau der Deutschen Esperanto Jugend

Die Arbeit der DEJ ist in mehreren Kommisionen, Arbeitsgruppen und Projekten organisiert.
Um einen kleinen Überblick zu bekommen hier das Wichtigste in einem Bild.

<div class="columns">
    <div id="item" class="column col-6 col-mx-auto col-md-12 extra-spacing">
      <figure class="figure">
        <img class="img-responsive" src="/gej/user/images/GEJ_organiza_skemo.png">
        <figcaption class="figure-caption text-center">Organigramm der GEJ</figcaption>
      </figure>
    </div>
</div>

##  Bundesvorstand

Hier findest du die wichtigsten Adressen. Weitere Ansprechpartner*innen finden
sich unter [Kommissionen](kommissionen)

<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <h2>Vorsitzende</h2>
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="/gej/user/images/large_Michaela.png">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <b>Michaela Stegmaier</b> <br> <br>
           Am Sportplatz 12  <br>
           37412 Herzberg am Harz <br> <br>
           <p>E-Mail:
              <a href="mailto:gej.prezidanto _at_ esperanto _punkt_ de">gej.prezidanto<img src="/gej/user/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <h2>Finanzen (Schatzmeister)</h2>
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="/gej/user/images/large_Lars.png">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <b>Lars Hansen</b> <br> <br>
           Stettiner Str. 22    <br>
           71032 Böblingen <br> <br>
           <p>E-Mail:
              <a href="mailto:gej.kasko _at_ esperanto _punkt_ de">gej.kasko<img src="/gej/user/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>
</div>

##  Weitere Vorstandsmitglieder


<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <h2>Vorsitzende</h2>
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="/gej/user/images/large_Konstanze.png">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <p>E-Mail:
              <a href="mailto:konstanze.schoenfeld _at_ esperanto _punkt_ de">konstanze.schoenfeld<img src="/gej/user/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <h2>Finanzen (Schatzmeister)</h2>
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="/gej/user/images/large_Devid.png">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <p>E-Mail:
              <a href="mailto:david.mamsch _at_ esperanto _punkt_ de">david.mamsch<img src="/gej/user/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>
</div>
