
---
title: Newsletter
menu:  Newsletter
taxonomy:
    tag:
    category:
---

La GEJ dissendas ĉirkaŭ unufoje kvaronjare kolektitaj informoj pri E-aranĝoj
kaj sia agado. Se estas spontana gravaĵo ankaŭ fojfoje etaj plusaj mesaĝoj
estas dissendataj. Se vi volas esti informigita pri ĉiuj tiuj aferoj, vi povas
vin inskribi ĉe <http://www.esperanto.de/cgi-
bin/mailman/listinfo/gej.novajxoj> por niaj informleteroj.

Pluse ekzistas diskutliston por aktivuloj de GEJ. Do se vi volas aktiviĝi en
nia asocio kaj helpi ekinformigi pli da junulojn pri Esperanto, enskribu ankaŭ
ĉe <https://www.esperanto.de/cgi-bin/mailman/listinfo/gej.aktivuloj> por nia
diskutlisto.
