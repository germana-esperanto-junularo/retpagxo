
---
title: Deutsche Esperanto-Jugend
menu:  Deutsche Esperanto-Jugend
taxonomy:
    tag:
    category:
---

Die Deutsche Esperanto-Jugend (DEJ) ist der Jugendverband des [Deutschen
Esperanto-Bundes](http://esperanto.de/deb/) (DEB). Sie wurde 1951 gegründet
und hat heute ca. 130 Mitglieder unter 27 Jahren.

Ziel der Deutschen Esperanto-Jugend ist die Völkerverständigung, insbesondere
auf der Ebene der internationalen Jugendarbeit. Zur Erreichung dieses Ziels
wird dabei die internationale Sprache Esperanto verwendet.

Die Umsetzung erfolgt hauptsächlich durch Zusammenarbeit mit anderen Ländern.
Gleichzeitig vernetzen wir die Aktivitäten unserer Ortsgruppen und
Landesverbände.

Unsere Aufgaben lassen sich grob in Öffentlichkeitsarbeit, Esperanto-
Unterricht, Treffenorganisation, Seminare und internationale Zusammenarbeit
untergliedern.

Mitgliedschaften: Die DEJ ist Sektion der [Weltesperantojugend
TEJO](http://www.tejo.org/) und Unterverband des Deutschen Esperanto-Bundes.
Außerdem sind wir Gründungsmitglied der Arbeitsgemeinschaft Neue Demokratische
Jugendverbände.

##  Öffentlichkeitsarbeit

Ein Großteil der Menschen in Deutschland weiß nicht, was Esperanto ist und
dass es die Deutsche Esperanto-Jugend gibt. Manchmal werden sogar falsche
Ideen mit Esperanto verbunden, beispielsweise wird der Euro oft als
"Esperanto-Währung" bezeichnet (was er nicht ist, da Esperanto andere Sprachen
gerade erhalten und nicht ersetzen möchte). Andere meinen, Esperanto sei ein
spanischer Dialekt und wissen nicht, was eine
[Plansprache](http://de.wikipedia.org/wiki/Plansprache) ist. Da die
zahlreichen Esperanto-Treffen und die Esperanto-Kultur in der Öffentlichkeit
kaum wahrgenommen werden, hört man auch immer wieder "Esperanto? Das spricht
doch keiner!".

Dieser Nichtinformiertheit versucht die Deutsche Esperanto-Jugend
entgegenzuwirken, indem sie verschiedene Informationsbroschüren

herausgibt und u.a. auf Messen, Kirchentagen, usw. präsent ist. Bevor jemand
Esperanto lernt, muss er/sie erst wissen, was Esperanto überhaupt ist und was
man damit alles machen kann.

##  Esperantokurse anbieten

Es gibt zwar eine ganze Reihe von Büchern und Internetkursen, um sich
Esperanto im Selbststudium alleine beizubringen. Schöner und mit mehr Spaß
verbunden ist es aber sicher, mit anderen Leuten zusammen Esperanto zu lernen!
Deswegen organisiert die Deutsche Esperanto-Jugend über das ganze Jahr
verteilt Esperantokurse. In Deutschland handelt es sich dabei meistens um
Wochenendkurse. Auf den großen internationalen Treffen wie der Internationalen
Woche gibt es außerdem Intensivkurse, die eine ganze Woche dauern. Dabei kann
man Esperanto lernen und sofort anwenden.

##  Seminare

Neben den Esperanto-Aktivitäten bieten wir auch Seminare für unsere Mitglieder
an, die nicht unbedingt direkt mit Esperanto in Verbindung stehen, dafür aber
wichtige Kenntnisse und Fähigkeiten vermitteln, die im Vereinsleben oder
anderweitig nützlich werden können. Dies geht aufgrund unserer begrenzten
Finanzausstattung leider nur, solange wir dafür Subventionen oder Spenden
einsetzen können.

##  Treffen organisieren

Esperanto lebt von seiner Sprechergemeinschaft. Auch wenn man eine Menge
verschiedener Dinge mit Esperanto machen kann, sind für viele die
internationalen [Treffen](http://eventoj.hu/) am reizvollsten. Hier besteht
die gerade für Jugendliche sonst eher seltene Gelegenheit, sich mit Leuten aus
aller Welt auszutauschen, zu diskutieren, zu feiern und miteinander in Kontakt
zu kommen. Auf Esperanto­treffen entstehen jahrelange Freundschaften zwischen
Menschen verschiedenster Nationalität. Die Deutsche Esperanto-Jugend
organisiert jedes Jahr zu Silvester gemeinsam mit der Polnischen Esperanto-
Jugend die Jugend-Esperantowoche abwechselnd in Deutschland und Polen. Sie ist
mit rund 250 Teilnehmern aus 30 Ländern nach dem
[Esperantojugendweltkongress](http://ijk-69.mesha.org/) das zweitgrößte
internationale Esperanto-Jugendtreffen der Welt.

##  Zusammenarbeit mit anderen Ländern

Die Zusammenarbeit mit Esperanto-Organisationen aus der ganzen Welt ist eine
zentrale Aufgabe der Deutschen Esperanto-Jugend, die ganz unterschiedliche
Bereiche berührt.

So werden mit anderen Esperanto-Jugendverbänden gemeinsam Subventionsanträge
für Treffen und Seminare gestellt, um den Teilnehmern einen Teil der
Reisekosten zurückzuerstatten und die Treffen damit auch für Schülerinnen und
Schüler sowie Studierende bezahlbar zu machen.

Auch die Treffen selbst werden manchmal von mehreren Esperanto-Jugenden aus
verschiedenen Ländern organisiert, z.B. das gastronomische Wochenende von der
Französischen und der Deutschen Esperanto-Jugend.

Ferner vermitteln wir auch Brieffreundschaften zu anderen Esperantosprechenden
in der ganzen Welt.

Schließlich ist die DEJ aktiv in der Weltesperantojugend TEJO mit zwei
Delegierten vertreten.

