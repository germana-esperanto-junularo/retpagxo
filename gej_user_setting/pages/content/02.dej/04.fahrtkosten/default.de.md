
---
title: Fahrtkostenzuschüsse
menu:  Fahrtkostenzuschüsse
taxonomy:
    tag:
    category:
---

Wenn du in Esperanto-Sachen unterwegs bist, kannst du unter Umständen einen
Teil der Fahrtkosten von der DEJ zurückbekommen. Durch Aktivität für den
Verein sollen dir möglichst keine gesonderten Kosten entstehen. Vor allem dank
der Unterstützung des Bundesministeriums für Familie, Senioren, Frauen und
Jugend können Mitglieder für einige Treffen Fahrtkostenunterstützung erhalten,
die in der Regel den Jahresbeitrag deutlich übersteigt..


## Internationalen Treffen

Subventionen ermöglichen unserem Verein, die Fahrtkosten seiner Mitglieder zu
[internationalen Treffen](http://www.esperanto.de/ereignisse) in gewissen
Grenzen zu übernehmen. Dafür solltest du dich während des Treffens vor Ort in
die Subventionsliste, das sogenannte "Formblatt L", eintragen, das eine*r der
deutschen Teilnehmer*innen für die DEJ mitbringt. Dieses wird dann an unseren
Subventionsbeauftragten weitergegeben.

Nach dem Treffen sind innerhalb von 4 Wochen die Fahrtkosten, etwa durch das
Einsenden der Fahrkarten, nachzuweisen. Vewende bitte das dafür [vorgesehene
Formular](/sites/default/files/esperanto.de/dej/Fahrtkostenformular_DEJ.pdf)
und schicke es inklusive der zugehörigen Fahrkarten oder Belege per Post an
unseren [Schatzmeister](http://esperanto.de/dej/vorstand). Alternativ kannst
du das ausgefüllte Formular (inkl. Scan oder Foto der Fahrkarten/Belege) auch
per E-Mail an [gej.kasko@esperanto.de](mailto:gej.kasko@esperanto.de)
schicken.

Bei weiteren Fragen wende dich bitte an unseren Schatzmeister unter
[gej.kasko@esperanto.de](mailto:gej.kasko@esperanto.de)

###  Geförderte Treffen und maximale Zuschüsse 2017

| Treffen |  Ort/Datum |  Maximaler Zuschuss |
| ---|---|---|
| [IJF](http://iej.esperanto.it/ijf/?lang=eo) | 12.04.-18.04. Castione della Presolana, Italio |  80 €                                              |  
| [IJK](http://ijk2017.tejo.org/)             | 05.08.-12.08. Aneho, Togolando                 |  *)auf vorherige Anfrage (doch es lohnt sich ;) )  |
| [UK](http://www.2017uk.net/uk2017/)         | 22.07.-29.07. Seolo, Koreo                     |  *)auf vorherige Anfrage (doch es lohnt sich ;) )  |
| [SES](http://ses.ikso.net/)                 | 15.07.-23.07. Banská Štiavnica, Slovakio       | 80€                                                |
| [JES](http://jes.pej.pl)                    | 26.12. 2016 - 02.01. 2018 Stettin, Pollando    | 50 €                                               |

<br> <br>

Fehlende Angaben werden bei Bekanntgabe ergänzt.

[[Die Regelung im Wortlaut.](http://esperanto.de/dej/asocio/dokumentoj/fahrtkosten.php#ausland)]

## Vereinsarbeit

Wer im Auftrag der DEJ unterwegs ist, kann ebenfalls Fahrtkostenzuschüsse
erhalten. Dafür gibt es die Regelung zu Fahrtkostenzuschüssen für
Dienstreisen.

Was genau eine _Dienstreise_ im Sinne dieser Regelung ist, legt der Vorstand
individuell fest - im Zweifelsfalle kannst du nachfragen.

Einige häufige Fälle wurden bereits festgelegt:

1. Reisen zu Vorstandssitzungen sind für Vorstandsmitglieder und für vom Vorstand dazu eingeladene Personen Dienstreisen.
2. Reisen zur Vorbereitung eines JES sind für die dazu beauftragten Teilnehmer*innen Dienstreisen.
3. Eine Reise zu einer Vollversammlung oder Hauptausschusssitzung des DBJR ist für eine*n von [KonKERo](http://esperanto.de/dej/kommissionen) zu bestimmenden Vertreter*in eine Dienstreise.
4. [KoLA](http://esperanto.de/dej/kommissionen) kann festlegen, dass eine Anreise eines Lehrenden zu einem durch KoLA vermittelten Esperanto-Kurs eine Dienstreise ist.
