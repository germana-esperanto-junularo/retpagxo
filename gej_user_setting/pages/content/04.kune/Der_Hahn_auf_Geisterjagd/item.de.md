---
title: Der Hahn auf Geisterjagd
menu:  Der Hahn auf Geisterjagd
date:  30-10-2018
slug:  kkps-2018-2
taxonomy:
    tag: [KKPS, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Jonas Marx
---

Das diesjährige Klaĉ-Kunveno Post-Somera fand in dem niederländschen Ort Hunsel statt, nur wenige Kilometer von Roermond entfernt. Ich freute mich darauf, viele Freunde wiederzusehen, mit denen ich den Sommer in Spanien auf dem IJK verbracht hatte.

![KKPS partoprenantoj](image://kkps2.jpg)

Durch die kalten Nächte und akute Werwolf-Warnungen in den eigenen Reihen mussten wir zusammenrücken. Beim Buffet und der Bierauswahl konnte ich mich aber nicht beklagen. Am zweiten Tag (Samstag) machten wir einen Ausflug zum nahegelegenen Teekannenmuseum. Dort gab es über zweitausend kuriose, schöne oder seltene Teekannen. Abends gab es den Halloween-Ball, an dem die besten Kostüme in drei Kategorien ausgezeichnet wurden. Den ersten Preis für das schönste Kostüm gewann Alina mit einem Charakter aus einer mir nicht bekannten Serie, das originellste Kostüm konnten eindeutig die Organisatoren als Ghost Busters für sich gewinnen. Das erschreckendste Kostüm hingegen bekam, trotz harter Konkurrenz durch einen als Trump gekleideten Kevin meine Verkleidung als Ringgeist aus Herr der Ringe. Leider konnte der namensgebende Hahn „Poso“ dieses Jahr wieder nicht erscheinen – Er musste wohl über das Wochenende bei der Geisterjagd helfen..

Mein größter Kritikpunkt kann also nur ein zu schnell vorübergehendes Wochenende sein. Immerhin gab es eine kleine Dosis Esperantujo, die hoffentlich bis zum nächsten JES ausreicht.
