---
title: Hautshaltsplan DEJ
menu:  Hautshaltsplan DEJ
date:  25-11-2018
slug:  hautshaltsplan-2018
taxonomy:
    tag: [Hautshaltsplan, Finanzen]
    category: Vorstand
author:
    name: Michaela Stegmaier
---

Der Haushaltsplan für das Jahr 2019 wird dominiert von den Ausgaben zum JES 2018/19, denen aber erwartete Einnahmen in ähnlicher Höhe gegenüberstehen. Diese werden allerdings noch für das Jahr 2018 verbucht. Die projektbezogenen Einnahmen und Ausgaben beziehen sich auf das Projekt "Fostering Equality in Youth", das Anfang 2019 seinen Abschluss findet. Das Projekt war sowohl inhaltlich als auch finanziell ein Erfolg und Projektgewinne werden von uns an die teilnehmenden Organisationen ausgeschüttet.

Da wir im Budget unsichere Kosten eintragen, unsichere Gewinne aber nicht, sind für Spenden, Subventionen und das kommende JES null Euro veranschlagt. Für die zwei KEKSOs, die nächstes Jahr stattfinden sollen, und die als Werbeveranstaltungen auf Verlust ausgelegt sind, sind dagegen Kosten eingetragen.

Fragen zum Haushaltsplan könnt ihr mir gerne per E-Mail schicken oder auf der Hauptversammlung während des JES stellen. Lars Hansen
