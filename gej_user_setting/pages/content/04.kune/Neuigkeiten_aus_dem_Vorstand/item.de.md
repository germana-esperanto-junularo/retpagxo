---
title: Neuigkeiten aus dem Vorstand
menu:  Neuigkeiten aus dem Vorstand
date:  10-10-2018
slug:  neuigkeiten-aus-dem-vorstand
taxonomy:
    tag: [Vorstand, ÖA]
    category: Vorstand
author:
    name: GEJ Vorstand
---


Wir haben einen Flyer neu aufgelegt „Esperanto – lingvo kiel ludo - die DEJ hat jetzt eine Visitenkarte (frisch gedruckt für die Expolingua in Berlin)

![Flyerbox](image://flyer.jpg)

→ das alles könnt ihr im BerO (Berlina Oficejo) bei Sibylle Bauer bestellen, um selbst damit zu werben (schreibt einfach eine Mail mit eurer Bestellung an sibylle.bauer@esperanto.de). Severin und Julia werden aus dem Vorstand ausscheiden. Wir suchen also dich für ein Vorstandsamt oder eine zwanglose Teilnahme in einer Arbeitsgruppe zu den folgenden Themenbereichen:

* Webseite / Email-Server / Social Media
* Aŭskulta Servo - Nachwuchsarbeit, JES
* KEKSO, Lehre/KEK, Projekte
* Zusammenarbeit mit DEB Zielgruppe ab 28
* komitatanoj / TEJO
