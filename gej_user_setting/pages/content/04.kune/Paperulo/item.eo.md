---
title: Paperulo, la besto manĝanta homojn
menu:  Paperulo, la besto manĝanta homojn
date:  10-11-2017
slug:  paperulo
taxonomy:
    tag: [rakonto, KEKSO]
    category: Rakontoj
author:
    name: Marco Watermeier
---

![paperulo](image://paperulo.jpg)

Saluton! Mia nomo estas Paperulo. Mi estas besto el oranĝa papero kaj mi ŝajnas tre timiga. Mi havas malgrandajn sed kolerojn okulojn, kiuj estas tute nigraj. Kvankam mi ne havas dentojn, mi povas manĝi ĉion per mia akra buŝo. Ĝi estas bonega por manĝi bongustajn personojn, mia plej ŝatata manĝo. Sed miaj gepatroj malpermesis min manĝi homojn, kvankam ili ankaŭ ŝatas tiun guston. Tion mi ne povas kompreni.

Ĉu ili nur volas la homojn por si, aŭ ĉu homoj estas tre malsanaj por mi kaj ili nur estas zorgema pri mi? Mi ne opinias la lastan, ĉar ili jam forpelis min de mia agrabla loĝejo! Mi estis loĝanta en bela granda ŝranko de infano, kie mi ĉiam havis noktmanĝon. Sed nun mi loĝas sub malbela ponto, mi havas nek manon nek laboron, kaj mi nur havas aĉegan manĝon kaj akvon kiu estas tre kota kaj bruna. Sed kio estas tia sono? Ĉu ĝi estas mia poŝtelefono?

"Jes, kiu estas vi?", mi demandas. "Ni estas Paperpatrino kaj Horst, viaj gepatroj!", la alvokantoj respondas. Mi nun demandas: "Kial vi malpermesis min manĝi homojn? Vi scias kiel multe mi ŝatas tiel bonegan guston de homa viando!", sed Horst respondas: "Vi scias ke manĝi homojn metos vin en la malliberejon! Vi devas tuj trovi alian manĝon!" Tiu diro tre surprizas min, ĉar mi ne sciis ĝin. "Mi jam ne povas manĝi homojn, ĉar mi loĝas sub ponto. Ĉu vi ne sciis?" Mi tuj aŭdis la respondon de mia patrino: "Ni ne scias! Tuj venu al ni, ni havas liberan ŝrankon por vi!" Mi dankas kaj vizitas ilin.

Tie mi nun rimarkas, ke homviando estas tre malsana por mi. Tial mi volas komenci nur manĝi sapvezikojn, ĉar ili estas tre dolĉaj kaj tre sanaj por mi.

LA FINO

*Origine skribita per skrib-maŝino, verkita de Marco Watermeier,
partopreninte la 22-an KEKSOn en Herzberg*
