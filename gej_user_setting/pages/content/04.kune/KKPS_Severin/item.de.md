---
title: Ein Reiseromänchen über das KKPS 2017
menu:  Ein Reiseromänchen über das KKPS 2017
date:  30-10-2017
slug:  kkps-2017-1
taxonomy:
    tag: [KKPS, Nederlando, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Severin
---

Auf dem KKPS war ich in offizieller Mission unterwegs: Als DEJ-Vertretung für das Genra Egaleco-Projekt, das unter der Schirmherrschaft der DEJ von sechs verschiedenen Esperanto- Jugendorganisationen durchgeführt wird. Aber davon an anderer Stelle mehr, darum an dieser Stelle nichts mehr.

![Severin](image://severin.jpg)

Abgesehen von der Projektsitzung habe ich auf dem KKPS einen Haufen Freund*innen wiedergesehen und ein putziges Hündchen kennen gelernt und eine fetzige Halloweenparty gefeiert. Auf der gab es eine spannende Werwolf-Runde mit den Verdaj Skoltoj, die gefühlt ungefähr die Hälfte der Teilnehmenden stellten. Außerdem wurden zwischen zwei und drei alle Getränke verdoppelt, weil es die Zeit zwischen zwei und drei dank Zeitumstellung gleich zweimal gab. Als dann jemand versuchte, einem Kubaner mit noch ganz geringen Esperanto-Kenntnissen mit Händen und Füßen zu erklären, was eine Zeitumstellung ist und er wahrscheinlich stattdessen nur Zeitzonen verstanden hat, wussten auch die Letzten, dass es langsam Zeit fürs Bett ist.

Anlässlich des zeitgleich in einem anderen Teil des Städtchens stattfindenden Zamenhoftages habe ich auch noch ein bisschen mehr von Lunteren gesehn. Dabei konnte ich feststellen, dass die Leute in den Niederlanden sehr klug sind: Sie nennen ihre Friedhöfe „Begraafplaats“, „Begrabeplatz“, damit direkt klar ist, was gemeint ist. Außerdem sind ihre Bahnhöfe unglaublich benutzer*innenfreundlich: nicht nur war der Fahrplan so übersichtlich, wie ich es in meiner Gewöhnung an deutsche Verhältnisse nicht für möglich gehalten hab; auch war darauf noch eingezeichnet, wie man zur nächsten Bushaltestelle kommt. Und nicht nur klug ist man dort, sondern auch freundlich: Ein sehr seriös aussehender Mann hat mich am Bahnhof trotz meiner eher unseriösen und übernächtigten Erscheinung freundlich gegrüßt. Also: Besucht die Niederlande, bevor die Nordsee hereinschwappt und sie weg sind. Es lohnt sich! Kare, kore kaj kolore Severin‘
