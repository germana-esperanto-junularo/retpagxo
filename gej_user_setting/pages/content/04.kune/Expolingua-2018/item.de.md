---
title: Expolingua in Berlin
menu:  Expolingua in Berlin
date:  18-11-2018
slug:  expolingua-2018
taxonomy:
    tag: [Expolingua, Berlin, ÖA]
    category: KEKSO
author:
    name: Michaela Stegmaier
---

Bei einem Treffen mit Esperantofreunden in Berlin wurde ich auf die Expolingua aufmerksam gemacht und suchte sogleich tatkräftige Unterstützung, damit wir den Vertretern des DEB bei der Betreuung ihres Standes bei dieser Sprachenmesse unter die Arme greifen können und damit die Besucher sehen, dass auch junge Menschen Esperanto sprechen. Um eins vorwegzunehmen: Diesen Eindruck konnte man am Ido-Stand leider nicht gewinnen.

![IJF](image://expolingua.jpg)

Ich fand also Verbündete und machte mich am Freitag Nachmittag mit Jonas und Chuantong, oder wie wir ihn nennen, Wang, auf den Weg nach Berlin. Für Wang war es nach dem KEKSO in Heidelberg, zu dem er seinen Mitbewohner Jonas zum ersten Mal begleitete, das zweite Esperanto-Event.

Am Russischen Haus trafen wir auf unsere Gastgeber, Ulrich und Barbara Brandenburg, die schon auf uns warteten und uns nach dem Ende des ersten Messetages bei sich zu Hause mit Pizza, Salat und Käsespezialitäten verwöhnten. Wir saßen noch lange zusammen am Tisch und redeten, bis uns die Aussichten auf ein frühes Frühstück in die Federn trieben.

Am nächsten Morgen standen wir motiviert in den Startlöchern, um ganz viele Leute über Esperanto zu informieren. Mit unseren neuen Flyern und Visitenkarten waren wir auch bestens dafür gerüstet und als um 10 Uhr die Messe eröffnet wurde, gingen wir es an und wir erlebten den Tag alle sehr unterschiedlich:

Jonas beschreibt den Tag insgesamt als eine schöne und interessante Erfahrung. Neben vielen Gesprächen mit interessierten Besuchern erkundete er sowohl die Stände zur russischen Sprache, als auch die russische Bar des Hauses. Passend zu seinem Studiengang Slawistik fühlte er sich im Russischen Haus sehr wohl.

Für Wang blieb auch etwas Zeit, sich in der Umgebung der Friedrichstraße umzusehen. Er findet Berlin schön, und kommt gern wieder, um mehr zu sehen. Die vielen Sprachen, die präsentiert wurden und die vielen Fragen, die zu Esperanto gestellt wurden, zeigten ihm, dass er sich auch weiterhin damit beschäftigen möchte. Das Sammeln von Adressen von Interessenten und andererseits das Verteilen von kleinen Werbegeschenken haben ihm Spaß gemacht und er hat auch andere Angebote der Messe genutzt, so wie einen Minisprachkurs und die Tandembörse.

Ich für meinen Teil habe, wenn ich mal kurz nicht an unserem Stand war, mal beim Ido-Stand vorbeigeschaut und mich über die Unterschiede zu Esperanto aufklären lassen, mich mit Chuck Smith zu einem Kaffé getroffen und mich nach lustigen Werbegeschenken umgesehen. Am coolsten waren da noch unsere Nachbarstände. Ihre hungrigen Betreuer kamen immer mal wieder auf einen Keks oder einen Gummibären zu uns rüber.

Der Abend kam schneller als gedacht und es wurde Zeit, den Stand abzubauen. Wir fuhren erneut zu unseren Gastgebern und aßen leckere Hotdogs und Kartoffelsalat. Der restliche Abend verlief ähnlich wie der vorige und wir fielen müde in unsere Betten.

Den Sonntag nutzen wir für einen Besuch des Deutschen Historischen Museums Berlin, das sehr zu empfehlen ist. Ein schöner Abschluss des Wochenendes war, als mir Wang beim Abschied noch ein ausgefülltes Mitgliedsformular in die Hand drückte und sich somit bis zum JES bei mir verabschiedete. Zum Schluss bleibt mir nur, einen Dank auszusprechen, an Ulrich und Barbara, die uns fürstlich bewirteten und das Wochenende zu einem unvergesslichen machten. Und an Wang und Jonas, mit denen es sehr lustig war. Bis bald! Eure Michaela
