---
title: 17. KEKSO in Erfurt
menu:  17. KEKSO in Erfurt
date:  21-05-2014
slug:  kekso-17
taxonomy:
    tag: [KEKSO, Erfurt, renkontiĝoj]
    category: KEKSO
author:
    name: Rolf Sievers
---

Samtempe kaj samloke kiel la GEK okasis la 17a KEKSO. Ĝi estas eta,
kreema esperanto kurso kaj renkontiĝo por junaj esperantistoj.

![Keksoj](image://kekso-17-1.jpg)

Ni ĉefe estis ekstere, malgraŭ ni iam babilis kun „plenkreskuloj“,
partoprenis en komunaj aferoj kaj vizitis la libroservon. Ĉi tie mi
rakontas pri nia semajnfino por la forestinta junuloj kaj la aliaj
partoprenantoj de GEK.

Ĉiutage ni manĝis rostitajn panojn, legumojn aŭ kuskuson, feliĉe ni
trovis rost-fajrigilon poste longa serĉumado. Stranĝas, ke ne ĉiu
superbazaro vendas tion.

### Kie la junuloj kaŝis?

Dum la aranĝo, ni komencis traduki malnovajn komikojn (kies kopirajta
jam finiĝis). Bedaŭrinde, tiu projekto estis tro granda por unu
semajnfino. Malgraŭ ĝi bone servis por lerni kaj ekzerci nian lingvon
kaj raraj vortoj. Plie, iom da novuloj esperantiĝis, amikiĝis.

![Keksoj](image://kekso-17-0.jpg)

Kiam ni ne laboris, ni ĝuis varman sunlumon, diversajn ludojn (Munchkin,
Dixit, eĉ Go') aŭ esploris la urbon. Multaj parkoj invitis nin, la akvo
fontana malvarmigis niajn piedojn. Aŭ eĉ plu.


Post sunsubiro, ni daŭre iras tra Erfurt kaj malkovris plu belajn ejojn.
Eksemple, la „Krämerbrücke“ altiras multaj homoj. Ili sidis sube. Ili
festis en la ĉirkaŭejo.


Dum la fina tago, ni rostas per niaj lastaj du karberoj.
Amikoj foriris – sed ĉiu jam antaŭĝojis por SES (Rusio), FESTO (Francio)
aŭ almenaŭ la venonta KEKSO. Se vi havas tempon dum aŭgusto, partoprenu
en internacia renkontiĝo!
