---
title: Partoprenu IJF-on en Italio senpage!
menu:  Partoprenu IJF-on en Italio senpage!
date:  19-01-2018
slug:  ijf-stipendio
taxonomy:
    tag: [IJF, Italio, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Julia Berndt
---

Ĉu vi ŝatus pasigi paskon ĉe la maro en varma suda Italio?

![IJF](image://ijf_stipendio.png)

Jen la perfekta oferto por vi: GEJ, PEJ kaj IEJ restarigis triflankan stipendian programon, kiu permesas al GEJ senpage sendi unu partoprenanton al Internacia Junulara Festivalo, organizata de IEJ. La senpaga partoprenado postulas, ke vi surloke helpos al la organizantoj – sed vi tamen havos sufiĉe da tempo por ĝui la festivalon! La festivalo okazos de la 28.03.2018 ĝis la 03.04.2018 en Marina di Ascea, en la regiono Kampanio. Vi povas antaŭĝoji interesan programon rilate al la ĉi-jara temo "Unuiĝo aŭ sendependo?", belajn ekskursojn, bongustan manĝaĵon kaj afablajn partoprenantojn el multaj landoj. Por kandidatiĝi sendu mallongan motivleteron al dej@esperanto.de.

Pliaj informoj pri la festivalo: iej.esperanto.it/ijf/

Membroj de GEJ povas peti subvenciojn por vojaĝkotizoj al IJF!
