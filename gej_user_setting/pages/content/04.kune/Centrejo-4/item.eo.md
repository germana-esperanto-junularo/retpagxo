---
title: La 4-a Centrejo en Bartoŝovice
menu:  La 4-a Centrejo en Bartoŝovice
date:  04-05-2017
slug:  centrejo-4
taxonomy:
    tag: [Centrejo, Ĉeĥio, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Michaela Stegmaier
---

Laste mi partoprenis en la 4-a Centrejo, organizita de Ĉeĥa E-junularo. Ĝi okazis inter la 28-a de aprilo kaj la 1-a de majo en bela kasteleto en la ĉeĥa vilaĝo Bartoŝovice.

![Centrejo](image://centrejo-4.jpg)

Kelkaj germanoj hazarde kolektiĝis ĉe stacidomo kaj estis bonvenigitaj de Vit kaj Veronika, kiuj jam sidis en la buso al la kastelo. Alvenante ni portis niajn pezajn valizojn en la trian etaĝon, kie sidis Leon kaj Tomaŝ ĉe akceptejo kaj skribis ĉiujn kvitancojn permane, ĉar la printilo ne volis fari ilian laboron. Thomas kaj Andi preparis la programĉambron por la diskejo. Dum la vespero alvenadis preskaŭ ĉiuj 28 partprenantoj, inter ili ĉefe anoj de ĈEJ, SKEJ kaj E@I, sed ankaŭ ukrainino, ruso kaj hungaro. Ni ludis interkonajn ludojn kaj preparis tri tablojn por la trinkomanĝa nokto. ĈEJ batalis kontraŭ SKEJ, sed venkis la internacia tablo. Eble ĉar ni, internaciaj gastoj, ne povis atendi la komencon, manĝis la mem alportitajn frandaĵojn kaj jam ne plu kapablis ĉion de la batalantoj gustumi.

La sabato komenciĝis per manlaboro kun Paŭlina kaj lingvokurso de mama Jana, sekvis seminario pri videovarbado kaj poste la tuta grupo iris al bruligado de sorĉistinoj, organizita de la vilaĝanoj. Ni restis ĝis la sunsubiro kaj translokiĝis al la hejma diskejo. Dimanĉo estis ekskurso al muzeo pri ĉapoj, kie oni povis surmeti ĉiujn ekspoziciitajn artaĵojn. Poste la busŝoforo decidis porti nin al montaro, kie troviĝas ĉarma vilaĝo kun turo sur montopinto. La loka specialaĵo estas "oreletoj" kaj ni ĉiuj gustumis ilin. Reen en la ejon venis la etoso de la lasta vespero, la homoj iĝis eĉ pli babilemaj, sidis en granda rondo, lumigis la ĉambron nur kandeloj. Ekde noktomezo ĉiuj dancis sub ultraviola lumo, ornamis la korpojn per gluo, kiu reflektis tiun lumon kaj tute freneziĝis. Post kvar horoj da dormo ni iom post iom forlasis la ĉambrojn kaj faris promenon okaze de unua majo. Kara Ĵenja ŝoforis preskaŭ ĉiujn partoprenantojn al la stacidomo en Studénka – kaj jen la fino de tiu ĉarma aranĝo.

Malgraŭ ke mankis tempo por fari la varbfilmetojn kaj kelkfoje ŝanĝiĝis la programo, mi volas danki al la organizantoj, kiuj ĉiam havis duan planon, informis ĉiujn partoprenantojn kaj sukcesis senprobleme fari komunan foton. Estis granda plezuro kun vi ĉiuj pasigi tiun tempon kaj mi certe klopodas veni al la 5-a centrejo. Mi esperas, ke mi revidos kelkajn de vi dum IJK en Togolando, dum itala E-kongreso, dum JES en Ŝĉecino aŭ iam en Herzberg. Nepre aŭskultu la sonraporton en baldaŭa elsendo de Varsovia Vento. Michaela Stegmaier
