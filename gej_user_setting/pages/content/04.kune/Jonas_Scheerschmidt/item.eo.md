---
title: Jonas
menu:  Jonas
date:  10-08-2018
slug:  jonas-2018
taxonomy:
    tag: [personoj, esperantistoj]
    category: Personoj
author:
    name: Jonas Scheerschmidt
---

![Jonas](image://JonasScheerschmidt.png)

Mi kreskis proksime de la Esperanto-urbo Herzberg am Harz kaj ĉiam miris, kio ekzakte Esperanto estas. Iun tagon mi estis en la Esperanto-centro en Herzberg kaj tuj entuziasmiĝis pri la planita lingvo Esperanto. Kion mi tre aprezas, estas multaj internaciaj kunvenoj, kie vi povas renkonti homojn de ĉie en Eŭropo kaj ĉirkaŭ la mondo.

Pro tio, kaj ĉar mi ankaŭ tre interesiĝas pri eksterlandaj aferoj kaj internacia politiko kaj estas konvinkita eŭropano, mi aliĝis al la Germana Esperanto-Junularo.

Mi pensas, ke Esperanto havas grandan estontecon kiel monda lingvo kaj mi ankaŭ pensas, ke ni povas konvinki multajn homojn ĉar ĝi estas neŭtrala kaj tre simpla lingvo. Pro tio mi iros al la „Expolingua“ en Berlino kaj helpos ĉe la Esperanto-stando. Ĉu ankaŭ vi venos por subteni nin?
