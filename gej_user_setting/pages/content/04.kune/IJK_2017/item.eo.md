---
title: Unua IJK en Afriko - Togolando
menu:  Unua IJK en Afriko - Togolando
date:  30-08-2017
slug:  ijk-2017
taxonomy:
    tag: [IJK, Togolando, Afriko, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Jorinde Duhme
---

Dieses Jahr fand der IJK zum ersten Mal überhaupt in Afrika, Togo statt.TEJO hat über das Projekt AEJK (afrika eŭropa junulara kapabligo) über 70 Teilnehmer mit EU-Subventionen eingeladen und insgesamt waren 110 Teilnehmer und Organisatoren vor Ort in Aneho, in einem Schulgebäude direkt an der Atlantikküste. Die Deutsche Esperanto-Jugend wurde von 4 Vorstandsmitgliedern und einigen Mitgliedern gut vertreten.

![IJF](image://ijk_togolando.jpg)

La ĉijara Internacia Junulara Kongreso okazis la unuan fojon sur la
afrika kontinento, prezise en Aneho, Togolando. Kun helpo de TEJO kaj
subvencioj de la Erasmus+ programo junuloj de multaj landoj povis
partopreni en tiu evento. La 6-an de Aŭgusto la organiza teamo oficiale
bonvenigis nin kaj de tiam la kongreso povis komenci. Apud
Esperanto-lingvokursoj por komencantoj kaj progresantoj la programeroj
estis kunligitaj kun la temo „Viroj kaj virinoj kun *siaj* kulturoj“.
Tiel ni havis multajn eblecojn por interŝanĝi kun partoprenantoj de
variaj landoj kun variaj kulturoj ekzemple dum diskutrondoj aŭ prelegoj.
Speciala ĉijare estis ankaŭ la granda oferto de trejnadoj. Motivataj
trejnistoj alvenis por instrui pri la organizado de landaj agadoj, pri
dancado aŭ laboro en teamoj, kaj diskutis pri konduto de introvertulo.
La organizantoj ofertis ĉiutage tiom multe da programpunktoj ke vere ĉiu
partoprenanto ĉiam trovis interesan okupon. En lundo okazis la
Kultur-Lingva Festivalo (KLF), kie ĉiuj partoprenatoj reprezentis sian
landon kun kunportitaj maĝaĵoj, trinkaĵoj, vestaĵoj, kaj ankaŭ dancoj.
Vespere ni amuziĝis dum koncertoj de Togolandaj kantistoj, kaj kun la
esperantaj muzikantoj Jonny M kaj Kimo. Kompreneble ankaŭ ekzistis
ebleco partopreni en du ekskursoj. La tuttaga ekskurso al Kpalimé okazis
en ĵaŭdo. Du busoj transportis nin al la 4 horojn fora urbo Kpalimé por
vidi akvofalon kaj eĉ naĝi en ĝi. Poste ni vizitis kakao- kaj
kafoplantaĝon. Dum la duontaga ekskurso en merkredo al Togoville ni
prenis boatojn por traspasi la Togo-lagon kaj vizitis la urbon Togoville
kun siaj preĝejo, ĝemelarboj kaj vodu-fetiŝoj. Poste ni ankaŭ vizitis
iaman sklavodomon kaj ricevis interesajn informojn pri la sklavado en
Togolando. En tute estis tre speciala, interesa kaj belega IJK, ĉar la
partoprenantoj estis tre diversaj kaj eblis havi ege interesajn
diskutojn pri kuluraj malsamecoj. Por ni germanoj estis bele vidi, ke la
afrikaj partoprenantoj (kiu nombre superis eŭropanojn) estas tiom
motivataj kaj aktivaj en iliaj landoj. Multegaj novaj amikecoj estiĝis
kaj en tute estis ege bela sperto kaj ankaŭ aventuro por ni germanoj kaj
fakte por ĉiui. Dankegon al la organiza teamo kaj ĉiuj, kiuj helpis
realigi tiun projekton. 
