---
title: KEKSO während des GEK 2017 in Freiburg
menu:  KEKSO während des GEK 2017 in Freiburg
date:  21-05-2017
slug:  kekso-21
taxonomy:
    tag: [KEKSO, Freiburg, renkontiĝoj]
    category: KEKSO
author:
    name: Paul Würtz
---

Es war also wieder soweit, zum Deutschen Esperantokongress fand nun zumdritten Mal parallel das Neulingstreffen KEKSO (Kreema Esperanto Kurso)statt. Dank des netten Angebotes des DEB war das 21. KEKSO zujugendfreundlichen Konditionen in der Sporthalle untergebracht und einKursraum und eine Kursveranda sorgten für eine hochkonzentrierteLernatmosphäre.

Zum Abend trudelten 14 Leute aus ganz Deutschland ein und stellten schleunigst ihr Gepäck ab, um rechtzeitig zum Konzert von Jonny M. dasTanzbein zu schwingen. Nach einem ausgelassenen Tanzakt saßen alle nochzum Abend zusammen, lernten sich kennen, spielten ein paar Spiele undfüllten ihren ausgetanzten Kalorienhaushalt wieder auf.

Die geplante Aufteilung in Anfänger- und Fortgeschrittenenkurs wurde in einen Tutkomencula Kurs und einen für Progresantaj komencantojumgestaltet, weil es doch für einen Großteil der Leute die ersteEsperantoveranstaltung war. Die Sprachkurslektionen fanden im Wechselmit auflockernden Spielen und Essenspausen statt. Wir sprachen überunsere Motivation Esperanto zu lernen und neben durch bekannte Menscheninduzierte Neugier stellte das freundschaftliche Reisen durch die Weltdie häufigste Motivation für die neuen Teilnehmer dar.

Am zweiten Abend war nach dem Konzert der BANDNAME die Lust auf weiteresTanzen noch groß genug, sodass wir uns gemeinsam in die lokaleDiskofamosität „White Rabbit“ bewegten. Eine Nacht mit weniger als dreiStunden Schlaf später beobachteten wir gespannt dieMitgliederversammlung des DEB und die Neuwahlen dessen Vorstands.

Schnell verging die Zeit und es rückte der Abschied nahe.

Wir freuen uns schon auf den nächsten KEKSO in Herzberg während des 12.
TORPEDOs vom 29.9.-3.10.
