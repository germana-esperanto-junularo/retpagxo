---
title: Esperanto challenge
menu:  Esperanto challenge
date:  23-10-2014
slug:  esperanto_challenge
taxonomy:
    tag: [lingvo]
    category: Artikoloj
author:
    name: Redakcio
---

Brian Powers is the administrator and creator of Languages Around the
Globe, an online community of language enthusiasts. Brian writes and
maintains the LATG blog at [www.latg.org](http://www.latg.org) and its
associate social media. He lives in Ithaca, New York and holds a BA in
Cultural Anthropology from Franklin Pierce University.

\[Brian Powers estas la administranto kaj kreinto de Lingvoj Ĉirkaŭ la
Mondo (Languages Around the Globe, LATG), interreta komunumo de
lingvemuloj. Brian skribas en la LATG-blogo ĉe
[www.latg.org](http://www.latg.org/) kaj ties rilataj socimediaĵoj kaj
ilin tenadas. Li vivas en Itako, Novjorkio kaj akiris bakalaŭrecon de
Kultura Antropologio ĉe la Franklin-Pierce-Universitato.\]

**Languages Around the Globe; Esperanto Experiment**

In the year or so since becoming involved in the online language
enthusiast community my awareness of certain languages has increased
exponentially across the board, but one of the more intriguing languages
that I've had the pleasure to discuss and learn about has been
Esperanto. I had been trying to create community building projects or
experiments for a while without any great ideas when I came across an
article by polyglot Benny Lewis about how spending only two weeks of
Esperanto managed to significantly impact his understanding of other
European languages and was sufficient to bring him into a fascinating
new community of individuals around the world. We're all attracted to
the idea of more gain for less work, and that's exactly what grabbed my
attention about Esperanto. Creating a community experiment would be
perfect.

And so the LATG Esperanto Experiment was born. The goal of this project
is to determine first hand whether spending three weeks studying
Esperanto will be enough to impact future language learning projects,
particularly among adults outside of a classroom setting. To do this
adequately we're looking for a wide variety of individuals from
different linguistic backgrounds, with different future language goals,
and who may or may not have prior knowledge of Esperanto.

The experiment seemed to be an instant hit with over 100 individuals
expressing interest in participating within the first few days. A survey
requesting basic demographic information was sent to our respondents. No
specific requirements are asked of participants beyond putting forth a
modicum of effort towards progressing in the allotted time frame. Many
participants have stated that they plan to keep blogs or diaries of
their experiences and their methods throughout the Esperanto phase as
well as for the following several months.[]{#anchor}

We expect results to vary, likely based heavily on available resources
and personal motivation among participants. Some folks are interested in
pursuing Romance languages such as Spanish or French following the first
21 day study period. We do have participants interested in pursuing
Chinese and other languages that fall outside the Indo-European language
family from which Esperanto was primarily based. Some claim that
Esperanto may better equip learners for European languages, and less for
others. So it will be interesting to see how this plays out for our
non-European learners. Some of our volunteers already have some
knowledge of Esperanto.

We're still interested in finding additional participants interested in
the self-study of Esperanto and encourage anyone interested in
participating to check out our website at www.latg.org. With a hard work
we're looking forward to joining what promises to be a fantastic global
community.
