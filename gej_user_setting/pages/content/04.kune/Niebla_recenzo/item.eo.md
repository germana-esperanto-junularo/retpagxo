---
title: Recenzo de Niebla de Miguel de Unamuno
menu:  Recenzo de Niebla de Miguel de Unamuno
date:  09-11-2014
slug:  la_niebloj
taxonomy:
    tag: [rakonto, renkontiĝoj]
    category: Rakontoj
author:
    name: Justin Bai
---

Kiam oni legas bonan libron, oni eble eksentas, ke oni perdiĝas en la
fikcia mondo, kaj ke la efektiva mondo malaperis. La distingo inter
realeco kaj fikcio malklariĝas en la libro *Niebla* (*Nubleto*) de la
hispana aŭtoro Miguel de Unamuno.

*Niebla* komenciĝas kun prologo de Victor Goti, kiu diras, ke li konas
la aŭtoron. Tamen, Victor estas fikcia rolulo, kiu estas la amiko de la
ĉefrolulo. La ĉefrolulo estas Augusto Pérez, viro enamiĝanta kun la
virino Eugenia Domingo del Arco. Bedaŭrinde por Augusto, Eugenia jam
fianĉiniĝis kun alia viro.

En *Niebla*, Augusto provas gajni la korinklinon de Eugenia, adoptas
hundon (al kiu li parolas), kaj filozofie diskutas kun aliaj roluloj. La
amiko Victor decidas skribi “nivolon.” Kaj, kio estas “nivolo”? Esence,
nivolo estas romano, kiu ne estas romano. Victor komprenigas la naturon
de lia nivolo. Li diras, ke ĝi havos multajn dialogojn, kaj ke se la
rolulo estas sola, ekzistos hundo, al kiu la rolulo diras siajn
monologojn.

Diras Augusto al Victor, “Ĉu vi scias, Victor, ke mi havas la impreson,
ke vi estas inventanta min?”

“Eble mi estas!” respondas Victor.

Tia estas la filozofiaj diskutoj de la nivolo *Niebla*. *Niebla* estas
fama pro la parto, en kiu Augusto renkontas sian propran aŭtoron, Miguel
de Unamuno. Augusto komencas pensi pri lia ekzisto. Li meditas: “Mi
pensas, tial mi estas,” kaj, “Mi estas, tial mi pensas,” kaj eĉ, “Mi
manĝas, tial mi estas.”

Kvankam mi ne pensas, ke Augusto estas tre ŝatinda rolulo, la dialogo
kaj aliaj roluloj estas tre fascinaj. Rolulo, kiu interesis min, estas
Don Fermín, la onklo de Eugenia. Don Fermín estas anarkiisto kaj
esperantisto. Kiam la vortoj de ŝia onklino konfuzas ŝin, Eugenia diras,
“Nu, mi ne komprenas. Estus pli bone se vi parolus Esperanton, kiel
Onklo Fermín.”

Je alia okazo, Don Fermín diras al Augusto, ke li devas skribi la
familian nomon de Eugenia, del Arco, kun la litero K.

“Do, ĉu vi estas fonetikisto, ankaŭ?” demandas Augusto.

“Ankaŭ? Kial ‘ankaŭ’?” respondas Don Fermín.

“Ĉar vi jam estas anarkiisto kaj esperantisto.”

“Ĉiuj tiuj estas unu, sinjoro, ĉiuj tiuj estas unu,” diras Don Fermín.

Malgraŭ—aŭ, eble pro—la filozofia naturo de la libro, *Niebla* montriĝas
esti alloga verko.

*Niebla*n en la germanan (*Nebel*) tradukis Otto Buek.
