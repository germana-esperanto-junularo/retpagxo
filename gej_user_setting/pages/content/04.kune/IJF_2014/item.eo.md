---
title: IJF en Kastel’ Sarda
menu:  IJF en Kastel’ Sarda
date:  10-04-2014
slug:  ijf-2014
taxonomy:
    tag: [IJF, Italio, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Manuel Berkemeier
---

Ĉi tio estas raporto pri la IJF (Internacia Junulara Festivalo) 2014 en Kastel’ Sarda, Italujo.

![IJF](image://ijf-2014-2.jpg)

Estas mia unua raporto pri Esperanta junulara renkontiĝo, ĉar ankaū estis mia unua renkontiĝo (pli granda ol KEKSO). Mi vojaĝis al IJF kiel oficiala reprezentanto de la GEJ. Fakte, tio signifas, ke mi vojaĝis kvazaū senkoste. Do mi certe antaūĝojis pro ferioj en varma Italujo kaj pro espereble ekkoni mojosajn homojn.

La 16a de Aprilo mi ekvojaĝis kaj flugis de Dortmund al Alghero. Jam en
la germana flughaveno mi rimarkis personojn, kiuj ete konformis miajn
kliŝojn pri Esperantistoj. Ne surprize, tiu suspekto estis konfirmata
atendante la buson (kion oni ofte faras en Italujo). Tie, en la itala
bushaltejo, kolektiĝis kurioza grupo da homoj kaj mi unue koniĝis kun
Gunnar kaj sia ukulelo.

Komence, mi ne komprenis tiel multe, kiel mi estis esperinta. Sed la
aliaj estis helpemaj kaj mi poste rimarkis, ke ankaū partoprenis
personoj, kiuj eĉ komprenis pli malmulte ol mi. Ni estis likigataj en
granda gastejo, kiu ne vere estis luksa hotelo, sed certe sufiĉis. La
plej bona estis nia privata strando kun la plej pura maro. Je nia
alveno, ni tuj ricevis antaūgusto de la itala talento de organizado. La
oficejo ŝajnis iomete kaosa kaj ankaū servis kiel eta renkontiĝejo.
Plie, amiko kaj mi pasigis la unuan nokton sen littuko.

![IJF](image://ijf_2014_1.jpg)

Mi ne sciis, ke mi devu fari ion pro partopreni senkoste. Feliĉe la
“Voĉo” de la IJF, Michael Boris, klarigis al mi, ke mi estus honorata
helpi en la trinkejo. Sekve, mi ĉiuvespere kunlaboris tie. Tio verŝajne
ne vere profitiĝis, ĉar la helpantoj rajtis trinki senkoste. Cetere,
ankaū okazis la gufujo dum la vesperoj. Estis la plej eleganta loko kun
klasika muziko kaj teo. Tage estis oferta diversa programo. Oni povis
(aū eĉ devis) partopreni je interesaj kursoj ka je longaj ekskursoj al
Alghero aū Anglono. Aū oni nur restis strande. Aū iris al la urbo. Aū
serĉis manĝajojn…

Krom tio, okazis bonegaj koncertoj vespere. I.a. Amplifiki unue
koncertis ekde longa tempo.

Nedubeble, mi ĝojegis partopreni. La homoj mojosegis kaj (precipe la
verspera) programo estis amuza. Fine, mi volas mencii kelkajn aĵojn,
kiujn mi speciale memorigas:

-   La italoj fakte nur manĝas pastaĵojn.
-   La italoj ne ŝatas matenmanĝon.
-   La tempo en Italujo pasas pli komforte.
-   La tie-estantaj Afrikanoj ĉiukaze estis afablaj homoj.
-   Ĉiu rigardu/partoprenu inernacian vesperon!

Do, estis mia una renkontiĝo, sed certe ne estis mia lasta.
