---
title: Printempo, maro kaj itala etoso
menu:  Printempo, maro kaj itala etoso
date:  14-05-2018
slug:  ijf-2018
taxonomy:
    tag: [IJF, Italio, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: David Mamsch
---

– IJF en Marina di Ascea!

Al kiu aranĝo logas vin printempo, bela naturo kaj la maro? En kiu aranĝo vi neniam scias kiam la (tamen bongusta) manĝo finfine aperas? En kiu aranĝo estas varme ekstere kaj malvarme ene?

![IJF](image://IJF_2018.png)

Ĝuste! Temas pri IJF – la Internacia Junulara Festivalo en Italio!

Ĉirkaŭ pasko, fine de Marto, kunvenis en Marina di Ascea en suda Italio ĉirkaŭ 50 homoj, junaj kaj junaj en la koro, por kune pasigi bonan tempon en tre bela ĉirkaŭaĵo. Kiam en multaj partoj de Eŭropo ankoraŭ estis aĉa vintro, en suda Italio jam plene regis varma printempo kun sia tuta beleco kaj agrableco. Nia domaro estis ĉirkaŭitaj de florantaj montetoj kaj nur iom grimpinte supren jam bele videblis la maro.

Konsiderante tiajn ĉi kondiĉojn kompreneblas ke ne vere ĝenis onin ke la oficiala programo nek estis tiom serioza nek tiom plena. Krom kelkaj prelegoj okazis dumtage trejnado pri egaleco kaj alireblecoj, en kiu kreskis multaj indaj ideoj por aliaj venontaj aranĝoj. Vespere ne mankis tradiciaj programeroj kiel trinko-manĝa nokto kaj internacia vespero. Furoris tamen nekutimaj novaĵoj - opera koncerto kaj popolmuzika koncerto, kiuj ofertis al ni tre malsamajn, sed bongustajn impresojn de la loka suditala muziko kaj kulturo.

Dum la nokto la drinkejo kaj dancejo estis la taŭgaj adresoj por elspezi stelojn (Esperanto-mono) kaj la energion kiu restis post la aktivaĵoj de la tago.

El la programo aparte menciindas la ekskursoj. Okazis ekskursoj al romiaj temploj, la vulkano Vezuvio kaj al pliaj vidindaĵoj en la naturo – arbaraj montetoj kun belegaj ĉirkaŭvidoj, la plaĝo kie ondetoj kaj ondegoj disbatiĝis ĉe diversaj ŝtonoj kaj kie moŝte leviĝantaj krutaĵoj tuj apudis.

La manĝoj, kiam ili finfine aperis, laŭis la kliŝon pri Italio kaj estis treege bongustaj. Niaj gastigantoj sukcesis prezenti al ni diversajn lokajn specialaĵojn.

Finfine, kvankam la aranĝo ne estis tiom ege organizita, la etoso estis sufiĉe agrabla. Ja estis tiaj detaloj kiel ekzemple unue manko de akvoboligilo en gufujo kaj la neniam fidindaj horoj de vesperaj programeroj kaj manĝoj. Sed bonŝance (aŭ fakte, bedaŭrinde?) la kelkfoja manko de organizado sufiĉe kongruis kun la atendoj de la partoprenantoj kaj eĉ kelkaj organizantoj...

Resume – se vi ŝatas seriozan, kongresecan aranĝon kun vasta programo, ne iru al IJF. Se vi tamen ja ŝatas feriumadon en bela naturo, agrablan etoson en rondo de esperantaj amikoj, nepre venu – al la Internacia Junulara Festivalo en Italio.

David Mamsch
