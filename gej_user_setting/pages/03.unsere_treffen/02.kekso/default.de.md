
---
title: KEKSO
menu:  KEKSO
taxonomy:
    tag:
    category:
---

## KEKSO

KEKSO steht für "Kreema Esperanto-KurSO". Es handelt sich um ein Treffen,
organisiert von der Deutschen Esperanto-Jugend, das regelmäßig mindestens
zweimal jährlich angeboten wird. In der Regel findet das KEKSO jeweils an
einem Wochenende im Frühjahr und im Herbst statt. Der Veranstaltungsort
wechselt jedes Mal, sodass die Treffen bunt gestreut in ganz Deutschland
stattfinden. Bisherige Veranstaltungsorte waren zum Beispiel: Wuppertal,
Potsdam, Soest, Karlsruhe, Nürnberg oder Weinstadt bei Stuttgart.  
Es handelt sich dabei um ein eher kleines Esperanto-Treffen mit regelmäßig
etwa 15 bis 20 Teilnehmenden; perfekt also für den Einstieg in die bunte
Esperanto-Welt.

<div id="item" class="column col-6 col-mx-auto col-md-12 extra-spacing">
<figure class="figure">
    <img class="img-responsive" src="/retideoj/gej/user/pages/images/KEKSO_Logo.png">
    <figcaption class="figure-caption text-right"><small>© Logotipo kreita de <a href="https://jonny-m.org/">Jonny M</a></small></figcaption>
 </figure>
</div>

In erster Linie richtet sich das KEKSO an Jugendliche, die sich für Esperanto
interessieren und sich einfach mal anschauen wollen, wie die Sprache
funktioniert. Es gibt Sprachkurse verschiedener Niveaus für Anfänger*innen und
Fortgeschrittene, wo in Kleingruppen die Regeln der Sprache erlernt werden
können. Daneben werden auch wechselnde Workshops zur Umsetzung konkreter
Projekte angeboten, in denen die neu erlernten Kenntnisse schnell angewendet
werden können. Dies kann zum Beispiel die Produktion eines Hörspiels oder
eines kleinen Films sein. Aber auch kleine Theaterstücke sind denkbar.

Vor allem soll das Wochenende Spaß machen. Die oft sehr unterschiedlichen
Teilnehmenden aus allen Ecken Deutschlands und darüber hinaus lernen sich
untereinander kennen. Es wird gemeinsam gespielt und gekocht, Musik gemacht,
etc.

## Nächstes KEKSO in Neumünster

Vom **7\. bis 10. Juni 2019** findet das KEKSO zusammen mit dem Deutschen
Esperanto-Kongress (GEK) in **Neumünster** statt. Da es das 25. KEKSO ist,
wollen wir mit euch dieses Jubiläum feiern. Dabei ist eure Krativität gefragt.
Wir wollen den GEK ein bisschen aufmischen. Ihr könnt verkleidet kommen,
Spiel- und Programmideen an uns senden und wir kümmern uns um den Rest.

Die Unterkunft hat für uns eine Küche, in der wir unsere veganen Speisen
zubereiten können und mit Teilnehmern des Kongresses ins Gespräch kommen
können. Geschlafen wird wie üblich mit Isomatte und Schlafsack in der
Massenunterkunft.

Es wird auch diesmal einen Anfänger*innen-Sprachkurs im Programm geben.
Außerdem freuen wir uns auf eure Beiträge, die ihr im Anmeldeformular angeben
könnt (oder gerne auch in unserem [Kontaktformular](../../kontakt)).

**Kosten für Erstteilnehmende an einem Esperantotreffen** : 0€  
**Kosten für Mitglieder der DEJ** : 30€  
**Kosten für Nicht-DEJ Mitglieder** : 45€

Im Teilnahmebeitrag sind Unterkunft und die Mahlzeiten an dem Wochenende mit
inbegriffen. Wir bitten euch trotzdem, etwas Obst oder Kekse und vielleicht
das Rezept eures Lieblingsessens mitzubringen.

<a class="btn btn-success" href="./kekso/anmeldung" style="color: white !important;">Jetzt zur Anmeldung!</a>
