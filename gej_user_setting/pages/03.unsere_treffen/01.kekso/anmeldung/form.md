---
title: 'Anmeldung KEKSO'
form:
    name: kekso-form
    fields:
        -
            name: name
            label: Name oder Spitzname/Nomo aŭ kromnomo
            placeholder: 'Dein Name/Via Nomo'
            type: text
            validate:
                required: true
        -
            name: email
            markdown: true
            label: "Email/Retpoŝtadreso"
            placeholder: 'Deine Email-Adresse/Via retpoŝtadreso'
            type: email
            validate:
                required: true
        -
            name: tel
            label: "Handy/telefonnumero"
            placeholder: 'Handy/telefonnumero'
            type: tel
        -
            name: plz
            label: "Postleitzahl/Poŝtkodo <small>Um ggf. mit anderen Teilnehmern Fahrgemeinschaften oder Fahrgruppen bilden zu können</small>"
            placeholder: 'Postleitzahl/Poŝtkodo'
            type: text
        -
            name: nakigxtago
            type: date
            label: 'Dein Geburtsdatum/Via naskiĝdato'
            default: '2015-01-01'
            validate:
                min: '1985-01-01'
                max: '2015-13-13'
                required: true
        -
            name: lingonivelo
            label: 'Deine Sprachniveau/Via lingvonivelo'
            placeholder: Auswählen/Elektu
            type: select
            options:
                flue: fließend/flue
                meze: 'geht so/meze'
                iomete: 'ein bisschen/iomete'
                komencanto: Anfänger*in/komencanto
            validate:
                required: true
        -
            name: diät
            label: Diät/Manĝkutimoj
            placeholder: Auswählen/Elektu
            type: select
            options:
                ĉio: 'Auch Fleisch/Ankaŭ vianda'
                ovo_lakto_vegetara: Ovo-Lakto-Vegetarisch/Ovo-Lakt-aĵ-vegetarana
                senlakta_vegetara: Laktosefrei/Senlaktoza
                vegana: Vegan/Vegana
                diabeta: Diabetisch/Diabeta
                kosxera: Koscher/Koŝera
                alia: 'Anderes (bitte im Kommentar spezifizieren)/Alie (bonvole klarigu en la komentaro)'
            validate:
                required: true
        -
            name: monkontribuo
            label: 'Teilnahmebeitrag/ Partoprenkotizo'
            placeholder: Auswählen/Elektu
            type: select
            options:
                novulo_senkoste: 'Das ist mein erstes Treffen, muss also  0 Euro zahlen/ Temas pri mia unua renkontiĝo - mi devas pagi 0 eŭrojn'
                eksterlandano_sekoste: 'Ich komme aus dem Ausland, muss also  0 Euro zahlen/ Mi venas el eksterlando - mi devas pagi 0 eŭrojn'
                gejmembro_rabatita: 'Ich bin Mitglied der DEJ, muss also 30 Euro zahlen/ Mi estas membro de GEJ - mi devas pagi 30 eŭrojn'
                normala_prezo: 'Nichts der obigen - ich muss 45 Euro zahlen/ Nenio supre menciita - mi devas pagi 45 eŭrojn'
            validate:
                required: true
        -
            name: keksoj
            type: radio
            label: 'Kekse/ Keksoj'
            options:
                keksoj: 'Ich bringe selbst Obst und Kekse mit/Mi mem kunportos fruktojn kaj keksojn'
                keksotakso: 'Ich zahle 10 Euro mehr und bringe kein Obst und keine Kekse mit/Mi pagos pliajn 10 eŭrojn kaj ne mem kunportos fruktojn kaj keksojn'
            validate:
                required: true
        -
            type: radio
            name: personaj_datumoj
            label: 'Persönliche Daten/ Personaj datumoj [1]'
            markdown: true
            options:
                Ja: Ja./Jes.
                Nein: Nein./Ne.
            validate:
                required: true
        -
            type: display
            size: medium
            label: "[1]"
            markdown: true
            content: 'Ich bin damit einverstanden, dass meine Kontaktdaten (Name, PLZ, E-Mail) an andere Teilnehmende weitergegeben werden können, um Fahrgemeinschaften zu organisieren. </br> Mi konsentas ke miaj kontaktdatumoj (nomo, poŝtkodo, retpoŝto) povas esti sendotaj al la aliaj partoprenantoj por organizi kunveturadon.'
        -
            type: radio
            name: fotos
            label: Fotos/Fotoj [2]
            markdown: true
            options:
                Ja: Ja./Jes.
                Nein: Nein./Ne.
            validate:
                required: true
        -
            type: display
            size: medium
            label: "[2]"
            markdown: true
            content: 'Ich bin damit einverstanden, dass während der Veranstaltung Fotos von mir gemacht und später gegebenenfalls in der Mitgliederzeitschrift, auf der Website und/oder in sozialen Netzwerken veröffentlicht werden dürfen. </br> Mi konsentas ke dum la eventoj mi povos esti fotita kaj ke tiuj fotoj poste povos esti publikitaj en nia magazino, la retpaĝo kaj/aŭ en en sociaj retoj.'
        -
            name: rimarkoj
            type: textarea
            label: 'Bemerkungen (Programmbeiträge, Hinweise, medizinische Besonderheiten)/ </br>Komentoj (programkontribuoj, sciindaĵoj, kuracaj apartaĵoj)'
    buttons:
        -
            type: submit
            value: Anmelden
    process:
        -
            email:
                from: '{{ config.plugins.email.from }}'
                to:
                    - '{{ config.plugins.email.to }}'
                    - '{{ form.value.email }}'
                subject: '[KEKSO Anmeldung] {{ form.value.name|e }}'
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: Danke!
        -
            display: angemeldet
---

## Anmeldung zum KEKSO

Hier kannst du dich zum nächsten KEKSO anmelden?

Was ist das KEKSO? Wo findet es statt? Das kannst du [hier](..) nachlesen!

Wenn du wissen willst, was die Leute denken, die auf den letzten KEKSOs waren dann schau mal [hier](../../../kune/tag:KEKSO) rein.

<div class="divider text-center" data-content="ANMELDEFORMULAR"></div>

