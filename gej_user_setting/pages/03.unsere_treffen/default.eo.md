---
title: 'Niaj renkontiĝoj'
hide_git_sync_repo_link: false
menu: 'Niaj renkontiĝoj'
---

## KEKSO

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      KEKSO, mallonge por "Kreema Esperanto-KurSO", estas renkontiĝo organizata de la Germana Esperanto Junularo. Ĝi okazas dufoje jare, dum printempa kaj aŭtuna semajnfinoj. La loko ŝanĝiĝas ĉiufoje, por ke la renkontiĝoj okazu en tuta Germanio. Ĝis nun estis inter alie vizititaj: Rostock, Potsdam, Soest, Karlsruhe kaj Hamburgo. La eta Esperanto-renkontiĝo havas 8 ĝis 15 partoprenantojn. En 2019 KEKSO okazis jam la 25-an fojon.
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <a href="unsere_treffen/kekso"><figure class="figure">
      <img class="img-responsive" src="/retideoj/gej/user/pages/images/KEKSO_Logo.png">
   </figure></a>
  </div>
</div>

## JES

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <a href="unsere_treffen/jes"><figure class="figure">
  <img class="img-responsive" src="/retideoj/gej/user/pages/images/jes.png">
  </figure></a>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      La Junulara E-Semajno (JES) estas novjara junulara esperanto-aranĝo organizata de la Pola Esperanto Junularo (PEJ) kaj la Germana Esperanto-Junularo (GEJ). Ĝi okazas ĉiujare dum lastaj tagoj de decembro kaj unuaj tagoj de januaro, proksimume dum unu semajno, en iu loko en Meza Eŭropo. La pinto de la evento estas la silvestra balo.
  </div>
</div>

## eventaservo

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      Ni estas reprezentata ĉe eventaservo, retejo por diskonigi eventojn en esperantujo. Tie vi povas vidi ĉiujn eventojn, kiujn ni aŭ organizas aŭ subtenas. Informiĝu tie pri ĉiuj grandaj kaj malgrandaj renkontiĝoj de ni. Se vi volas anonci eventon, kiun vi organizas, pere de nia konto, <a href="../kontakt">kontaktu nin</a> volonte :)
  </div>
  <div id="item" class="columns column col-6 col-md-12 extra-spacing">
    <div class="column col-2"></div>
    <a class="column col-8" href="https://eventaservo.org/o/GEJ">
    <figure class="figure">
        <img class="img-responsive" src="unsere_treffen/es.jpg">
    </figure></a>
  </div>
</div>

## En la mondo

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <a href="unsere_treffen/internaciaj_renkontigxoj"><figure class="figure">
  <img class="img-responsive" src="unsere_treffen/en_la_mundo_thumb.png">
  </figure></a>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      <p>Se vi ankoraŭ ne scias kion fari dum viaj ferioj, aŭ ĉiam volis ekkoni Eŭropon pli bone, jen kelkaj esperantorenkontiĝoj, kie sin ofertas la ŝanco por tio! Tiuj estas sufiĉe grandaj kaj tradiciaj renkontiĝoj kun bunta programo kaj internaciaj vizitantoj, do ili ofertas perfektan eblecon por praktiki Esperanton kaj sperti esperantujon.</p>
      <p>Se vi vizitas internacian renkontiĝon, sciu pri niaj <a href="dej/fahrtkosten">subvencioj por vojaĝkostoj</a>! Se vi ŝatas sciigi nin pri via sperto dum la renkontiĝo, volonte skribu al ni artikolon por nia membrogazeto <a href="kune">kune</a>.</p>
  </div>
</div>
