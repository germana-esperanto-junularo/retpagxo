
---
title: Internaciaj renkontiĝoj
menu:  Internaciaj renkontiĝoj
taxonomy:
    tag:
    category:
---

## Internaciaj renkontiĝoj

Se vi ankoraŭ ne scias kion fari dum viajn feriojn, aŭ ĉiam volis ekkoni pli bone eŭropo, jen kelkajn esperanto renkontiĝojn kie sin ofertas ŝancon! Tiuj estas sufiĉe grandaj kaj tradiciaj renkontiĝojn kun bunta programo kaj internaciaj vizitantoj, do ili ofertas perfektan eblecon por praktiki kaj sperti esperanto kaj esperantujon.

Se vi vizitas la renkontiĝon informu vin pri niaj <a href="../dej/fahrtkosten">vojaĝsubvenciojn</a>! Se vi sciigi nin pri via sperto dum la renkontiĝojn volonte skribu al ni artikolon por la <a href="../kune">kune</a>.

### Internacia Junulara Kongreso (IJK)

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      <p>La Internacia Junulara Kongreso (IJK) estas la oficiala ĉiujara kongreso de TEJO kaj kutime fariĝas la plej granda junulara esperanto-renkontiĝo de la jaro en la mondo. Ĝi okazas tradicie dum unu semajno en julio aŭ aŭgusto, ofte tuj antaŭ aŭ post la UK, kaj normale ĉeestas ĝin ĉirkaŭ 300 junuloj. La 43-a IJK okazinta en 1987 en Krakovo atingis la rekordan nombron de pli ol 1000 kongresanoj. </p>
      <p><a href="https://www.tejo.org/cause/internacia-junulara-kongreso/">Jen</a> la aliĝilo!</p>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <figure class="figure">
    <a href="https://www.tejo.org/cause/internacia-junulara-kongreso/">
      <img class="img-responsive" src="internaciaj_renkontigxoj/ijk.png">
    </a>
   </figure>
  </div>
</div>

<div class="columns text-center">
<div class="col-12" style="margin-top:20px;margin-buttom:20px;">
<a href="../kune/tag:IJK">Legu pli la spertoj de IJK en la kune</a>
</div>
</div>

### Somera Esperanto-Studado (SES)

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <figure class="figure">
      <a href="https://ses.ikso.net/">
        <img class="img-responsive" src="internaciaj_renkontigxoj/ses.png">
      </a>
   </figure>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      <p>Somera Esperanto-Studado, en 2007 nomata Slava Esperanto-Studado, (mallongigo SES) estas la plej granda internacia Esperanto-renkontiĝo celanta lernadon de Esperanto.Ĉiujare, ekde 2007, ĝi okazas somere en Slovakio, kun escepto en 2014, kiam ĝi krom Slovakio okazis ankaŭ en Rusio. Ĝia programo krom instruado konsistas ankaŭ de akompanaj kultur-amuzaj programeroj.</p>
      <p><a href="https://ses.ikso.net/">Jen</a> la aliĝilo!</p>
  </div>
</div>

### Internacia Junulara Semajno (IJF)

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      <p>La Internacia Junulara Festivalo (IJF) estas porjunula Esperanto-aranĝo organizata de la Itala Esperantista Junularo ĉiujare en alia itala urbo. Ĝi kutimas okazi dum la paskaj lernejaj ferioj. </p>
      <p>La unua IJF okazis en 1977. La aranĝo havas plej ofte ĉirkaŭ 100 partoprenantojn; la plej granda (kun 325 partoprenantoj) estis la 24-a IJF, okazinta en 2000 en Cavallino. </p>
      <p><a href="http://iej.esperanto.it/eo/la-festivalo/">Jen</a> la aliĝilo!</p>
  </div>
  <div id="item" class="columns column col-6 col-md-12 extra-spacing">
    <div class="column col-2"></div>
    <a href="http://iej.esperanto.it/eo/la-festivalo/">
      <img class="column col-8 img-responsive" src="internaciaj_renkontigxoj/ijf.jpg">
    </a>
  </figure>
  </div>
</div>

<div class="columns text-center">
<div class="col-12" style="margin-top:20px;margin-buttom:20px;">
<a href="../kune/tag:IJF">Legu pli la spertoj de IJF en la kune</a>
</div>
</div>

### Internacia Junulara Semajno (IJS)

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <a href="http://ijs.hu/">
      <img class="img-responsive" src="internaciaj_renkontigxoj/ijs.png">
    </a>
  </figure>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      <p>La Internacia Junulara Semajno, mallongigite IJS, hungare Nemzetközi Ifjúsági Eszperantó Fesztivál estas la plej granda ĉiujara, internacia renkontiĝo de la Hungara Esperanto-Junularo (HEJ), kaj unu el la plej grandaj internaciaj Esperanto-renkontiĝoj en la mondo. </p>
      <p><a href="http://ijs.hu/">Jen</a> la aliĝilo!</p>
  </div>
</div>

### Artaj Konfrontoj en Esperanto (ARKONES)

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      Arkones (mallongigo de Artaj Konfrontoj en Esperanto) estas E-renkontiĝo okazanta ĉiujare en Poznano, Pollando. Ĝi estas organizata de E-SENCO tuj post studsesio de Interlingvistikaj Studoj ĉe la Universitato Adam Mickiewicz, en septembro. En 2014 okazis la 30-a (jubilea) Arkones. 
      <p><a href="http://arkones.org/">Jen</a> la aliĝilo!</p>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <a href="http://arkones.org/">
      <img class="img-responsive" src="internaciaj_renkontigxoj/arkones.png">
    </a>
  </figure>
  </div>
</div>