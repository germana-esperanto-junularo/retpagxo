
---
title: Junulara E-Semajno (JES)
menu:  Junulara E-Semajno (JES)
taxonomy:
    tag:
    category:
---

## Junulara E-Semajno

La Junulara E-Semajno (JES) estas novjara junulara esperanto-aranĝo organizata de
Pola Esperanto Junularo (PEJ) kaj Germana Esperanto-Junularo (GEJ). Ĝi okazas ĉiujare
dum lastaj tagoj de decembro kaj unuaj tagoj de januaro, proksimume dum unu semajno,
en iu loko en Meza Eŭropo. Laŭ formo, sed ne laŭ sezono, ĝi similas do al Internacia
Junulara Kongreso.

<div id="item" class="column col-6 col-mx-auto col-md-12 extra-spacing">
<figure class="figure">
<img class="img-responsive" src="/retideoj/gej/user/pages/images/jes.png">
</figure>
</div>

JES estas kuniĝo de du antaŭaj novjaraj junularaj esperanto-aranĝoj – la Ago-Semajno (AS) de PEJ kaj la Internacia Seminario (IS) de GEJ,[1] kiuj okazadis aparte ĝis la jarŝanĝo 2008/2009 (tiam okazis la 7-a AS kaj la 52-a IS).[2]

> vikipedio

**Wann und wo das nächste JES stattfindet, kannst du [hier](http://jes.pej.pl/de/) nachlesen.**
