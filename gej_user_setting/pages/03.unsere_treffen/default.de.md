---
title: 'Unsere Treffen'
hide_git_sync_repo_link: false
menu: 'Unsere Treffen'
---

## KEKSO

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
     Das KEKSO, kurz für "Kreema Esperanto-KurSO" (kreativer Esperantokurs), ist ein Treffen der Deutschen Esperanto-Jugend, das unter anderem als Einstieg für Neulinge in die Esperanto-Welt gedacht ist. Es wird zweimal im Jahr, an je einem Wochenende im Frühjahr und im Herbst, veranstaltet. Jedes Mal besucht es eine andere Stadt, damit es Jugendliche überall in Deutschand erreicht. Bisher gab es KEKSOs z.B in: Rostock, Potsdam, Soest, Karlsruhe und Hamburg. Das KEKSO ist ein kleines Treffen, es kommen meist zwischen 8 und 15 Teilnehmer. 2019 fand das KEKSO zum 25. Mal statt.
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <a href="unsere_treffen/kekso"><figure class="figure">
      <img class="img-responsive" src="/retideoj/gej/user/pages/images/KEKSO_Logo.png">
   </figure></a>
  </div>
</div>

## JES

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <a href="unsere_treffen/jes"><figure class="figure">
  <img class="img-responsive" src="/retideoj/gej/user/pages/images/jes.png">
  </figure></a>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      Die Jugend-Esperanto-Woche JES (Junulara E-Semajno) ist ein Jugendtreffen um Neujahr herum, das von der Polnischen Esperanto-Jugend (PEJ) und der Deutschen Esperanto-Jugend (DEJ) gemeinsam organisiert wird. Es beginnt kurz nach Weihnachten und geht bis ins neue Jahr hinein und dauert etwa eine Woche. Meist findet es abwechselnd in Polen und Deuschland statt. Den Höhepunkt des Treffens bildet der Silvesterball.
  </div>
</div>
<!-- wollen wir hier die PEJ noch verlinken? -->
## eventaservo

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      Wir sind auf der Website eventaservo vertreten, die Esperantotreffen überall auf der Welt bewirbt. Dort kannst du alle Treffen finden, die wir organisieren oder unterstützen. Informiere dich dort über unsere kleinen und großen Treffen. Wenn du selbst ein Treffen organisierst und es über unser Konto veröffentlichen willst, dann <a href="../kontakt">kontaktier uns</a> gerne :)
  </div>
  <div id="item" class="columns column col-6 col-md-12 extra-spacing">
    <div class="column col-2"></div>
    <a class="column col-8" href="https://eventaservo.org/o/GEJ">
    <figure class="figure">
        <img class="img-responsive" src="unsere_treffen/es.jpg">
    </figure></a>
  </div>
</div>

## Weltweit

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <a href="unsere_treffen/internaciaj_renkontigxoj"><figure class="figure">
  <img class="img-responsive" src="unsere_treffen/en_la_mundo_thumb.png">
  </figure></a>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      <p>Wenn du noch nicht weißt, was du in deinen Ferien machen willst, oder immer schon Europa besser kennenlernen wolltest, dann gibt es hier ein paar Esperanto-Treffen, die dir dazu die Chance bieten! Es handelt sich um eher große und traditionelle Treffen mit einem bunten Programm und internationalen Teilnehmern, die also die perfekte Gelegenheit bieten, Esperanto zu praktizieren und "esperantujo" zu entdecken und zu erleben.</p>
      <p>Wenn du eins dieser Treffen besuchst, informiere dich über unsere <a href="dej/fahrtkosten">Fahrtkostensubventionen</a>! Und wenn du deine Erfahrungen auf diesen Treffen mit uns teilen möchtest, dann schreibe gerne einen Artikel für unsere Mitgliederzeitschrift <a href="kune">kune</a>.</p>
  </div>
</div>
