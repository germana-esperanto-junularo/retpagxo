---
title: Raporto de Janett Keilholz
menu:  Raporto de Janett Keilholz
date:  25-04-2017
slug:  janett-2017
taxonomy:
    tag: [JES, personoj, esperantistoj, renkontiĝoj]
    category: Personoj
author:
    name: Janett Keilholz
---

![Grupo](image://janett.jpg)

Raporto de Janett Keilholz, kiu partoprenis KEKSOn en Würzburg

La pasinta JES estis mia unua renkontiĝo sed kompreneble ne estos mia
lasta. Mi ŝategis festi la novan jaron kun aliaj esperantistoj ĉar estis
tre bela etoso. Mi aŭdis kelkajn prelegojn, partoprenis multajn
interesajn kursojn kaj dancis ĝis la nokto-fin. Sed eĉ pli grava: la
afablajn homojn kiujn mi povis ekkoni. Grandan dankon al la organizteamo
kaj espereble ĝis la venonta jaro! 
