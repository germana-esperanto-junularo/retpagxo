---
title: IJF - 2019
menu:  IJF - 2019
date:  15-09-2019
slug:  ijf-19
taxonomy:
    tag: [IJF, Italio, renkontiĝoj]
    category: IJF
author:
    name: Julia Berndt
---


Dum Pasko 2019 denove okazis la Internacia Junulara Festivalo (IJF) en Italio, ĉi-foje en la mara urbeto Viserbella apud Rimini. Kiel ĉiam, oni povis ĝui bongustan italan manĝaĵon kaj ankaŭ la proksimega plaĝo (ĉirkaŭ 1 minuton piede for de la ejo) estis invitanta. Kelkaj kuraĝuloj eĉ naĝis! 

![Keksoj](image://IJF.jpg)

Du ekskursoj portis nin al vizitindaj lokoj de la regiono: Dum la tuttaga ekskurso ni iris al la belaj urbetoj Sant’Arcangelo kaj San Leo, kaj dum la duontaga ekskurso ni eĉ povis vojaĝi al lando for de Eŭropa Unio: San Marino. Estis iom domaĝe ke la programo ĉi-jare ne estis tiom riĉa kaj ke ni estis pli malmultaj partoprenantoj ol dum la lastaj jaroj. Pro tio kelkfoje mankis al mi la etoso de granda, vigla Esperanto-aranĝo. Sed tamen mi ĝojis revidi geamikojn kaj ekkoni novajn personojn, viziti interesajn lokojn en meza Italio kaj pasigi Paskon ĉe la maro.

![Keksoj](image://IJF_grupfoto.jpg)