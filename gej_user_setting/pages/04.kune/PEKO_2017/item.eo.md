---
title: PEKO 2017
menu:  PEKO 2017
date:  14-03-2017
slug:  peko-2017
taxonomy:
    tag: [PEKO, verdajskoltoj, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Tine Van Den Bruel
---

PEKO estas por mi ĉiam mallonga rompo de la realeco. Mi atendas revidi delongajn amikojn, sed ankaŭ povas renkonti nekonatajn esperantistojn.

![PEKO](image://peko-1.jpg)

Ĉi jare estis la unua Peko, en kiu mi partoprenis kun Timm. Ni ĉi jare aliĝis malfrue kaj pro mia gravedo ni iris aŭte. Ne malbona ideo, ĉar la kutima veturisto de kaj al stacidomo ĉi jare bedaŭrinde ne povis veni, do Valère, la organisanto havis duoblan taskon.

Kiam ni alvenis, unue estis kelke ĥaosa situation pri ĉambroj, ĉar eĉ homoj kiuj ne antaŭe aliĝis, alvenis. Sed finfine la organiza teamo sorĉis kaj trovis bonan solvon por ĉiuj.

La plej interesa dormaranĝo estis farita por la Verdaj Skoltoj, kiuj ĉiuj dormis en unu ĉambro, surplanke kaj eĉ sur tabloj. Tiu grupo estas gvidata de Valère, kaj ili kunvenas ĉirkaŭ ĉiu monato, ie en cirklo de ĉirkaŭ 200 km ĉirkaŭ Luksemburgio. En ĝi estas pluraj naciecoj, kaj kompreneble ankaŭ pluraj denaskaj lingvoj. Mirinda initiato de Valère, ĉar preskaŭ ĉiuj iamaj aliĝintoj daŭre entusiasmegas.


Mi dum la renkontiĝo okupiĝis pri trikado ĉe Christine, aŭskultis prelegon de Yves pri Esperantodisvastigo en la reto, lernis pli pri pasporta servo, kaj ĝiaj defioj kaj vespere eĉ kantis. Intertempe mi apenaŭ vidis Timm, kiu aliĝis al preskaŭ ĉiuj TEJO-studsesioj pri merkatiko.

Pri manĝo oni povas diri: Ne estis granda elekto, sed la kuiristo mirakle okupiĝis pri la multaj malsamaj manĝreĝimoj. Islamano, vegano, vegetarano, senglutenulo, senlaktosa … nenio estis tro multe. Iun tagon estis eble ĉirkaŭ 7 malsamaj bovloj por ĉiuj malsamaj postuloj, kaj tiu en grupo de ĉirkaŭ 70 homoj...

![PEKO](image://peko-2.jpg)

Promenanta, kuŝanta en suno, instruanta trikado … oni apenaŭ povis maltrafi la fotemulojn, Lode entusiasmigis ilin, multe foti pere de liaj pluraj rilataj kursoj. Tio donis belajn fotojn de homoj kun novtrikitaj “pussyhat”, naturajn fotojn, kaj de pluraj okazaĵoj dum la renkontiĝo.

Kompreneble ankaŭ la etuloj estis bone prizorgitaj, ĉefe de Nicky. Feliĉe ŝi ne nur prizorgas etulojn, sed ankaŭ pli grandaj uloj rajtas aliĝi, ĉe ŝi mi povis fari belan sakon por mia naskiĝonta etulo.

Krome eblis ludi. Pluraj homoj kunportis ludojn. Plej memorebla estis vespera homlupa ludo, en la ĉambro de la Verdaj Skoltoj. Kvankam estas bedaŭrinda ke ĉiam homoj estas eligitaj de la grupo, daŭre estas interesa malkovri interhomajn rilatojn.

Do, ĉu mi konsilas? Certe jes. Estas bela kaj interesa renkontiĝo, por ĉiuj, krom eble por tiuj kiuj valoras feriojn per hotelsteloj, anstataŭ per Esperantosteloj.
