---
title: Sinprezento - Lars Hansen
menu:  Sinprezento - Lars Hansen
date:  01-05-2017
slug:  sinprezento_lars
taxonomy:
    tag: [personoj, esperantistoj]
    category: Personoj
author:
    name: Lars Hansen
---

Mi estas Lars kaj mi estas la nova kasisto de GEJ. Mi havas 24 jarojn kaj kreskis en norda Germanio – ĉefe Schwerin kaj Kiel.

![Lars](image://lars.jpg)

Mi eklernis Esperanton en 2010 ĉe lernu. M net kaj vizitis JES en Burg en tiu jaro. Sekvontjare mi komencis studi informadikon en Magdeburg kaj renkontis aliajn esperantistojn kaj rete kaj surloke. Dum tiu tempo mi pli kaj pli esperantistiĝis kaj ĉiujare vizitis JES. Post la unua studado mi faris grandan vojaĝon tra la someraj aranĝoj 2015 – Roskilde, SES, UK, IJK, IJS. En tiu jaro mi ankaŭ komencis majstran studadon de informadiko kaj lingvoscienco en Edinburgo kaj ĝuis la tempon kun britaj esperantistoj, ekzemple dum la skota kongreso. Nun mi finstudis kaj komencas labori ĉe Stuttgart.

Dum la sekvontaj du jaroj mi volas helpi pri la organizado de JES kaj kompreneble zorgi pri la financaj aferoj. Kun Jarno kaj multaj aliaj mi laboros pri bona ŝanĝo de kasistoj, por ke vi laŭeble ne rimarku ilian ŝanĝon. Mi kore dankas al ili.

Mi antaŭĝojas renkonti vin ĉe GEK/KEKSO, JES aŭ eĉ IJK en Togolando!
