---
title: La KKPS de Klara
menu:  La KKPS de Klara
date:  30-10-2017
slug:  kkps-2017
taxonomy:
    tag: [KKPS, Nederlando, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Klára Ertl
---

Ĉi-jare mi partoprenis la KKPS-n (Klaĉ-Kunvenon Post-Someran) por la 4-a fojo, se mi bone memoras. Ĝi estas semajnfina renkontiĝo, kiun NEJ (la Nederlanda Esperanto-Junularo) organizas ĉiujare dum Haloveno.

![KKPS partoprenantoj](image://kkps1.jpg)

Ĝi altiras ĉefe, sed ne nur, junulojn, kaj ne nur nederlandajn, sed ankaǔ belgajn, germanajn kaj pli forajn Esperantistojn. Ĝi havas varman etoson, pro tio ke la vintro alproksimiĝas kaj multaj de la partoprenantoj jam konas unu la alian. Ĉi-jare ĝi okazis en Hunsel, vilaĝo proksime de Weert, en Limburgo, la plej suda regiono de la lando.

Tradicie KKPS estas renkontiĝo de klaĉoj kaj ludoj, kvankam la ĉeesto de ambaǔ dependas ĉefe de tio, kion oni mem emas fari kiel partoprenanto. Kelkajn klaĉojn oni tamen povas jam legi en la Pos-Amiko, la revuo de NEJ kaj plej amuza revuo de Esperantujo.

Aliaj tradicioj estas la Halovena balo, ekskurso, la jarkunveno de NEJ, kaj nuntempe ankaǔ TEJO-trejnadoj. Ĉi-jare estis anglalingva trejnado pri rajtbaziteco, kun trejnisto de Forumo Junulara Eǔropa. Mi bedaǔrinde ne partoprenis ĝin, sed mi vidis ke ĝi estis entuziasme sekvata. Dum la NEJ-kunveno, ni voĉdonis pri statutŝanĝoj, kutimaj dokumentoj, elektis novan Estraron, kaj decidis ke NEJ kandidatiĝos por IJK 2020 se ne venas taǔgaj kandidatiĝoj el Ameriko.

Ni havis tre plaĉan ekskurson al interesa loka muzeo: la muzeo de strangaj tepotoj! Tie ni ĉiuj malkovris ke tiu ĉiutaga objekto fakte estas fonto de senlima arta inspiriĝo. Dum la Halovena balo, ni ŝminkis kaj strange vestis nin, kaj voĉdonis por la plej bela (Alina), plej timiga (Jonny M) kaj plej originala (Roĉjo, Agata, Niko, Nika) kostumo. Krom tio estis multaj interesaj programeroj, kiel vespera kunkant-koncerto (Spenĉjo), desegnokurso (Alina), kaj prelegoj pri interalie la historio de la periodika sistemo (Frank), rifuĝintoj al Eǔropo (mi) aǔ spirproblemoj dum dormado (Miko).

Lastjare multaj homoj plendis ke la ejo estis malkomforta, ĉar estis nur unu ejo kie pluraj programeroj okazis samtempe,kaj ne estis taǔga hejtado. Feliĉe ĉi-jare NEJ povis oferti lokon samtempe malmultkostan kaj komfortan, kun hejtado kaj pluraj ejoj – eĉ apartaj dormejoj por festemuloj kaj dormemuloj. Bonege!! Estis ankaǔ laǔdoj por la manĝo, kiu estis kaj tagmeze kaj vespere varma, kaj vere bongusta. Kiel kutime, veganoj kaj vegetaranoj ne bezonis havi zorgojn. Dum la balo vendiĝis tre diversaj varmaj manĝetoj kaj bongustaj t/drinkaĵoj. Kompreneble, ĉiu renkontiĝo ricevas kaj laǔdojn kaj mallaǔdojn. Ĉi-jare mi aǔdis plendojn ke la renkontiĝo ne estis sufiĉe bonveniga por komencantoj. Estus bona ideo enkonduki anĝelan servon dum KKPS kaj doni pli da atento al komencantoj dum la interkona vespero.

Mi esperas ĝui denove bonegan etoson en la sekvontjara KKPS, kaj tie renkonti vin, karan leganton!
