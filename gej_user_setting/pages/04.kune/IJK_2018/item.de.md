---
title: Sommer, Sonne, Sonnenschein
menu:  Sommer, Sonne, Sonnenschein
date:  30-08-2018
slug:  ijk-2018
taxonomy:
    tag: [IJK, Hispanio, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Jonas Marx
---

Hóla die Waldfee - Das diesjährige IJK in Badajoz (Spanien) war ein Hitzehammer. Die Woche über herrschten Temperaturen bis zu 45 Grad. So verbrachten wir die meiste Zeit im hauseigenen Pool oder in einem der klimatisierten Räume. Außerdem gab es typische Programmpunkte wie Esperanto für Anfänger und Fortgeschrittene, Poesie, Jogakurse, Tanzen, Capoeira und Aikidō.

![IJF](image://IJK_Badajoz.jpg)

Am ersten Abend ging es – trotz Hitze - in die Stadtmitte, wo das KulturLingva Festivalo stattfand. Nachdem sich alle mit argentinischem Mate, deutschen Gummibärchen und slovakischen Spirituosen eingedeckt hatten, rockte Martin Wiese mit seiner E-Gitarre den Marktplatz. Am nächsten Tag sorgte eine Wasserschlacht für Abkühlung und zur Abendstunde wurden Filme gezeigt. Am Tag darauf folgte ein Ganztagesausflug, bei dem man sich für die römischen Monumente der Stadt Merido, einem Wasserpark oder einer Besichtigung der Grenze zu Portugal entscheiden konnte. Danach gab es mit besinnlicher Gitarrenmusik von Timothy Gallego einen Moment, um sich vom Tag auszuruhen. Jeder, der danach noch Kraft in den Beinen übrig hatte, konnte sich freuen: Ab heute organisierten die Veranstalter jede Nacht eine Disko, die selten vor 4 Uhr morgens endete.

„Utopio“ war der Name des skurrilen, aber einzigartigen Theaterstücks, das am darauffolgenden Abend aufgeführt wurde. Der Hauptdarsteller, ein betrübter Clown, konnte das Publikum so sehr rühren, dass am Ende sogar die kleineren Kinder des Treffens ungeplant die Bühne stürmten, um dem Protagonisten beizustehen. Zurecht bekam der Auftritt Standing Ovations. Gekrönt wurde der Tag mit der Karaoke.
Für den spanischen Flair kümmerte sich die Gruppe Cira y Ulises, die mit Percussion, Gitarrenklängen und kräftiger Frauenstimme überzeugte. Der nächtliche „Astronomische Spaziergang“ haute allerdings aufgrund des zu hellen Nachthimmels niemanden vom Hocker.

Der letzte Tag war vollgepackt mit Tanzkursen, Debatten und Ateliers. Der Abschlussabend startete mit dem Internacia Vespero, bei dem jeder Teilnehmer in ein paar Minuten ein Lied, Tanz oder sonstiges Kulturgut aus seinem Land aufführen konnte. Dieser Programmpunkt war sehr beliebt, denn auch er zog sich bis in die Nacht hinein. Obwohl das Konzert von Jonny M erst um Mitternacht begann, heizte er dem Publikum ordentlich ein. Zum Glück holte trotz diesbezüglicher Bedenken kein Nachbar die Polizei. Danach verlegten wir das Tanzparkett wieder in die Disko. Ins Bett gingen in dieser Nacht nur die wenigsten.  Die meisten Teilnehmer schleppten sich am nächsten Tag nach wehmütigen Verabschiedungen in ihre Busse und schliefen dort für ein paar Stunden. Ich verbrachte noch einen Tag in Sevilla und flog dann zurück in das doch etwas kühlere Deutschland.
