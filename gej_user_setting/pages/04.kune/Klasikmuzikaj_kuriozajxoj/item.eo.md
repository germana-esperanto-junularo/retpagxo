---
title: Klasikmuzikaj Kuriozaĵoj
menu:  Klasikmuzikaj Kuriozaĵoj
date:  08-09-2014
slug:  klasikmuzikaj_kuriozajxoj
taxonomy:
    tag: [rakonto, renkontiĝoj]
    category: Artikolo
author:
    name: Timo Pähler
---

La speco de muziko kiun plej multe da homoj konas kiel *klasikan
muzikon* fakte pli oficiale nomiĝas *okcidenta altkultura muziko *(aŭ
*OAM*), supozeble ĉar estas sufiĉe granda kongruo inter la homoj
klasikmuzikemaj kaj la homoj kiuj preferas havi klasan societon.
Verdire, en la historio de OAM troveblas ne malmultaj kuriozaj kaj ne
tro altkulturaj okazaĵoj... Pri kelkaj vi povas legi ĉi tie.

-   Pjotr Iljiĉ Ĉajkovskij kaj Ŝarl Kamij Sen-Sans ambaŭ estis
    baletemeguloj: Ili ofte kaj ĝoje kune imitis baletistinojn. Unufoje
    en Moskvo ili surscejenigis, por impresi unu la alian, la mallongan
    baleton *Galateo kaj Pigmaliono*. De Sen-Sans grandioze enkorpiĝis
    Galateo. Por tiuj el vi kiuj ne konas la rakonton: Galateo estas
    skulpturo. Baletanta skulpturo, ĉi tie.
-   Edvard Grig, kiu interalie komponis muzikon por la Ibsen-dramo *Per
    Gjunt* (*Peer Gynt*), diris la jenan pri unu el la pecoj: „Por la
    *Halo de la Monto-Reĝo* mi komponis ion kio tiom fetoras je
    bovokoto, ultra-Norvegiismo, kaj *al-vi-mem-sufiĉu*-eco, ke mi mem
    ne povas toleri aŭskulti ĝin, kvankam mi esperas ke la ironio ja
    rimarkiĝos.“ La venontan fojon kiam iu mencias al vi OAM-on, vi
    havas bonan kialon por rajti paroli pri bovokoto.
-   Artur Ŝnabel, aŭstra pianisto de la frua 20-a jarcento, iam
    demandiĝis pri la sekreto rilate al lia ludado. Lia respondo: „Mi
    ĉiam atentas, ke la kovrilo super la klavaro estu levita, antaŭ ol
    mi ekludas.“
-   Volfgango Amadeo Mocarto pro sia tuta persono kaj vivo meritas
    propran listopunkton. Nur kelkmalmultaj el liaj kuriozaj muzikaj
    kreaĵoj estas: la kanono „Leku mian pugon“ (K. 231); la
    violonsonatoj K. 379 kaj K. 454, por kiuj li notis la pianopartojn
    nur post la premieroj; la *Muzika Amuzaĵo* K. 522, kiu ŝercas pri
    sentalentaj komponistoj; kaj la ario *Kvazaŭ Roko *(*Come Scoglio*):
    La premierkantistino Adriana del Bene ofte levis la kapon dum altaj
    tonoj kaj faligis la mentonon dum malaltaj, do Mocarto komponis
    arion kun multaj grandaj tonsaltoj por povi rigardi ŝin „kapmovantan
    kvazaŭ koko“.
-   Lastlistere: La Baĥ-familio multkonate sufiĉe grandis. Johano
    Sebastiano havis dudek infanojn – aŭ ĉu dudek unu? Peter Ŝikele
    raportas pri (kaj ludas verkaĵojn de) la sola forgesita filo: P.
    D. Q. (*Pretty Damn Quick/Sufiĉe Damne Rapide*) Baĥ. Laŭ Ŝikele, la
    plej rekonebla karakteraĵo de la muziko de P. D. Q. estas „mania
    plagiatismo“. Sed kion alian povus fari forgesita filo, vere?
