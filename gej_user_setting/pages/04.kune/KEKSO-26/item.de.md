---
title: 25. KEKSO in Neumünster
menu:  25. KEKSO in Neumünster
date:  15-07-2019
slug:  kekso-25
taxonomy:
    tag: [KEKSO, Neumünster, renkontiĝoj]
    category: KEKSO
author:
    name: Lian Löffler
---

Ein bisschen seltsam waren die Leute schon, aber es war auch mein zweites KEKSO und ich wusste, was ich zu erwarten hatte. Und außerdem - seltsam bin ich ja auch, ein Grund wohl, warum ich mich in Esperantujo so wohl fühle.

![Keksoj](image://KEKSO_Neumuenster_1.jpg)

Als ich Freitag nach einer langen Zugfahrt und zweimal verlaufen (die Straßenbeschilderung dieser Stadt lässt wirklich zu wünschen übrig, mein Orientierungssinn auch) endlich ankam, wurde ich sogleich super lieb aufgenommen. Eine Gruppe von sechs Menschen waren wir, zu der am nächsten Morgen ein siebter hinzustoßen sollte. Den Abend begannen wir gleich mit einem Plenum, in dem vor allem eines klar wurde: wir hatten Hunger! Also wurde erst einmal gekocht und gegessen. Einen festen Plan für den Abend hatten wir so dann noch nicht, aber nach ein paar schönen Unterhaltungen wurde der Vorschlag eines Gruppenspiels - zwei Gruppen, die eine wird von der anderen zu einem Standbild geformt, das einen vorgegebenen Begriff darstellt - angebracht und angenommen, und auch wenn wir nicht so gut darin waren, haben wir doch verdammt viel gelacht.

Und auch die folgenden zwei Tage waren vollgepackt mit schönen Momenten - der Samstagmorgen ging für mich und eine weitere Anfängerin los mit einem Sprachkurs, in dem ich mich tatsächlich getraut habe, Esperanto zu sprechen (so nett Esperantist\*innen auch sind, wenn du kein bzw. wenig Esperanto kannst, können sie ganz schön gemein sein, weswegen ich persönlich die Strategie lebe, einfach niemals mit irgendwem zu reden), dann Plenum und sehr leckeres Mittagessen, mittlerweile waren wir auch vollzählig. Neben ausladenden Gesprächen über Sprache und politischen Diskussionen wurden mitgebrachte Brettspiele gespielt, wir gingen spazieren und machten Musik (zur Verfügung stand: eine Ukulele!). Und auch verschiedene WUPs (schnelle Warum-Up-Spiele, die sehr lustig und oft sehr peinlich sind) kamen nicht zu kurz, immer wieder konnte eins die KEKSO-Teilnehmer*innen vor dem Veranstaltungsgebäude stehend beobachten, wie sie beispielsweise mehr oder weniger rhythmisch "kunikleto", "tuki tuki" und "dangen" durcheinander sagten, begleitet von merkwürdigen Gesten. 

Am Sonntag Nachmittag war dann das „Kekso kaj Teo“ angesetzt, ein Programmpunkt des GEKs, in dem Teilnehmer*innen angeboten wurde, mit uns jungen Esperantist*innen in Kontakt zu treten - es wurde also Tee getrunken und gemeinsam Brettspiele gespielt.

Am Abend dann gab es ein Konzert, das wir gemeinsam besuchten - super schön war es, diejenigen, die tanzten wollten, tanzten! und zwischenzeitlich schlurpten wir uns aneinander fest (Schlurp! Meine Hand berührt jetzt deine Schulter. Schlurp! Ein Ellenbogen liegt auf meinem Rücken. Schlurp! Ein Fuß saugt sich an einer Hüfte fest. Schlurp schlurp schlurp! Wir sind ein Knäuel aus menschlichem Körper und keiner kann sich mehr bewegen. Wir fallen um). 

Ein bisschen belastend war nur, als wir für einen Tanz aufgefordert wurden, uns binärgeschlechtlich in Männer und Frauen getrennt gegenüberzustellen - nicht nur schließt das nicht-binäre Menschen (also Menschen, die weder ausschließlich Mann oder Frau sind) aus, es war auch einfach unnötig und von Heteronormativität geprägt. Aber trotzdem: ein sehr schöner Abend!
Am Montag hieß es dann schon Abschied nehmen, am Abend zuvor hatten wir bereits alles was möglich war aufgeräumt, jetzt wurden noch die letzten Sachen gepackt, gefegt und der Müll weggebracht, Möbel wieder an ihre Plätze gerückt.

Es war sogar möglich, dass wir alle gemeinsam Richtung Hamburg fuhren, und so Esperantujo noch ein kleines Stück mit auf den Weg nehmen konnten.

