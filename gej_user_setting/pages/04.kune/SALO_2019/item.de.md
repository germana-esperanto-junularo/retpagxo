---
title: 'Esperanto-Sommerfreizeit SALO'
date: '15-09-2019 00:00'
taxonomy:
    category:
        - SALO
    tag:
        - renkontiĝoj
        - Herzberg
        - SALO
hide_git_sync_repo_link: false
menu: 'Esperanto-Sommerfreizeit SALO'
slug: salo-19
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
author:
    name: 'Michaela Stegmaier'
---

Schon zum vierzehnten Mal in Folge und zum 10. Mal in Herzberg fand die deutsch-polnische Jugendbegegnung SALO (somera arbara lernejo) statt. Dabei ist Esperanto als Brückensprache nicht wegzudenken, da die Organisation und die Betreuung hauptsächlich durch das Esperanto-Zentrum mit freundlicher Unterstützung der Stadt Herzberg erfolgen.

Die Jugendlichen trafen sich schon vor der Begegnung, um sich untereinander und uns Betreuer kennenzulernen und ersten Kontakt zu den Austauschpartnern aufzunehmen. Die Vorfreude stieg und die letzten Vorbereitungen wurden getroffen. Und bald darauf war es soweit:

Zuerst trafen die deutschen Teilnehmer im Otto-Diedrich-Sportlerheim des VfL 08 Herzberg ein, das aufgrund einer kurzfristigen Absage durch die gewohnte Unterkunft als Alternativlösung diente. Der Gemeinschaftsraum wurde gestaltet und die Bettenlager aufgebaut. Kurz darauf konnten wir die polnischen Gäste begrüßen und ihnen ihre Schlafräume im Dachgeschoss des Jugendzentrums zeigen. Nach einem gemeinsamen Abendessen und ein paar Kennenlernspielen zogen sich die polnischen Teilnehmer erschöpft von der langen Reise in ihre Zimmer zurück. Es folgte eine Woche mit vielen spannenden Programmpunkten wie einer Stadtrallye mit Freibadbesuch im Juessee, einem Pizzaback-Schnupperkurs in der Stadtbesten Pizzeria, einem Besuch des Kängurooms in Bad Harzburg, Kino, Kulturquiz, Empfang im Rathaus uvm. An den Abenden wurde viel gespielt, ein Discoabend veranstaltet oder einfach mal nur am Lagerfeuer gesessen. Es gab von Beginn an einen regen Austausch zwischen den Jugendlichen, der im Laufe der Woche dazu führte, dass Freundschaftsbande geschlossen wurden.

Zum Ende der Freizeit lagen sich die Jugendlichen weinend in den Armen, um sich zu verabschieden. Der Bus war schon fertig gepackt und die Heimreise musste angetreten werden. 
Am vorherigen Tag war die Stimmung noch sehr ausgelassen, als die Gruppe das Museum im Schloss besuchte. Dort wurde zuerst die Sonderausstellung über die Geschichte des Schüleraustausches SALO betrachtet und zuletzt die mittelalterlichen Kostüme angezogen, die immer einen Höhepunkt des Museumsbesuchs darstellen.

Ansonsten wurde der letzte vollständige Tag ausschließlich genutzt, um die abendliche Abschiedsfeier vorzubereiten. Es wurden Papiergirlanden gebastelt, etliche Luftballons aufgepustet, bemalt und an eine Schnur gebunden. Die Jugendlichen übten Lieder und Tänze ein, um sie am Abend ihren Eltern und Freunden präsentieren zu können. Dann wurden Salate, Grillgut, Kekse und Muffins zubereitet. Als die Gäste eintrafen, konnten sie im Otto-Diedrich-Sportheim die entstandenen Materialien der Woche bestaunen: ein Stimmungsbarometer, Umschlagpost, Figuren aus Salzteig. Nach dem Essen war es Zeit für die Überreichung der Teilnehmerurkunden und ein paar Worte des Dankes durch Michaela Stegmaier. Iwona Sawicka überreichte im Anschluss an alle Teilnehmer ein Mitbringsel aus Góra. Dann konnten die Jugendlichen zeigen, was sie sich ausgedacht hatten. Gemeinsame Tänze wie den Bamba konnten auch die Eltern mittanzen. Alle waren begeistert von dem Zusammenhalt der Gruppe und die Jugendlichen äußerten Interesse an einer erneuten Teilnahme im nächsten Sommer. Einige sehen sich auch im Rahmen anderer Esperanto-Veranstaltungen im September schon wieder, um die lange Wartezeit besser zu überbrücken.