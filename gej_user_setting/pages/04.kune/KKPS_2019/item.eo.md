---
title: Pri KKPS kaj malaperantoj
menu:  Pri KKPS kaj malaperantoj
date:  15-11-2019
slug:  kkps-19
taxonomy:
    tag: [KKPS, Görlitz, renkontiĝoj]
    category: KKPS
author:
    name: Michael Vrazitulis
---

Do mi vizitis KKPS-n inter la 1-a kaj 3-a de novembro. Por tiuj, kiuj ne konas ĝin: Temas pri ĉiuaŭtuna eventeto, organizata de la Nederlanda Esperanto-Junularo kun surloka helpado de la Verdaj Skoltoj.

![Keksoj](image://Koko_Poso_2019.jpg)

Areto da homoj ni estis, kiuj renkontiĝis proksime al Bodegraven, malgranda urbo en ĉarme nenieca ĉirkaŭaĵo: "Tiu ĉi estas la plej holanda pejzaĝo, kiun vi iam ajn vidintos." Iel tiel diris al ni Roĉjo, unu el la ĉefaranĝantoj, dum li tiuvendrede en la pluveta posttagmezo nin veturis aŭte de la stacio al la ejo. Li estis montranta per unu mano ekster la aŭtfenestron. Videblis malhelverdaj kampoj, plej fore ventelektrigiloj disritme rotaciantaj kaj tie-tie la siluetoj de biciklistoj. „Ja holanda, ĉu“, mi pensis, kiam la aŭto iam haltis, kaj eliĝis krom Roĉjo du italaj geesperantistoj kaj mi. Oni estis apenaŭ dudek, kiam mi sidiĝis – akceptiĝinte, sentante malsaton, pro la ĵusa vojaĝo – por vespermanĝi. Pli malfrue alvenis kelke pli da personoj.

Certe estis belege revidi multajn vizaĝojn konatajn. La unua vespero de tiu trankvila dumsemajnfina evento iam disfluiĝis post etrondaj babiladoj, kartludadoj, drinkumadoj.

![Keksoj](image://KKPS_2019_k.jpg)

Sabatmatene voĉoj vigle diskutantaj ekvekis min. Ili venis defore. Kelkaj homoj supozeble do jam estis matemanĝantaj sube en la salono. Iom bizare ja estas vekiĝi pro planlingvaj krioj, mi subite riflektis. Sed kial oni preferu kokokrion?
Post la matenmanĝo kunsidis kaj asembleis la anoj de NEJ. Verdire mi malmulte scias pri la priparolitaĵoj kaj deciditaĵoj, ĉar mi ne longe ĉeestis tion. Sed mi ja imagas, ke la nederlandajn samideanojn okupas ĉiuokaze granda kvanto da laboro, certe ankaŭ pro la planata realigo de venontjara IJK en ilia lando.

Sekvis ekskurso al proksime troviĝanta natura parko. La halovena kostuma "balo" dum la vespero kompletigis la jam tradician distroprogramon de KKPS. 

Por mi tiu estis fakte la dua tia evento partoprenita. Antaŭ du jaroj mi jam vizitis ĝin. Tiam, mi memoras, la dancejo kaj drinkejo estis plenaj dumlonge, preskaŭ ĝis la noktofin'. Ĉi-jare la kompare malgranda nombro de partoprenantoj igis la etoson pli trankvilema, kvazaŭgufuja. Sendube ankaŭ tio havis sian unikan ĉarmon.
Konsiderante tamen, ke estas la jaro 2019, kiam ĉi-somere en Lahtio, Finnlando, okazis denove historie malgranda Universala Kongreso, eble por ni indas, ke ni esploru la jam anekdote rimarkeblan, paŝon post paŝo graviĝantan stagnacion aŭ eĉ malkreskadon de la esperantistaro. Kvankam promesemaj retbazitaj projektoj, kiel Amikumu aŭ la lingvokurso ĉe Duolingo parte prave altigis esperojn pri esperantigo de iaj novaj interretulaj amasoj, tiuj projektoj ŝajne ne sukcesis estigi ion plian ol efemerajn interesitojn pri la lingvo.

Kompreneble estus pesimismaĵo, tro draste interpreti la nuntempajn realojn. Rimarkeblas tamen, ke ĝuste tiun zorgon pri malrapida formalkresko de nia lingvokomunumo antaŭ ne tro longe jam esprimis Grigorij Arosev en artikolo "La Esperanto-movado pereas", publikigita fine de ĉi-pasinta julio en Libera Folio.

Dimanĉe, post la lasta tagmanĝo, kiel menciinda programero okazis ankoraŭ trejnado pri varbado. Ĝin bonege gvidis konata italdevena aktivulo kun karakteristike laŭta voĉo. Verŝajne ja nur ĝuste tio, la ĝisdatigo kaj perfektigo de poresperante varbaj aktivadoj, povas kuraci la problemon de stagnacio.

Iom poste ĉiuj adiaŭis unuj al la aliaj, disiĝis, forveturis. Espero por Esperantujo ja ne malabundas, mi pensis, kiam mi staris kun du samlingvanoj ĉe la stacidomo de Bodegraven, atendante la trajnon direktiĝontan ree al Utrecht. Unu el la du ja estis "freŝbakita" flandra esperantparolanto, eklerninta la lingvon antaŭ apenaŭ tri monatoj laŭaserte.

Alveninte en Utrecht post duonhoreto da veturado, mi restis ankoraŭ kun unu alia juna esperantisto, nederlanda. Dum niaj lastaj hazardaj interparoleroj li rakontis al mi, ke li aktivadas ĉe Vikipedio, kreante kaj redaktante artikolojn tie. Ekzistis, tiel li plurakontis, en la sepdekaj jaroj iu greka esperantisto, Savas Tsiturides, kiu dum du jaroj kiel prezidanto estris TEJO-n. Sed nenie ŝajnas trovebli pli da informoj pri li ol tio. Supozeble li baldaŭ post prezidanteco kabeis. "Ĉu vi povas helpi min trovi pli da faktoj pri tiu viro?", demandis la redaktemulo. "Certe", mi respondis.
Malaperis ankaŭ li. Mi entrajniĝis denove por reveturi Germanien. Ĝis Frankfurto oni estis bezononta pli ol tri horojn. Komforte sidiĝinte, mi kontrolis la tujmesaĝejon ĉe Telegramo, kreitan por la ĵus finiĝinta evento. Venontjare, iu skribas, KKPS okazos en la nordo de Danio, proksime de la urbo Ry. Malŝaltinte la poŝtelefonon mi fermas la okulojn kaj komencas duondormi. Ja mankas iom da dormo, kutima afero dum kaj post evento esperanta.

Kelkajn tagojn poste mi rememoras la peton de la juna nederlanda vikipediisto, kiun mi estis adiaŭinta ĉe la trajnstacia kajo. En fejsbukan grupon de grekaj esperantistoj mi afiŝas demandon pri tiu mistera kabeinta TEJO-prezidanto de la sepdekaj jaroj. "Mi konis Savas", iu maljuna samideano komente respondas, "ni estis eĉ najbaroj en Ateno. Li studis medicinon, fariĝis psikiatro. Sed poste iel mi perdis la kontakton. Ŝajne li ja simple malaperis." Kien do, Gerda, kien?
