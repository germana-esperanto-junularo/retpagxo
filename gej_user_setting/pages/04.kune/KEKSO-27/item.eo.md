---
title: 'ĈAR KEKSOj mojosas – landlima renkontiĝo de PEJ kaj GEJ'
date: '19-11-2019 00:00'
taxonomy:
    category:
        - KEKSO
    tag:
        - renkontiĝoj
        - KEKSO
        - Görlitz
hide_git_sync_repo_link: false
menu: 'ĈAR KEKSOj mojosas – landlima renkontiĝo de PEJ kaj GEJ'
slug: kekso-27
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
author:
    name: 'Lian Löffler'
---

Ĉijare la Pola Esperanto-junularo organizis sian aŭtunan renkontiĝon por novuloj ĈAR samtempe kaj kune kun la KEKSO de la Germana Esperanto-junularo ĉe la german-pola landlimo en Zgorzelec kaj Görlitz. Pro la kombino de la eventoj ni nomis ĝin „ĈAR KEKSOj mojosas“ kaj kreis propran logotipon kombinitan el tiuj de la unuopaj eventoj. Ĉiuj partoprenantoj renkontiĝis meze de oktobro dum milda semajnfino en la turisma domo en la pola flanko de la tiel nomata Eŭropa-urbo. Dum la unua vespero ni interkonatiĝis, ludis, gustumis polan vodkon, manĝis picon kaj amuziĝis. De la germana flanko venis du aktivuloj kaj unu novulino. Por ŝi estis la unua Esperanto-evento. Ŝi venas el Lepsiko kaj eklernis Esperanton du monatoj antaŭ KEKSO. Ŝi jam bone komprenis la enhavon de interparoloj kaj povis formuli kelkajn frazojn en Esperanto. 

<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <figure class="figure">
      <img class="img-responsive" src="/retideoj/gej/user/images/CXAR_KEKSOj_mojosas.jpg">
   </figure>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <figure class="figure">
      <img class="img-responsive" src="/retideoj/gej/user/images/CXAR_KEKSOj_mojosas_2.jpg">
   </figure>
  </div>
</div>

De la pola flanko alvenadis deko da junuloj per trajno kaj per propra aŭto ĝis tre malfrua horo, inter ili tri novuloj, kiuj unue aperis en IJK en Slovakio kaj poste ankaŭ en Arkones. En la sabata antaŭtagmezo ni estis dividitaj en etaj grupoj por malkovri la tutan urbon ludante urboludon. Ĉirkaŭ dudek lokojn ni devis trovi lau iliaj koordinatoj kaj tie respondi al ne facilaj demandoj. La ludo tre plaĉis al ĉiuj, ĉar estis vere interesaj lokoj kaj feliĉe belega vetero. Tagmeze ĉiuj kuniĝis en restoracio por poste ankoraŭ kune rigardi la urbon, inter alie kristnaskan domon, kie vi povas aĉeti figurojn kaj ornamaĵojn por la sankta festo dum la tuta jaro. Ni estis bonŝancaj havi lokulon, kiu gvidis nin esperante kaj aldone invitis nin al lia loĝejo por tie denove ludi, babili kaj, tre grave, manĝi KEKSOjn en la vespero. Dum li ĉion purigis kaj preparis, ankoraŭ okazis la premiigo de la venkintoj de la urboludo kaj prezentadoj de prelegetoj pri diversaj temoj. Kvizo pri keksoj bone enplektis la plej gravan econ de la germanaj novulaj renkontiĝoj. Dankon al Roma pro la preparado. Ni iris al la loĝejo de nia ĉiĉerono, kie ni sidis kune ĝis tre malfrue. Iam en la nokta malvarmo homoj unu post la alia remigris al nia gastejo.  

En dimanĉo ni trankvile kunsidis, iris matenmanĝi, priparolis ontan kunlaboron inter GEJ kaj PEJ kaj devis adiaŭi baldaŭ por reveturi al niaj hejmoj. La germana grupeto iris tra la germana urboparto al la germana stacidomo de Görlitz kaj karavane reveturis al Dresdeno, Lepsiko laj Brunsviko.


