---
title: Kultura kunlaboro en la montaro - IJF en Lombardio
menu:  Kultura kunlaboro en la montaro - IJF en Lombardio
date:  10-04-2017
slug:  ijf-2017
taxonomy:
    tag: [IJF, Italio, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Charlotte Scherping Larsson
---

Matene, la vido el mia ĉambro estis la sunleviĝo malantaŭ bela montaro. La ejo de la Internacia Junulara Festivalo estis en Castione della Presolana en Lombardio, en la nordo de Italio, fore de kutimaj turistaj lokoj. Ĉi-jare la Internacia Junulara Festivalo (IJF) okazis inter la 12-a kaj 18-a de aprilo, kio signifis ke ne ĉiutage estis varma vetero. 98 partoprenantoj el multaj landoj havis la eblon amikumi. Por la unua fojo ankaŭ ĉeestis partoprenantoj el Benino kaj Burundo, kadre de AEJK, la afrika-eŭropa junulara kapabligo. Dume okazis la komitatkunsido de TEJO, la unua fojo kun faciligado brile farita de Oleksandra Kovyazina. Ankaŭ la nova estraro de la Itala Esperantista Junularo (IEJ) estis elektita.

![](image://ijf-2017.jpg)

Parto de la partoprenantoj alvenis kelkaj tagoj antaŭe por partopreni la tutsemajnan Trejnadon de Trejnistoj. Planita estis 24 partoprenantoj, sed bedaŭrinde la togolandanoj ne ricevis vizojn. La unuaj tagoj estis plenaj je teorioj kaj diskutoj. Kune ili preparis trejnadojn por la festivalo. Estis kunlaborema kaj kreema etoso. Ni ĉeestis enhavplenajn sesiojn pri tiom diversaj temoj kiel tempo-mastrumado, merkatiko kaj
genra egaleco.

La manĝaĵo estis bongusta, kun lokaj specialaĵoj, kiel polento, pico, pastaĵo kaj *mostarda di frutta di Cremona*, dolĉe konservita frukta spicaĵo kun mustarda gusto. La organizado de Michael Boris Mandirola, ke oni mendu kiam la pladoj estis konataj, estis vere bona. De nordeŭropa vidpunkto la matenmanĝo pliboniĝis pro la eblo aĉeti italan fromaĝon kaj kolbason per steloj. Per kafo ebligis ĝui la veran koron de Italio.

La ekskursoj estis diverstipaj, kiel la ekskurso al Brescia kie estis prelegoj pro la 100-a datreveno de la forpaso de Zamenhof pri Esperanto en la nuna mondo. Ni inter alie lernis kiel funkcias la rilato inter ĝemelaj urboj, de la ekzemplo Darmstadt-Brescia. Poste okazis profesia urbogvidado kun vizito de Capitolium, konstruaĵo de la antikva roma tempo. Alia ekskurso estis migrado en la montaro, kie ni akcidente trovis Rusion, malgrandan apudan vilaĝon. La tuttaga ekskurso al Montisola, la insulo en la lago de Iseo, fine ebligis iom da ripoztempo apud la lago. Vere indas iri al la itala kongreso.
