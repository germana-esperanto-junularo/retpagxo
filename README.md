# Kiel instali

Klonu la repo en via `www` dosierujo kaj poste

`bash instali.sh`

Poste nepre vizitu la grav paĝon por enigi vin kiel administrestro.

# Ekzemplo

www.p4w5.eu/grav/gej/

## Servu la paĝaro lokale

Ŝangu al la gej subdosierujo post la instalo kaj ekservu per 

`php -S localhost:8000 system/router.php`

# Ĝisdatigi

Vi povas rekte en la grav subdoierujoj sub "gej/user/" fari viajn ŝanĝojn kaj ĝisdatigi la repo pere de

`rsync gej/user/ grav_user_setting/ -r`

aŭ iel tiel ;=)
